float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
   float2 Tex    : TEXCOORD0; // base
   float2 Spec   : TEXCOORD1; // spec
   float4 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sSpec : register (s1);
samplerCUBE sCube : register (s2);

PS_OUTPUT ps20_srpl_carpaint01 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Fresnel parameters
  float fresnelBias = 0.2;
  float fresnelScale = 1.0;   //original = 7.0
  float fresnelPower = 5.0;   //original = 4.0

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);

  if (tex.a < 0.1)
  {
    discard;
  }
  float4 spec = tex2D (sSpec, Input.Spec) * 1.75;

  float3 normal = normalize (Input.Normal.xyz);
  float3 normEyeVec = normalize (Input.EyeVec);

  float3 halfway = normalize (worldLight - normEyeVec); 
  halfway.x = dot (halfway, normal);
  
  half4 specular;
  specular.rgb = saturate (pow (halfway.x, 4.0)) * spec.r * 0.2;
  specular.a = 0;

  half4 specular2;
  specular2 = saturate (pow (halfway.x, 1000.0)) * spec.r * float4(20.0, 18.0, 15.0, 0.0);

  // Compute diffuse color
  half4 diffuse = saturate ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor * 0.5 + Input.Omni);

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 cubeCoord;
  cubeCoord = reflect(normEyeVec, normal);
  cubeCoord.y *= sign(Input.Normal.w);
  float4 cube = texCUBE (sCube, cubeCoord);
  cube.rgb *= 1.6;
  cube.rgb *= spec.rgb;
  cube.a = 1.0;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = fresnelBias  + fresnelScale * pow(1 - dotEyeVecNormal, fresnelPower);

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day;
  
  // Cube diffusion
  float cubeDiffuse = diffuseColor + (Input.Omni.r * 0.333) + (Input.Omni.g * 0.333) + (Input.Omni.b * 0.333);

  // Final color
  Out.Color = (tex * diffuse + specular * day) * (1.0) + ((cube + specular2) * fresnel * 0.8);

  return (Out);
}
          