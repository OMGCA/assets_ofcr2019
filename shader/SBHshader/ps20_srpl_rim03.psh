float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 SideVec : COLOR0;
   float4 Omni    : COLOR1;
   float  Fog     : FOG;
   float2 Tex     : TEXCOORD0; // base
   float2 Spec    : TEXCOORD1; // spec
   float4 Normal  : TEXCOORD2;
   float3 EyeVec  : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sSpec : register (s1);
samplerCUBE sCube : register (s3);

PS_OUTPUT ps20_srpl_rim03 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Fresnel parameters
  float fresnelBias = 0.2;
  float fresnelScale = 0.3;
  float fresnelPower = 3.0;

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);

//  if (tex.a < 0.2)
//  {
//    discard;
//  }
  float4 spec = tex2D (sSpec, Input.Spec);
  float3 normal = normalize (Input.Normal.xyz);
  float3 normEyeVec = normalize (Input.EyeVec);
  float3 sideVec = (Input.SideVec - float4(0.5f, 0.5f, 0.5f, 0.5f)) * 2.0;

  float3 halfway = normalize (worldLight - normEyeVec); 
  halfway.x = dot (halfway, normal);
  
  half4 specular;
  specular.rgb = saturate (pow (halfway.x, specularColor.a * 2.0)) * specularColor.rgb * spec.rgb;
  specular.a = 0;

  half4 specular2;
  specular2 = saturate (pow (halfway.x, 20.0)) * float4(5.0, 4.5, 4.0, 0.0);

  // Compute diffuse color
  half4 diffuse = saturate ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor * 0.5 + Input.Omni);
  float diffuseStrength = max(dot(normal.xyz, sideVec.xyz) * 0.6 + 0.4, 0.0);
  diffuse *= diffuseStrength;

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 cubeCoord;
  cubeCoord = reflect(normEyeVec, normal);
  float reflectStrength = pow(max(dot(cubeCoord.xyz, sideVec.xyz) * 0.7 + 0.3, 0.0), 0.4);
  float4 cube = texCUBE (sCube, cubeCoord);


  //add more contrast to cubemap
  cube.rgb *= 0.5;
  cube.rgb *= spec.rgb;

  cube.a = 1.0;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = fresnelBias  + fresnelScale * pow(1 - dotEyeVecNormal, fresnelPower);
  fresnel *= reflectStrength;

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day;
  
  // Cube diffusion
  float cubeDiffuse = diffuseColor + (Input.Omni.r * 0.333) + (Input.Omni.g * 0.333) + (Input.Omni.b * 0.333);

  // Final color
  Out.Color = (tex * diffuse + specular * float4(day, day, day, 1.0)) + ((cube + specular2) * fresnel);
//Out.Color.rgb = float3(diffuseStrength, reflectStrength, 0.0);

  return (Out);
}
           