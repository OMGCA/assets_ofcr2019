#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float4 Omni   : COLOR1;
#ifdef USEFOG
   float  Fog    : FOG;
#endif   

 
   float2 Tex    : TEXCOORD0; // base
   float2 Spec   : TEXCOORD1; // spec
   float4 Normal : TEXCOORD2; // world space vertex normal + sign of reflection
   float3 EyeVec : TEXCOORD3; // world space eye vec
};

VS_OUTPUT vs20_srpl_window04 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL, float2 inTex : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inBump : TEXCOORD2)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  Out.Normal.xyz = worldNorm;
  Out.Normal.w = worldMatrix[1][1];
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec = eyeVec;
  
#ifdef USEFOG
  // compute fog
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif   

  // Compute omnilights
  Out.Omni = (0,0,0,0);

  // Propagate color and texture coordinates:
  Out.Diff = inColor;

  Out.Tex.xy = inTex;
  Out.Spec = inSpec;

  return (Out);
}

  