float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float  Fog    : FOG;

   float4 Tex0    : TEXCOORD0;
   float4 Tex1    : TEXCOORD1;
   float3 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
   float3 WaterParams : TEXCOORD4;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sBump : register (s0);
samplerCUBE sCube : register (s1);

PS_OUTPUT SSTR_WATER02 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Sample the textures
  float3 n1 = tex2D (sBump, Input.Tex0.xy).xyz - float3(0.5, 0.5, 0.5);
  float3 n2 = tex2D (sBump, Input.Tex0.zw).xyz - float3(0.5, 0.5, 0.5);
  float3 n3 = tex2D (sBump, Input.Tex1.xy).xyz - float3(0.5, 0.5, 0.5);
  float3 n4 = tex2D (sBump, Input.Tex1.zw).xyz - float3(0.5, 0.5, 0.5);
  float3 bump = n1 * 0.3 + n2 * 0.4 + n3 * 1.0 + n4 * 1.2;
  bump.xy *= Input.WaterParams.y;
  bump = bump.xzy;
  bump = normalize(bump);


  float3 normEyeVec = normalize (Input.EyeVec);


  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 cubeCoord;
  cubeCoord = reflect(normEyeVec, bump);

  float specularDot = saturate(dot(worldLight, cubeCoord));
  float3 specular;
  specular  = float3( 0.3,  0.25, 0.2) * pow(specularDot, 3.0);
  specular += float3( 0.6,  0.5,  0.4) * pow(specularDot, 50.0);
  specular += float3( 6.0,  5.5,  5.0) * pow(specularDot, 1200.0);

  cubeCoord.y += Input.WaterParams.z;
  float4 cube = texCUBE (sCube, cubeCoord);
  specular *= cube.a;

  float dayTime = saturate(ambientColor.b * 2.0);

  cube.rgb += specular * specularColor;
  cube *= dayTime;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, bump));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = saturate(FRESNELBASE  + FRESNELSCALE * pow(1.0 - dotEyeVecNormal, FRESNELPOWER));

fresnel /= (1.0 + length(Input.EyeVec) / Input.WaterParams.x);

//  cube = lerp( float4(0.0, 0.2, 0.15, 0.1), cube, fresnel);
cube.a = fresnel;
  cube.a += specular * 0.1;

  Out.Color.rgba = cube.rgba;

  return (Out);
}
