// Vertex shader for light flares 
// ------------------------------
// This shader makes the quad face the camera and change its size according to the camera vs quad normal.
// This shader should only be applied to quads (square consisting of 2 triangles) that face forward. 
//
// Created by Siim Annuk
// Date: June 28, 2009

#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);
float4 worldEye: register (c8);
float4 fogData : register (c9);
float4 texMod : register (c11); // tex0.u=x, tex0.v=y, tex1.u=z, tex1.v=w
float4 specularColor : register (c15); // power in c15.a
float4 dirLight : register (c14);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
#ifdef USEFOG
   float  Fog    : FOG;
#endif   
   float3 Tex    : TEXCOORD0;
   float3 Normal   : TEXCOORD1;
   float3 RealSunPos : TEXCOORD2;
   float4 TexOff : TEXCOORD3; // texcoord offsets for two blended images
   float3 EyeVec : TEXCOORD4; // world space eye vec
};


VS_OUTPUT SSTR_MOONGLOW01 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0,
                       float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL,
                       float2 inTex0 : TEXCOORD0, float2 inParam1 : TEXCOORD1,
                       float2 inParam2 : TEXCOORD2, float2 inParam3 : TEXCOORD3)
{
  VS_OUTPUT Out;

float LAT = inParam1.x; //39.0 * 3.14159265 / 180.0;
float NORTH = inParam1.y; //0.0;

  float3x3 sunMatrix;
  sunMatrix[0] = float3(1.0, 0.0, 0.0);
  sunMatrix[1] = float3(0.0, sin(-LAT), cos(-LAT));
  sunMatrix[2] = float3(0.0, -cos(-LAT), sin(-LAT));

  float3x3 northMatrix;
  northMatrix[0] = float3(cos(NORTH), 0.0, -sin(NORTH));
  northMatrix[1] = float3(0.0, 1.0, 0.0);
  northMatrix[2] = float3(sin(NORTH), 0.0, cos(NORTH));

  sunMatrix = mul(sunMatrix, northMatrix);



float4 plane1;
plane1.xyz = sunMatrix[2];
plane1.w = inParam3.y;
float4 sunDir;
sunDir.xyz = normalize(dirLight.xyz);
sunDir.w = 1.0;

float d1 = dot(sunDir, plane1);

sunDir.y -= d1 / sunMatrix[2].y;
sunDir.xyz = normalize(sunDir.xyz);


////////////////////////////////////////

float2 vec1;
vec1.x = dot(sunMatrix[0], sunDir.xyz);
vec1.y = dot(sunMatrix[1], sunDir.xyz);
vec1 = normalize(vec1);

float angle;

  if (vec1.x == 0)
  {
    if (vec1.y > 0)
    {
      angle = 0.0;
    }
    else
    {
      angle = 3.1415926535;
    }
  }

  if (vec1.y == 0)
  {
    if (vec1.x > 0)
    {
      angle = 0.5 * 3.1415926535;
    }
    else
    {
      angle = 1.5 * 3.1415926535;
    }
  }

  if (vec1.y > 0)
  {
    angle = atan(vec1.x / vec1.y);
  }
  else
  {
    angle = 3.1415926535 + atan(vec1.x / vec1.y);
  }
////////////////////////////////////////


float MOONLAT = inParam2.x;
float MOONNORTH = inParam2.y;

Out.RealSunPos = sunDir.xyz;

float moonAngle = -2.0f * angle;

float3x3 moonMatrix;
moonMatrix[0] = float3(cos(moonAngle), 0.0,  sin(moonAngle));
moonMatrix[1] = float3(0.0,        1.0,         0.0);
moonMatrix[2] = float3(sin(moonAngle), 0.0, -cos(moonAngle));

float3x3 moonMatrix2;
moonMatrix2[0].xy = inParam2;
moonMatrix2[0].z = inParam3.x;
moonMatrix2[1] = inTangent.xyz;
moonMatrix2[2] = inBiNorm.xyz;

moonMatrix = mul(moonMatrix2, moonMatrix);

float4 pos;
pos.xyz = inPos.xzy * float3(0.2, 0.2, 0.0) + float3(0.0, 1640.0 * (inColor.r - 0.5), 820.0);  //1640.0 * (inColor.r - 0.5)
pos.xyz = mul(moonMatrix, pos.xyz);
pos.xyz += sunDir.xyz * 12.0;
pos.w = 1.0;

Out.EyeVec = pos.xyz;
Out.Pos = mul(viewProjMatrix, pos);


  Out.Normal = mul(worldMatrix, mul(moonMatrix, float3(0.0, 0.0, -1.0)));

#ifdef USEFOG
  Out.Fog.x = 100000.0f;
#endif
  
  Out.Tex.xy = inTex0;
  Out.Diff = float4(1.0f, 1.0f, 1.0f, 1.0f);




  float fImageIndex = atan2(sunDir.y, length(sunDir.xz));
  fImageIndex = fImageIndex * 31.0 + 32.0;
  fImageIndex = max(0.0, fImageIndex);
  fImageIndex = min(62.0, fImageIndex);
  int imageIndex = int(fImageIndex);

  Out.Tex.z = fImageIndex - float(imageIndex);
  
  int2 imageCoord;
  imageCoord.y = imageIndex / 8;
  imageCoord.x = 7 - imageIndex - imageCoord.y * 8;
  Out.TexOff.xy = float2(imageCoord) / 8.0;

  imageIndex = imageIndex + 1;
  imageCoord.y = imageIndex / 8;
  imageCoord.x = 7 - imageIndex - imageCoord.y * 8;
  Out.TexOff.zw = float2(imageCoord) / 8.0;


  return (Out);
}








