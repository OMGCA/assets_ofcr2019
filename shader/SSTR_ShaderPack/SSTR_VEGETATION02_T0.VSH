float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;
   float2 Tex0    : TEXCOORD0; // base0
   float3 EyeVec  : TEXCOORD1; // world space eye vec
//   float3 Normal  : TEXCOORD2; // 
   float3 CenterVec1 : TEXCOORD3; //
   float3 CenterVec2 : TEXCOORD4; //
   float2 Radius : TEXCOORD5; //
};


VS_OUTPUT SSTR_VEGETATION02_T0 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0,
                                float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2, float2 inTex3 : TEXCOORD3)
{
  VS_OUTPUT Out;
  
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = worldEye.xyz + worldPos.xyz;
  Out.EyeVec = normalize(eyeVec);


  float3 centerPoint;
  float3 worldNorm = inNorm;
//  worldNorm.xy = inTex1;
//  worldNorm.z = inTex3.x;
  centerPoint.xy = inTex2;
  centerPoint.z = inTex3.y;


  float2 radius;
  radius.x = length(inPos.xz - centerPoint.xz);
  radius.y = abs(inPos.y - centerPoint.y);


  worldNorm = normalize(mul(worldMatrix, worldNorm));

//  if (dot(eyeVec, worldNorm) >= 0.0)
//  {
//    worldNorm *= -1.0;
//  }
//  Out.Normal = worldNorm;

  float3 centerVec = inPos.xyz - centerPoint;
  Out.CenterVec1 = mul(worldMatrix, centerVec * float3(1.0, 0.0, 1.0));
  Out.CenterVec2 = mul(worldMatrix, centerVec * float3(0.0, 1.0, 0.0));
  Out.Radius = inTex1;

  
  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

#ifdef OMNISCALE
  // Compute omnilights
  Out.Omni = float4(0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }
#endif

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex0.xy = inTex0;

  return (Out);
}

