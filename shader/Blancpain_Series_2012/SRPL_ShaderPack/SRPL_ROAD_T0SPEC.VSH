#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 worldLight    : register (c2);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);




struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog   : FOG;

   float4 Tex    : TEXCOORD0; // base=xy, spec=zw

   float3 Normal : TEXCOORD1; // normal vector
   float3 Light  : TEXCOORD2; // tangent space light
   float3 EyeVec : TEXCOORD3; // tangent space eye vec

};

VS_OUTPUT MAINFUNCTION (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex : TEXCOORD0, float2 inSpec : TEXCOORD1)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldPos = mul (worldMatrix, inPos);
  
  Out.Light = dirLight;
  Out.EyeVec = worldPos - invViewMatrix[3];

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  float3 eyeVec = (worldEye + worldPos.xyz);

  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex;
  Out.Tex.zw = inSpec;
  Out.Normal = worldNorm;

  return (Out);
}
