float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3);

#define exposition(op1, op2) float3(1.05, 1.05, 1.05) - pow(float3(2.7, 2.7, 2.7), op1 * op2) * 1.05

struct PS_INPUT
{
  float3 Tex0  : TEXCOORD0; // xy=texcoord,  z=blend
  float4 Diff  : COLOR0;
  float3 Normal   : TEXCOORD1;
  float3 RealSunPos : TEXCOORD2;
  float4 TexOff : TEXCOORD3; // texcoord offsets for two blended images
  float3 EyeVec : TEXCOORD4; // world space eye vec
  float  Expos  : TEXCOORD5;
};

sampler2D sTex0 : register (s0);
sampler2D sTex1 : register (s1);
samplerCUBE sTex2 : register (s2);
sampler2D sTex3 : register (s3);

struct PS_OUTPUT
{
  float4 Color : COLOR0;
//  float  Depth : DEPTH;
};


PS_OUTPUT SRPL_L30_MOON01 (PS_INPUT Input)
{
  PS_OUTPUT Out;

  float3 normal = normalize (Input.Normal.xyz);
  float3 normLightVec = normalize (Input.RealSunPos.xyz);
  float3 normEyeVec = normalize(Input.EyeVec);

  float4 cube = texCUBE (sTex2, normEyeVec);

  float2 texc;
  texc.y = max(0.0, normEyeVec.y) / 8.25 + 0.002;
  texc.x = distance(normLightVec.xz, normEyeVec.xz) / 16.5 + 0.003;

  float4 tex0 = tex2D (sTex0, Input.Tex0.xy); 

  float diffuse = saturate(dot(normal, normLightVec)) * (1.25 - diffuseColor.r * 0.6);
  tex0.rgb *= diffuse;

  //atmosphere
  float4 tex1 = tex2D(sTex1, texc + Input.TexOff.xy);
  float4 tex2 = tex2D(sTex1, texc + Input.TexOff.zw);
  tex1.rgb *= tex1.a * 25.5;
  tex2.rgb *= tex2.a * 25.5;
//  float4 skyColor = pow(lerp(tex1, tex2, Input.Tex0.z), 1.6) * float4(3.0, 2.8, 2.5, 0.0);
  float4 skyColor = lerp(tex1, tex2, Input.Tex0.z);

float3 cirrusColor = tex2D(sTex3, float2(distance(Input.RealSunPos, normEyeVec) * 0.5, Input.RealSunPos.y * 0.5 + 0.5)).rgb;
skyColor.rgb = lerp(skyColor.rgb, cirrusColor * 5.0, cube.r * 0.5);


skyColor.rgb = exposition(skyColor.rgb, Input.Expos);

  Out.Color = tex0 + skyColor;
  
  return (Out);
}
