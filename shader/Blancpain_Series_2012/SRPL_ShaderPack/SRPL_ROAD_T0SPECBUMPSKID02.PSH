float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float  Fog    : FOG;
   float2 Tex    : TEXCOORD0; // base
   float4 Spec   : TEXCOORD1; // spec and track map
   float4 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;

   float4 Omni   : COLOR1;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sSpec : register (s1);
sampler2D sBump : register (s2);
sampler2D sMap  : register (s3);

PS_OUTPUT SRPL_ROAD_T0SPECBUMPSKID02 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Fresnel parameters
  float fresnelBias = 0.1;
  float fresnelScale = 0.8;  //1.6
  float fresnelPower = 20.0; //20.0

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);
  float4 spec = tex2D (sSpec, Input.Spec.xy);
  float4 bump = tex2D (sBump, Input.Spec.xy);
  float2 skidCoord = Input.Spec.zw;
skidCoord.x = saturate(skidCoord.x);
  float4 trackMap = tex2D (sMap, skidCoord);
//trackMap = lerp(trackMap, float4(0.35, 0.0, 0.51, 1.0), Input.Color.a);
trackMap = lerp(trackMap, tex2D (sMap, float2(0.0, 0.0)), Input.Color.a);

tex.rgb *= (1.0 - trackMap.r * 0.99);
//bump.z += trackMap.g * 10.0;
//spec = lerp(spec, float4(0.5, 0.5, 0.5, 1.0), trackMap.g);

  float3 bumpNormal = normalize (float3(bump.x - 0.5, bump.z - 0.5, bump.y - 0.5));

  float whitePaint = saturate((bumpNormal.y - 1.0 + saturate(tex.a - 0.5) * 0.1) * 40.0);
  tex = tex * (1.0 + whitePaint * 0.8);
//  spec = saturate(spec + whitePaint * 0.4);
//  bump.z += whitePaint * 3.0;
  fresnelScale -= whitePaint * 0.4;
//  fresnelPower -= whitePaint * 16.0;

  float3 normal = normalize (Input.Normal.xyz);
  bumpNormal = normalize(2.0 * bumpNormal + normal);
  float3 normEyeVec = normalize (Input.EyeVec);
  float3 lightVec = normalize(worldLight);

  float3 reflected = reflect(normEyeVec, bumpNormal);
  float dotReflect = max(0.0, dot(reflected, lightVec));
  
  half3 specular;
//  specular = (pow(dotReflect, 4.0) + pow(dotReflect, 40.0)) * float3(1.6, 1.4, 1.2);
  specular = pow(dotReflect, 2.0 + trackMap.b * 8.0) * float3(8.0, 7.4, 6.6);

  // Compute diffuse color
  half4 diffuse = saturate ((saturate (dot (lightVec, bumpNormal))) * diffuseColor + ambientColor);
diffuse += Input.Omni;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, Input.Normal.xyz));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = fresnelBias  + fresnelScale * pow(1.0 - dotEyeVecNormal, fresnelPower);

  //less reflection during night
  float day = Input.Normal.w; //diffuseColor.r; //(diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day * diffuseColor.r * trackMap.b;
  
  // Final color
  Out.Color.rgb = (tex.rgb * diffuse.rgb)
     + (specular + float3(0.8, 0.88, 1.0)) * spec.rgb * fresnel;
  Out.Color.a = 1.0;

  return (Out);
}
