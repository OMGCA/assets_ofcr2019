#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float4 Omni   : COLOR1;
#ifdef USEFOG
   float  Fog    : FOG;
#endif   
   float4 Tex0    : TEXCOORD0; // base0 + specular map
   float3 Normal  : TEXCOORD1; // world space vertex normal
   float3 EyeVec  : TEXCOORD2; // world space eye vec
};


VS_OUTPUT vs20_srpl_cockpit03 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL, float2 inTex0 : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inBump : TEXCOORD2)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  Out.Normal = worldNorm;
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec = eyeVec;
  
#ifdef USEFOG
  // compute fog
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif   

  // Compute omnilights
  Out.Omni = (0,0,0,0);

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex0.xy = inTex0;
  Out.Tex0.zw = inSpec;

  return (Out);
}

