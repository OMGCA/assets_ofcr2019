float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};


struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
#if NUMTEX == 1
   float2 Tex    : TEXCOORD0; // base
#else   
   float4 Tex    : TEXCOORD0; // base1 + base2
#endif   
   float2 Bump   : TEXCOORD1; // bump map
   float3 Light  : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;

   float3 Basis1 : TEXCOORD4;
   float3 Basis2 : TEXCOORD5;
   float3 Basis3 : TEXCOORD6;
};

#if NUMTEX == 1

  sampler2D sTex0 : register (s0);
  sampler2D sBump : register (s1);
  samplerCUBE sCube : register (s2);

  #if ADDALPHA == 1
   PS_OUTPUT SRPL_L20_blinnBumpCmapAddAlphaReflectT0 (PS_INPUT Input)
  #else
   PS_OUTPUT SRPL_L20_blinnBumpCmapT0 (PS_INPUT Input)
  #endif 

#else

  sampler2D sTex0 : register (s0);
  sampler2D sTex1 : register (s1);
  sampler2D sBump : register (s2);
  samplerCUBE sCube : register (s3);

  #if ADDALPHA == 1
   PS_OUTPUT SRPL_L20_blinnBumpCmapAddAlphaReflectT0T1 (PS_INPUT Input)   // modulate cube map with T1 alpha
  #elif ADDCOLOR == 1
   PS_OUTPUT SRPL_L20_blinnBumpCmapAddColorReflectT0T1 (PS_INPUT Input)   // modulate cube map with T1 rgb
  #elif ADDCOLOR == 2
   PS_OUTPUT SRPL_L20_blinnBumpCmapAddGrayscaleReflectT0T1 (PS_INPUT Input) // modulate cube map with T1 rgb as grayscale
  #endif 
  
#endif  
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex0 = tex2D (sTex0, Input.Tex.xy);                // sample base tex
  float3 bump = (tex2D (sBump, Input.Bump) - 0.5) * 2.0;  // sample and unbias normal
#if NUMTEX > 1
  float4 tex1 = tex2D (sTex1, Input.Tex.zw);                // sample base tex
#endif

  // now put the normal into eye space
  float3 viewBump;
  viewBump.x = dot (Input.Basis1, bump);
  viewBump.y = dot (Input.Basis2, bump);
  viewBump.z = dot (Input.Basis3, bump);

  // normalize texture space light vector
  float3 normLight = normalize (Input.Light);
  // compute diffuse color (in texture space)
  half4 diffuse = (saturate (dot (normLight, viewBump)) * diffuseColor) + ambientColor + Input.Omni;

  // compute cube map vector [E - 2(E dot N)N] in eye space, then sample the cube map
  float4 cube = texCUBE (sCube, reflect (Input.EyeVec.xyz, viewBump));
  cube.a = 1.0;
  
#if NUMTEX == 1

 #if ADDALPHA == 1
   Out.Color = (tex0 + (cube * tex0.a)) * diffuse * Input.Color;
 #else
   Out.Color = (lerp (tex0, cube, blendPct.a) * diffuse) * Input.Color;
 #endif  

#else

 #if ADDALPHA == 1
   Out.Color = ((tex0 * diffuse) + (cube * tex1.a)) * Input.Color;
 #elif ADDCOLOR == 1
   Out.Color = ((tex0 * diffuse) + (cube * diffuse * tex1)) * Input.Color;
 #elif ADDCOLOR == 2
   const half4 grayScale = {0.30f, 0.59f, 0.11f, 1.0f};
   Out.Color = ((tex0 * diffuse) + (cube * diffuse * dot (tex1, grayScale))) * Input.Color;
 #endif  

#endif  
  return (Out);
}
