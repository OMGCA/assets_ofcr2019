#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

#if PROJTEX == 1
 float4x4 shadowProj : register (c16);
 float4x4 shadowView : register (c20);
 float4x4 shadowViewProj : register (c28);
#endif

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos     : POSITION;
   float4 Diff    : COLOR0;
   float4 Omni    : COLOR1;
#ifdef USEFOG
   float  Fog     : FOG;
#endif   
#if NUMTEX > 1
   float4 Tex0    : TEXCOORD0; // tex0=xy, tex1=zw
   float4 Tex1    : TEXCOORD1; // spec=xy, bump=zw
   float3 Light   : TEXCOORD2;
   float3 EyeVec  : TEXCOORD3; // 
#else
 #if PROJTEX == 1
   float4 Tex0    : TEXCOORD0; // tex0=xy, spec=zw
   float2 Tex1    : TEXCOORD1; // bump=xy
   float3 Tex2    : TEXCOORD2; // proj=xy
   float3 Light   : TEXCOORD3;
   float3 EyeVec  : TEXCOORD4; // 
 #else  
   float4 Tex0    : TEXCOORD0; // tex0=xy, spec=zw
   float2 Tex1    : TEXCOORD1; // bump=xy
   float3 Light   : TEXCOORD2;
   float3 EyeVec  : TEXCOORD3; // 
 #endif  
#endif   

};

#if NUMTEX > 1
  VS_OUTPUT SRPL_L20_blinnBumpSpecMapT0T1 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL, float2 inTex : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inSpec : TEXCOORD2, float2 inBump : TEXCOORD3)
#else 
 #if PROJTEX == 1
  VS_OUTPUT SRPL_L20_blinnBumpRefSpecMapT0 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL, float2 inTex : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inBump : TEXCOORD2)
 #else
  VS_OUTPUT SRPL_L20_blinnBumpSpecMapT0 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL, float2 inTex : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inBump : TEXCOORD2)
 #endif 
#endif 
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldS = mul (worldMatrix, inTangent.xyz);
  float3 worldT = mul (worldMatrix, inBiNorm.xyz);
  float3 worldSxT = mul (worldMatrix, inNorm.xyz);

  //float3 SxT = normalize (cross (inTangent, inBiNorm));
  //if (dot (SxT.xyz, inNorm.xyz) < 0.0)
  //  worldSxT = mul (worldMatrix, -SxT.xyz);
  //else  
  //  worldSxT = mul (worldMatrix, SxT.xyz);
  
  Out.Light.x = dot (dirLight.xyz, worldS.xyz);
  Out.Light.y = dot (dirLight.xyz, worldT.xyz);
  Out.Light.z = dot (dirLight.xyz, worldSxT.xyz);
  
  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  
  Out.EyeVec.x = dot (eyeVec.xyz, worldS.xyz);
  Out.EyeVec.y = dot (eyeVec.xyz, worldT.xyz);
  Out.EyeVec.z = dot (eyeVec.xyz, worldSxT.xyz);

  //float3 halfway = normalize (dirLight - normalize (eyeVec));
  //Out.Halfway.x = dot (halfway.xyz, worldS.xyz);
  //Out.Halfway.y = dot (halfway.xyz, worldT.xyz);
  //Out.Halfway.z = dot (halfway.xyz, worldSxT.xyz);

 #if PROJTEX == 1
  // projected texture coordinates
  Out.Tex2 = mul (shadowViewProj, float4 (worldPos.xyz, 1));
 #endif
  
#ifdef USEFOG
  // compute fog
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif   

  // Compute omnilights
  Out.Omni = float4 (0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
        
    Out.Omni.a = 1.0F;    
  }
  Out.Omni.rgb *= 0.5F;
  //else if (numSpotLight.x > 0)
  //{
  //  // Compute spotlights
  //  float4 spotColor = (0,0,0,0);
  //  float3 worldNorm = mul (worldMatrix, inNorm);
  //
  //  for (int i=0; i<numSpotLight.x; i++)
  //  {
  //    // check backface
  //    if (dot (worldNorm, spotDirCone[i].xyz) <= 0)
  //    {
  //      float3 spotToVert = worldPos.xyz - spotPosRad[i].xyz;
  //      float mag = dot (spotToVert.xyz, spotToVert.xyz);
  //      // check if vert in range
  //      if (mag < spotPosRad[i].w) // using square distances
  //      {
  //        // check if vert in cone
  //        mag = dot (normalize(spotToVert.xyz), spotDirCone[i].xyz);
  //        if (mag > spotDirCone[i].w)
  //        {
  //          float intensity = (1.0F - ( mag * spotRGBDelta[i].w));
  //          spotColor.rgb += (spotRGBDelta[i].rgb * intensity);
  //        }  
  //      }
  //    }
  //  }
  //  Out.Omni = spotColor;
  //}

  // Propagate color and texture coordinates:
  Out.Diff    = inColor;
  Out.Tex0.xy = inTex;
#if NUMTEX > 1
  Out.Tex0.zw = inTex1;
  Out.Tex1.xy = inSpec;
  Out.Tex1.zw = inBump;
#else
  Out.Tex0.zw = inSpec;
  Out.Tex1.xy = inBump;
#endif  
  return (Out);
}

