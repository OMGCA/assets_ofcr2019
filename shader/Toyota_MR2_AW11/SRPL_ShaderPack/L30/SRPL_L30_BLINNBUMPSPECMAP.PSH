float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.w
float4 blendPct      : register (c4); //bump map blend in c4.z, cube map blend in c4.w

#define exposition(op1, op2) float4(1.2, 1.2, 1.2, (op1).a) - pow(float4(2.7, 2.7, 2.7, 0.0), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color   : COLOR0;
   float4 Omni    : COLOR1;
   float  Fog     : FOG;
#if LERPALPHA == 1 || MULTALPHA == 1
   float4 Tex0    : TEXCOORD0; // base0=xy, base1=zw
   float4 Tex1    : TEXCOORD1; // spec=xy, bump=zw
 #if PROJTEX == 1
    float3 Tex2    : TEXCOORD2; // proj=xy
    float3 Light   : TEXCOORD3;
    float4 EyeVec  : TEXCOORD4;
 #else 
    float3 Light   : TEXCOORD2;
    float4 EyeVec  : TEXCOORD3;
 #endif
};
#else   
   float4 Tex0    : TEXCOORD0; // base0=xy, spec=zw
   float2 Tex1    : TEXCOORD1; // bump=xy
   float3 Light   : TEXCOORD2;
   float4 EyeVec  : TEXCOORD3;
};
#endif   


struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sBase:   register (s0);

#if LERPALPHA == 1 || MULTALPHA == 1

 sampler2D sBase1: register (s1);
 sampler2D sSpec:  register (s2);
 sampler2D sBump:  register (s3);

 #if MULTALPHA == 1
  #if PROJTEX == 1
    sampler2D sRef:  register (s4);
    PS_OUTPUT SRPL_L30_blinnBumpRefSpecMapT0T1MatAlphaVertexAlpha (PS_INPUT Input)
  #else
   PS_OUTPUT SRPL_L30_blinnBumpSpecMapT0T1MatAlphaVertexAlpha (PS_INPUT Input)
  #endif 
 #elif LERPALPHA == 1
  PS_OUTPUT SRPL_L30_blinnBumpSpecMapT0LerpT1VertexAlpha (PS_INPUT Input)
 #endif
 
#elif LERP == 1

 sampler2D sSpec:  register (s1);
 sampler2D sBump:  register (s2);

 sampler2D sBase1: register (s3);
 sampler2D sSpec1: register (s4);
 sampler2D sBump1: register (s5);

 PS_OUTPUT SRPL_L30_blinnBumpSpecMapLerpT0 (PS_INPUT Input)

#else

 sampler2D sSpec:  register (s1);
 sampler2D sBump:  register (s2);

 PS_OUTPUT SRPL_L30_blinnBumpSpecMapT0 (PS_INPUT Input)

#endif 
{
  PS_OUTPUT Out;
  
  float4 basePixel  = tex2D (sBase, Input.Tex0.xy);  // sample base tex
  
#if LERPALPHA == 1 || MULTALPHA == 1

  float4 base1Pixel = tex2D (sBase1, Input.Tex0.zw);  // sample base tex
  float4 specPixel  = tex2D (sSpec, Input.Tex1.xy);  // sample spec tex (gloss)
  float3 bumpPixel  = (tex2D (sBump, Input.Tex1.zw) - 0.5) * 2.0;  // sample and unbias normal

 #if LERPALPHA == 1
   basePixel = lerp (base1Pixel, basePixel, Input.Color.a);
 #elif MULTALPHA == 1
  #if PROJTEX == 1
   float2 mid = Input.Tex2.xy / Input.Tex2.z;
   float4 refPixel = tex2D (sRef, mid);  // sample proj reflection tex
   //float4 refPixel = tex2D (sRef, Input.Tex2.xy);  // sample proj reflection tex
  #else
   basePixel.rgb = basePixel.rgb * lerp (base1Pixel.rgb, 1.0f, ambientColor.a);
   //basePixel.a = lerp (base1Pixel.a, 1.0f, ambientColor.a);
  #endif 
 #endif  

#else

  float4 specPixel  = tex2D (sSpec, Input.Tex0.zw);  // sample spec tex (gloss)
  float3 bumpPixel  = (tex2D (sBump, Input.Tex1.xy) - 0.5) * 2.0;  // sample and unbias normal

#endif

#if LERP == 1

  float4 base1Pixel = tex2D (sBase1, Input.Tex0.xy);  //
  float4 spec1Pixel = tex2D (sSpec1, Input.Tex0.zw);  //
  float3 bump1Pixel = (tex2D (sBump1, Input.Tex1.xy) - 0.5) * 2.0;  // sample and unbias normal

  basePixel = lerp (base1Pixel, basePixel, blendPct.z);
  specPixel = lerp (spec1Pixel, specPixel, blendPct.z);
  bumpPixel = lerp (bump1Pixel, bumpPixel, blendPct.z);
  
#endif

  float3 normLight = (Input.Light);
  
  // compute specular color
  float3 halfway =  normalize (normLight -  normalize (Input.EyeVec.xyz)); 
  
  half4 specular;
  specular.rgb = saturate ((pow (saturate (dot (halfway, bumpPixel)), specularColor.a) * specularColor) + Input.Omni) * specPixel;
  //float specAngle = saturate (dot (halfway, bumpPixel));
  //half4 specC = pow (specAngle, specularColor.a) * specularColor;
  //specular.rgb = saturate (specC + Input.Omni) * specPixel;
  specular.a = 0;

  // compute diffuse color
  half4 diffuse;
  diffuse = ((saturate (dot (normLight, bumpPixel)) * diffuseColor * float4(1.65, 1.65, 1.65, 1.0)) + ambientColor + Input.Omni) * basePixel;
 #if MULTALPHA == 1
  diffuse.a = lerp (base1Pixel.a, 1.0f, ambientColor.a);
 #endif 
  
  #if PROJTEX == 1
  // DEBUG
  Out.Color = exposition((((basePixel * diffuse) + specular) + (refPixel * diffuse * blendPct.a)) * Input.Color, Input.EyeVec.w);
  //Out.Color = refPixel;
  #else
  // DEBUG
  // diffuse ADD specular, unless Omni > 0, then diffuse ADDSMOOTH specular (Omni.a = 1.0 if there is omni)
  Out.Color = exposition(((diffuse + specular) - (diffuse * specular * Input.Omni.a)) * Input.Color, Input.EyeVec.w);
  //Out.Color = (specular + diffuse) * Input.Color;
  #endif

  return (Out);
}
