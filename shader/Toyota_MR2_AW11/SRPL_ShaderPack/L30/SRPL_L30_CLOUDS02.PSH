float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 dirLight : register (c14);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

#define exposition(op1, op2) float3(1.05, 1.05, 1.05) - pow(float3(2.7, 2.7, 2.7), op1 * op2) * 1.05

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Texc0  : TEXCOORD0;
   float4 Texc1  : TEXCOORD1;
   float4 Texc2  : TEXCOORD2;
   float4 EyeVec : TEXCOORD3;
   float  Expos  : TEXCOORD4;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sTex1 : register (s1);
sampler2D sTex2 : register (s2);


PS_OUTPUT SRPL_L30_CLOUDS02 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Sample the textures
  float tex1 = tex2D (sTex0, Input.Texc0.xy).r;
  float tex2 = tex2D (sTex0, Input.Texc0.zw).r;
  float tex3 = tex2D (sTex0, Input.Texc1.xy).r;

  float value = tex1 * 0.45 + tex2 * 0.29 + tex3 * 0.18;
  value -= 0.275 * pow(Input.EyeVec.w, 0.5);

  if (value < 0.38)
  {
    discard;
  }

  float tex4 = tex2D (sTex0, Input.Texc1.zw).r;
  float tex5 = tex2D (sTex0, Input.Texc2.xy).r;
  float tex6 = tex2D (sTex0, Input.Texc2.zw).r;

  value +=  tex4 * 0.08 + tex5 * 0.06 + tex6 * 0.03;

  float4 cloudColor = tex2D( sTex1, float2(value, diffuseColor.b));

  cloudColor.a = tex2D(sTex2, float2(value, diffuseColor.r)).a;

  float3 normEyeVec = normalize (Input.EyeVec.xyz);
  float3 LightVec = normalize(worldLight.xyz);
  float sun = max(dot(LightVec, normEyeVec), 0.0);

  sun = saturate(pow(sun, 2.0) - cloudColor.a * 0.98) * 6.0;
  cloudColor.rgb += saturate(diffuseColor.rgb - float3(0.5, 0.5, 0.5)) * sun * diffuseColor.b;

  cloudColor.a *= min(Input.EyeVec.w * 6.0, 1.0) * 1.5;

  cloudColor.rgb *= 2.85;
  cloudColor.rgb = exposition(cloudColor.rgb, Input.Expos);

  Out.Color = cloudColor;

  return (Out);
}
