#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 sunLight : register (c13);
int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float4 Omni   : COLOR1;
#ifdef USEFOG
   float  Fog    : FOG;
#endif   
#if NUMTEX > 3
   float4 Tex0    : TEXCOORD0; // base0 + base1
   float4 Tex1    : TEXCOORD1; // base2 + base3
   float2 Spec    : TEXCOORD2; // specular map
   float3 Normal  : TEXCOORD3; // world space vertex normal
   float4 EyeVec  : TEXCOORD4; // world space eye vec
#elif NUMTEX > 2
   float4 Tex0    : TEXCOORD0; // base0 + base1
   float4 Tex1    : TEXCOORD1; // base2 + spec map
   float3 Normal  : TEXCOORD2; // world space vertex normal
   float4 EyeVec  : TEXCOORD3; // world space eye vec
#elif NUMTEX > 1   
   float4 Tex0    : TEXCOORD0; // base0 + base1
   float2 Spec    : TEXCOORD1; // specular map
   float3 Normal  : TEXCOORD2; // world space vertex normal
   float4 EyeVec  : TEXCOORD3; // world space eye vec
#else   
   float4 Tex0    : TEXCOORD0; // base0 + specular map
   float3 Normal  : TEXCOORD1; // world space vertex normal
   float4 EyeVec  : TEXCOORD2; // world space eye vec
#endif   
};

#if NUMTEX > 3
 VS_OUTPUT SRPL_L30_blinnSpecMapT0T1T2T3 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2, float2 inTex3 : TEXCOORD3, float2 inSpec : TEXCOORD4)
#elif NUMTEX > 2
 VS_OUTPUT SRPL_L30_blinnSpecMapT0T1T2 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2, float2 inSpec : TEXCOORD3)
#elif NUMTEX > 1
 VS_OUTPUT SRPL_L30_blinnSpecMapT0T1 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inSpec : TEXCOORD2)
#else
 VS_OUTPUT SRPL_L30_blinnSpecMapT0 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inSpec : TEXCOORD1)
#endif
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  Out.Normal = worldNorm;
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec.xyz = eyeVec;

//exposition
float lightDist = distance(normalize(mul(viewProjMatrix, normalize(dirLight)).xyz), float3(0.0, 0.0, 1.0)); //0.0 - looking at sun, 2.0 - looking away from sun
lightDist = pow(lightDist / 2.0, 1.3);
Out.EyeVec.w = lerp(-1.3, lightDist * -1.3 - 0.5, saturate(sunLight.g * 2.0 - 0.3));
  
#ifdef USEFOG
  // compute fog
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif   

  // Compute omnilights
  Out.Omni = (0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }
  Out.Omni.rgb *= 0.5F;
  //else if (numSpotLight.x > 0)
  //{
  //  // Compute spotlights
  //  float4 spotColor = (0,0,0,0);
  //  float3 worldNorm = mul (worldMatrix, inNorm);
  //
  //  for (int i=0; i<numSpotLight.x; i++)
  //  {
  //    // check backface
  //    if (dot (worldNorm, spotDirCone[i].xyz) <= 0)
  //    {
  //      float3 spotToVert = worldPos.xyz - spotPosRad[i].xyz;
  //      float mag = dot (spotToVert.xyz, spotToVert.xyz);
  //      // check if vert in range
  //      if (mag < spotPosRad[i].w) // using square distances
  //      {
  //        // check if vert in cone
  //        mag = dot (normalize(spotToVert.xyz), spotDirCone[i].xyz);
  //        if (mag > spotDirCone[i].w)
  //        {
  //          float intensity = (1.0F - ( mag * spotRGBDelta[i].w));
  //          spotColor.rgb += (spotRGBDelta[i].rgb * intensity);
  //        }  
  //      }
  //    }
  //  }
  //  Out.Omni = spotColor;
  //}

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
#if NUMTEX > 3
  Out.Tex0.xy = inTex0;
  Out.Tex0.zw = inTex1;
  Out.Tex1.xy = inTex2;
  Out.Tex1.zw = inTex3;
  Out.Spec    = inSpec;
#elif NUMTEX > 2
  Out.Tex0.xy = inTex0;
  Out.Tex0.zw = inTex1;
  Out.Tex1.xy = inTex2;
  Out.Tex1.zw = inSpec;
#elif NUMTEX > 1
  Out.Tex0.xy = inTex0;
  Out.Tex0.zw = inTex1;
  Out.Spec.xy = inSpec;
#else
  Out.Tex0.xy = inTex0;
  Out.Tex0.zw = inSpec;
#endif  

  return (Out);
}

