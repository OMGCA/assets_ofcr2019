float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 sunLight : register (c13);
float4 ambLight : register (c12);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;

   float4 Color1  : TEXCOORD1; //
   float4 Color2  : TEXCOORD2; //

   float  Fog    : FOG;
   float2 Tex0    : TEXCOORD0; // base0
};


VS_OUTPUT SRPL_L25_GRASSBLADES_T0MIX (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0)
{
  VS_OUTPUT Out;
  
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = normalize(mul (worldMatrix, inNorm.xyz));
  float3 lightVec = normalize(dirLight.xyz);
  float dotLightVec = saturate(dot(worldNorm, lightVec));

  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = normalize(worldEye.xyz + worldPos.xyz);


  float2 lbDot = float2( dot(lightVec.xz, eyeVec.xz) * 0.6 + 0.4, dot(lightVec, eyeVec));
  lbDot = pow(max(lbDot, float2(0.0, 0.0)), float2(2.0, 15.0));
  float lightBehind = dot(lbDot, float2(0.5, 0.5));

  
  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));


  float4 omni = float4(0.0, 0.0, 0.0, 0.0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }


  Out.Color1 = (sunLight * 1.5 * dotLightVec + ambLight + omni) * inColor * (1.0 - lightBehind);
  Out.Color1.a = 1.0;

  Out.Color2 = (sunLight.g * 1.15 + ambLight.r) * inColor * lightBehind;
  Out.Color2.a = 0.0;

  // Propagate color and texture coordinates:
  Out.Tex0.xy = inTex0;

  return (Out);
}

