
float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 ambLight : register (c12);
float4 sunLight : register (c13);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog    : FOG;
   float4 Texc0  : TEXCOORD0; // base / specaulr and bump
   float4 Texc1  : TEXCOORD1; // aiw / skid
   float4 Normal : TEXCOORD2; // world space vertex normal + day factor
   float3 EyeVec : TEXCOORD3; // world space eye vec
   float4 specularPower : TEXCOORD4;
   float3 Normal2 : TEXCOORD5;
   float4 fresnelScale  : TEXCOORD6;
   float4 fresnelPower  : TEXCOORD7;

   float4 Omni   : COLOR1;
};





//  Encoding in GMT:
//
//  Pos = Pos
//  Normal = Normal
//  Color = Color
//  TexCoord0 = base tex coord  
//  TexCoord1 = aiw coord and skidmark X coord (skidmark y coord is based on AIW coord)
//              note that now only fragment of AIW texture will be used,
//              only as long to get desired number of skid texture repeats
//  
//  What more do we need?
//  - 16 floats for fresnel and specular parameters
//
//  what we have?
//  - TexCoord2 and 3 = 4 floats
//  - Tangent and binormal = 6 floats
//
//  - 6 more needed
//  - 4 could be passed as 1x1 texture (preferably fresnel base)
//
//  That leaves 2 still needed, but preferably 3 to get skid texture mapping independent from aiw mapping
//
//  We have: Color.a - that could be used to scale skid texture mapping
//
//  now there's specular power in c3.a and cubemap blend in c4.a
//  
//
//

// Encoding:
//
// Vertex:
//  TexCoord0 = base tex coord
//  TexCoord1.x = aiw coord
//  TexCoord1.y = skid X coord
//  TexCoord2 = fresnelscale1-2
//  TexCoord3 = specularPower1-2
//  Tangent.xy = specularPower3-4
//  Tangent.z = fresnelScale3
//  Binormal = fresnelPower1-3
//  inColor.a = (8-bit) fresnelPower4 (convert to 0.0 - 25.5 range)
//  length(inNormal) - fresnelScale4
//  
// Pixel:
//  tex6 = fresnelBase1-4




VS_OUTPUT SRPL_L25_ROAD_T0SPECBUMPSKID03 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0,
                                          float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL,
                                          float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2, float2 inTex3 : TEXCOORD3)
{
  VS_OUTPUT Out;
  


  float4 fresnelScale;
  fresnelScale.xy = abs(inTex2);
  fresnelScale.z = abs(inTangent.z);
  fresnelScale.w = length(inNorm.xyz);

  fresnelScale *= saturate(sunLight.b * 1.5);
  Out.fresnelScale = fresnelScale;

  Out.specularPower.xy = abs(inTex3);
  Out.specularPower.zw = abs(inTangent.xy);

  Out.fresnelPower.w = inColor.a * 25.5;
  Out.fresnelPower.xyz = abs(inBiNorm);

  float skidUVScale = 0.0;
  skidUVScale += saturate(sign(-inTex2.x));
  skidUVScale += saturate(sign(-inTex2.y)) * 2.0;
  skidUVScale += saturate(sign(-inTex3.x)) * 4.0;
  skidUVScale += saturate(sign(-inTex3.y)) * 8.0;
  skidUVScale += saturate(sign(-inTangent.x)) * 16.0;
  skidUVScale += saturate(sign(-inTangent.y)) * 32.0;
  skidUVScale += saturate(sign(-inTangent.z)) * 64.0;
  skidUVScale += saturate(sign(-inBiNorm.x)) * 128.0;
  skidUVScale += saturate(sign(-inBiNorm.y)) * 256.0;
  skidUVScale += saturate(sign(-inBiNorm.z)) * 512.0;
  if (skidUVScale == 0.0)
  {
    skidUVScale = 100.0;    //100.0 is the default number of times skidmark texture is repeated along entire track
  }


  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, normalize(inNorm.xyz));

  //daytime
  Out.Normal.w = saturate(normalize(dirLight.xyz).y * 1.6 + 0.1);
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);

  float3 worldZenith = mul (worldMatrix, float3(0.0, 1.0, 0.0));
  float horizon = pow(1.0 - saturate(dot(reflect(normalize(eyeVec), worldNorm), worldZenith)), 15.0);

//  Out.Diff =   ambLight.g * float4(0.02, 0.04, 0.09, 0.0)
//             + ambLight.g * float4(0.11, 0.11, 0.09, 0.0) * horizon;
  Out.Diff =   sunLight.b * float4(0.01, 0.03, 0.08, 0.0)
             + sunLight.b * float4(0.06, 0.05, 0.02, 0.0) * horizon;


  float3 normal = normalize(worldNorm);

  Out.Normal.xyz = normal;

  Out.EyeVec = eyeVec;
  
  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

  // Compute omnilights
  Out.Omni = float4(0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }
  Out.Omni.a = 1.0F;    
  //Out.Omni.rgb *= 0.5F;

  // we subtract 0.5 now, so we save one instruction in pixel shader
  // additionally we scale normal vector by it's xz length so vertical vector doesn't contribute to final vector
  // while at higher slopes it becomes more dominant
  // there were not enough instructions for proper tangent space calculations so normals are simply added in pixel shader
  // this scaling keeps bumpmap accurate on flat roads while at higher slopes we don't loose the general lighting
  Out.Normal2.xyz = normal * saturate(length(normal.xz) * 5.0) - float3(0.5, 0.5, 0.5);
  

  Out.Texc0.xy = inTex0;
  Out.Texc0.zw = inPos.xz * 0.5;
  Out.Texc1.xy = float2(inTex1.x * 64.0, -inTex1.x - 1.0 / 128.0);
  Out.Texc1.zw = float2(inTex1.y, inTex1.x * skidUVScale);

  return (Out);
}

