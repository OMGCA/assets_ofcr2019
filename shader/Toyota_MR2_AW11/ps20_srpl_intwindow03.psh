float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;

   float2 Tex    : TEXCOORD0; // base
   float2 Spec   : TEXCOORD1; // spec
   float4 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex : register (s0);
sampler2D sSpec : register (s1);
sampler2D sBump : register (s2);
samplerCUBE sCube : register (s3);

//sSpec:
//R - glass visibility (dirtness/thickness)
//G - window detail texture
//B - greyscale diffuse color of elements
//A - how much of solid elements (overwrite glass)

PS_OUTPUT ps20_srpl_intwindow03 (PS_INPUT Input)
{
  PS_OUTPUT Out;

  // sample the textures
  float4 diff = float4(0.0, 0.0, 0.0, 0.0);
  float4 spec = tex2D (sSpec, Input.Tex);             // sample parameters tex

  float details = max(tex2D (sSpec, Input.Tex * 15.0).a * 1.75 - 0.65, 0.0);

  float day = max(diffuseColor.r + diffuseColor.g + diffuseColor.b - 1.0, 0.0) / 2.0;
  spec.r *= day;

  float3 normal = normalize (-1.0 * Input.Normal.xyz);
  float3 normEyeVec = normalize (Input.EyeVec);
  

  // compute glass color 
  float specular;


  specular = max(dot (worldLight.xyz, normEyeVec), 0.0);
  specular = pow(specular, 3.0 - details * 3.0);

  float4 finalColor;
  finalColor.rgb = float3(3.5, 2.8, 1.5) * specular + float3(1.2, 1.4, 1.8);
//!  finalColor.a = (specular * 0.6 + 0.2) * pow(details * 3.0, 2.0); //specular * 1.0 + details * 1.3;
  finalColor.a = specular * 1.0 + details * 1.3; //!
  finalColor.a *= spec.r;

  spec.b *= max(-1.0 * dot(worldLight, normal), 0.0) * 0.8 + 0.2;

  finalColor.rgb = lerp(finalColor.rgb, float3(0.5, 0.5, 0.5) * spec.b, step(0.1, spec.g)); //!
  finalColor.a = lerp(finalColor.a, 1.0, spec.g);
  finalColor.rgba = lerp(saturate(finalColor.rgba), diff.rgba, pow(diff.a, 3.0));

  Out.Color = finalColor;

  return (Out);
}
