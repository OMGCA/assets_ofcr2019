#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye: register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
int4 numOmniLight : register (i0); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos   : POSITION;
   float4 Diff  : COLOR0;
   float4 Omni  : COLOR1;
#ifdef USEFOG
   float  Fog   : FOG;
#endif   
#if NUMTEX == 1
   float2 Tex    : TEXCOORD0; // base
#else   
   float4 Tex    : TEXCOORD0; // base
#endif   
   float2 Bump   : TEXCOORD1; // bump map
   float3 Light  : TEXCOORD2; // tangent space light
   float3 EyeVec : TEXCOORD3; // tangent space eye vec
};

#if NUMTEX > 1   
 VS_OUTPUT vs20_blinnBumpT0T1 (float4 inPos : POSITION, float3 inNorm : NORMAL, float4 inColor : COLOR0, float3 inTangent : TANGENT, float3 inBiNorm : BINORMAL, float2 inTex0 : TEXCOORD0,  float2 inTex1 : TEXCOORD1, float2 inBump : TEXCOORD2)
#else 
 VS_OUTPUT vs20_blinnBumpT0 (float4 inPos : POSITION, float3 inNorm : NORMAL, float4 inColor : COLOR0, float3 inTangent : TANGENT, float3 inBiNorm : BINORMAL, float2 inTex0 : TEXCOORD0, float2 inBump : TEXCOORD1)
#endif 
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldS;
  float3 worldT;
  float3 worldSxT;
  worldS.xyz = mul (worldMatrix, inTangent.xyz);
  worldT.xyz = mul (worldMatrix, inBiNorm.xyz);
  worldSxT.xyz = mul (worldMatrix, inNorm.xyz);
  
  Out.Light.x = dot (dirLight.xyz, worldS.xyz);
  Out.Light.y = dot (dirLight.xyz, worldT.xyz);
  Out.Light.z = dot (dirLight.xyz, worldSxT.xyz);
  
  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  
  // pass eye vec to pixel shader: E - 2(E dot N)N
  Out.EyeVec.x = dot (eyeVec.xyz, worldS.xyz);
  Out.EyeVec.y = dot (eyeVec.xyz, worldT.xyz);
  Out.EyeVec.z = dot (eyeVec.xyz, worldSxT.xyz);
  
#ifdef USEFOG
  // compute fog
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif   
  
  // Compute omnilights
  Out.Omni = (0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }
  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex0;
#if NUMTEX > 1  
  Out.Tex.zw = inTex1;
#endif  
  Out.Bump = inBump;

  return (Out);
}

