float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
   float4 Tex    : TEXCOORD0; // base
   float3 Normal : TEXCOORD1;
   float3 EyeVec : TEXCOORD2;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
samplerCUBE sCube : register (s1);

PS_OUTPUT ps20_blinnFresnelCmapAlphaSpecT0 (PS_INPUT Input)
{
  PS_OUTPUT Out;

  // Fresnel parameters
  float fresnelScale = 5.0;
  float fresnelPower = 10.0;

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);
  
  float3 normal = normalize (Input.Normal);
  float3 normEyeVec = normalize (Input.EyeVec);
  
  float3 halfway = normalize (worldLight - normEyeVec); 
  half4 specular;
  specular.rgb = saturate (pow (dot (halfway, normal), 15.0)) * specularColor.rgb * 0.5 * tex.a; // add metallic
  specular.rgb += saturate (pow (dot (halfway, normal), specularColor.a * specularColor.a * 0.5)) * specularColor.rgb * tex.a;
  specular.a = 0;

  // Compute diffuse color
  half4 diffuse = saturate ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor + Input.Omni);

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float4 cube = texCUBE (sCube, reflect (normEyeVec, normal));
  cube.a = 1.0;
  
  // Use the specular alpha for reflection amount and add fresnel to that
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));
  float fresnel = 1.0 + fresnelScale * pow(1 - dotEyeVecNormal, fresnelPower);
  float reflectionFactor = min(fresnel * tex.a * tex.a, 1.0);

  // Cube diffusion
  float cubeDiffuse = diffuseColor + (Input.Omni.r * 0.333) + (Input.Omni.g * 0.333) + (Input.Omni.b * 0.333);

  // Final color
  Out.Color = ((tex * diffuse) + specular + (cube * cubeDiffuse * reflectionFactor)) * Input.Color;
  Out.Color.a = tex.a;
  return (Out);
}
