float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3);

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
#if NUMTEX == 1
   float2 Tex    : TEXCOORD0; // base
#else   
   float4 Tex    : TEXCOORD0; // base
#endif   
   float2 Bump   : TEXCOORD1; // bump map
   float3 Light  : TEXCOORD2;
   float3 EyeVec : TEXCOORD3; // tangent space eye vec
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

 sampler2D sTex0 : register (s0);

#if NUMTEX == 1

 sampler2D sBump : register (s1);

 PS_OUTPUT ps20_blinnBumpSpecT0Custom (PS_INPUT Input)

#else 

 sampler2D sTex1 : register (s1);
 sampler2D sBump : register (s2);

 #if MULTEX == 0
  PS_OUTPUT ps20_blinnBumpSpecT0T1 (PS_INPUT Input)
 #else 
  PS_OUTPUT ps20_blinnBumpSpecT0xT1 (PS_INPUT Input)
 #endif 

#endif
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);  // sample base tex

#if NUMTEX > 1

  float4 tex1 = tex2D (sTex1, Input.Tex.zw); // sample base tex

 #if MULTEX == 0
   tex = saturate (tex + tex1);
 #elif MULTEX == 1 
   tex = (tex * tex1);
 #endif

#endif
  
  float3 bumpPixel = (tex2D (sBump, Input.Bump) - 0.5) * 2.0;  // sample and unbias normal
  float3 normLight = normalize (Input.Light);
  
  // compute specular color
  float3 halfway = normalize (normLight - normalize (Input.EyeVec)); 
  float4 specular;
  specular.rgb = saturate ((pow (dot (halfway, bumpPixel), specularColor.a) * specularColor));
  //specular.rgb = saturate ((pow (dot (halfway, bumpPixel), specularColor.a) * specularColor) + Input.Omni);
  specular.a = 0;

  // compute diffuse color
  float4 diffuse = saturate (((saturate (dot (normLight, bumpPixel))) * diffuseColor) + ambientColor + Input.Omni) * tex;
  
  Out.Color = (diffuse + specular) * Input.Color;
  return (Out);
}
