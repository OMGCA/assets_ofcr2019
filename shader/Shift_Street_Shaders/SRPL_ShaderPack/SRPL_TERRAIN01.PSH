float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;
   float4 TexCoord01    : TEXCOORD0;    // Map, Layer 1
   float4 TexCoord23    : TEXCOORD1;    // Layer 2, Layer 3
   float3 Normal        : TEXCOORD2;    // xyz - normal
   float3 EyeVec        : TEXCOORD4;    // xyz - eyeVec
   float3 Tangent       : TEXCOORD5;    // xyz - tangent
   float3 Binormal      : TEXCOORD6;    // xyz - binormal
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0   : register (s1);
sampler2D sDiff1  : register (s0); //this one is at S0 so it's also used with headlights
sampler2D sNorm1  : register (s2);
sampler2D sDiff2  : register (s3);
sampler2D sNorm2  : register (s4);
sampler2D sDiff3  : register (s5);
sampler2D sNorm3  : register (s6);

PS_OUTPUT SRPL_TERRAIN01 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  float4 mapTex =  tex2D(sTex0, Input.TexCoord01.xy);

  float3x4 diff;
  diff[0] = tex2D(sDiff1, Input.TexCoord01.zw);
  diff[1] = tex2D(sDiff2, Input.TexCoord23.xy);
  diff[2] = tex2D(sDiff3, Input.TexCoord23.zw);

  float3x4 bump;
  bump[0] = tex2D(sNorm1, Input.TexCoord01.zw);
  bump[1] = tex2D(sNorm2, Input.TexCoord23.xy);
  bump[2] = tex2D(sNorm3, Input.TexCoord23.zw);

  float3 mix = mapTex.rgb;
  mix.r += bump[0].a;
  mix.g += bump[1].a;
  mix.b += bump[2].a;

  float ma = max(max(mix.r, mix.g), mix.b);
  mix.r = step(ma, mix.r);
  mix.g = step(ma, mix.g);
  mix.b = step(ma, mix.b);
//  mix.rgb /= (mix.r + mix.g + mix.b);   // more components having a value of 1.0 is very unlikely to happen, skip this protection


  float4 color = mul(mix, diff);
  float3 bumpVec = mul(mix, bump) - float3(0.5, 0.5, 0.5);

  float3 normal = normalize(  bumpVec.x * Input.Tangent
                            + bumpVec.y * Input.Binormal
                            + bumpVec.z * Input.Normal);

  // Compute diffuse color
  float4 diffuse = saturate(dot (worldLight, normal)) * diffuseColor + ambientColor;

#ifdef OMNISCALE
  diffuse = diffuse + Input.Omni;
#endif

  color = color * diffuse * mapTex.a;
  Out.Color = color;

  return (Out);
}
