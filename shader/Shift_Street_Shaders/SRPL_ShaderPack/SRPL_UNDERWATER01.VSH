#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 worldLight    : register (c2);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);




struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog   : FOG;

   float3 Tex    : TEXCOORD0; // Texcoord
   float4 Tex0   : TEXCOORD1;
   float4 Tex1   : TEXCOORD2;
  
   float3 Normal : TEXCOORD3;
   float3 Light  : TEXCOORD4;
   float3 EyeVec : TEXCOORD5;
   float4 WaterParams : TEXCOORD6;
};

VS_OUTPUT MAINFUNCTION (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex : TEXCOORD0,
                        float2 inParams1 : TEXCOORD1, float2 inParams2 : TEXCOORD2)
{
  VS_OUTPUT Out;
  
  Out.WaterParams.xy = inParams1.xy;
  Out.WaterParams.zw = inParams2.xy;


  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldPos = mul (worldMatrix, inPos);
  
  Out.Light = dirLight;
  Out.EyeVec = worldPos - invViewMatrix[3];

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  float3 eyeVec = (worldEye + worldPos.xyz);

  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex;
  Out.Normal = worldNorm;
  Out.Tex.z = inParams1.x - inPos.y;


  float2 offset = mul(worldMatrix, inPos).xz;
  offset = lerp(offset, mul(worldMatrix, inPos).xy, step(0.707, abs(inNorm.z)));
  offset = lerp(offset, mul(worldMatrix, inPos).yz, step(0.707, abs(inNorm.x)));

////////////////////////////////////////
float2 vec1 = normalize(dirLight.rb);

float angle;

  if (vec1.x == 0)
  {
    if (vec1.y > 0)
    {
      angle = 0.0;
    }
    else
    {
      angle = 3.1415926535;
    }
  }

  if (vec1.y == 0)
  {
    if (vec1.x > 0)
    {
      angle = 0.5 * 3.1415926535;
    }
    else
    {
      angle = 1.5 * 3.1415926535;
    }
  }

  if (vec1.y > 0)
  {
    angle = atan(vec1.x / vec1.y);
  }
  else
  {
    angle = 3.1415926535 + atan(vec1.x / vec1.y);
  }
  float animation = angle / 6.2832;
////////////////////////////////////////
  animation *= 1440.0; //1 full cycle per 1 minute

  Out.Tex0.xy = offset * 0.012 + float2( 0.26, 0.42) * animation;
  Out.Tex0.zw = offset * 0.010 - float2( 0.41, 0.38) * animation;
  Out.Tex1.xy = offset * 0.008 + float2( 0.33, 0.25) * animation;
  Out.Tex1.zw = offset * 0.006 - float2( 0.3,  0.2 ) * animation;


  return (Out);
}
