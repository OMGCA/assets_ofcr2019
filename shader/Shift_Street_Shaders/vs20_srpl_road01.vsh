
float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 dirLight : register (c14);
float4 worldLight    : register (c2);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float2 Tex    : TEXCOORD0; // base
   float4 Spec   : TEXCOORD1; // spec and track map
   float4 Normal : TEXCOORD2; // world space vertex normal + sign of reflection
   float3 EyeVec : TEXCOORD3; // world space eye vec
};

  VS_OUTPUT vs20_srpl_road01 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inSpec2 : TEXCOORD3)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  Out.Normal.xyz = worldNorm;
  Out.Normal.w = worldMatrix[1][1];
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec = eyeVec;
  
  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex0;
  Out.Spec.xy = inSpec;
  Out.Spec.zw = inSpec2;

  return (Out);
}

