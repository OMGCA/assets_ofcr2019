// Pixel shader for light flares 
// -----------------------------
// Created by Siim Annuk
// Date: June 28, 2009; Update: October 13, 2013

float4 ambientColor  : register (c0);
//float4 specularColor : register (c15); // power in c15.a
float4 diffuseColor  : register (c1);
float4 specularColor : register (c3); //power in c3.a

sampler2D sTex0 : register (s0);

#if LIGHTFLARE_VARIANT == 2
sampler2D sTex1 : register (s1);
#endif

struct PS_INPUT
{
  float2  Tex0: TEXCOORD0; // xy=map0, zw=map1
  float2 Angle: TEXCOORD1; // x = normal glow angle, y = high glare angle
  
#if BILLBOARD == 1
  float  Depth: TEXCOORD2;
#endif
};

struct PS_OUTPUT
{
  float4 Color : COLOR0;
#if BILLBOARD == 1
  float  Depth : DEPTH;
#endif
};

float grayScale(float4 v) 
{
    return v.x * 0.299 + v.y * 0.587 + v.z * 0.114;
}

#if LIGHTFLARE_VARIANT == 1
PS_OUTPUT ps20_blinnLightFlare2 (PS_INPUT In)
#elif LIGHTFLARE_VARIANT == 2
PS_OUTPUT ps20_blinnLightFlareHeadlights2 (PS_INPUT In)
#else
PS_OUTPUT ps20_blinnLightGlareEffect (PS_INPUT In)
#endif
{
  PS_OUTPUT Out;
  
  float sceneIntensity = saturate(grayScale(diffuseColor));
  float invSceneIntensity = 1.0 - sceneIntensity;

  float4 tex0 = tex2D(sTex0, In.Tex0.xy);
  float4 tex0value = tex0;

#if LIGHTFLARE_VARIANT == 3 // Lights with glare

  //float4 tex = tex0value * max(In.Angle.x, specularColor.w) + tex0value * In.Angle.y * tex0.a * 10;
  //float4 tex = tex0value * max(In.Angle.x, specularColor.a) + tex0value * In.Angle.y * tex0.a * 10;
  //Out.Color = tex * (1.0 + sceneIntensity);
  float4 tex = tex0 * tex0.a + tex0 * In.Angle.x + tex0 * tex0.a * In.Angle.y;
  Out.Color = tex;

#elif LIGHTFLARE_VARIANT == 2 // Headlight flare

  float4 tex1 = tex2D(sTex1, In.Tex0.xy);
  
  float4 tex = tex0 * In.Angle.x * invSceneIntensity + tex1 * In.Angle.y;

  float4 tex0c = tex2D(sTex0, float2(0.5, 0.5)); // sample at the center of the first tex to check if the tex is enabled
  Out.Color = tex;
  //Out.Color = pow(tex * (1.0 + sceneIntensity), 1.25) * tex0c.a * specularColor.a;

#else // Normal light flare

  //float4 tex = tex0value * invSceneIntensity * In.Angle.x;
  //Out.Color = pow(tex * (1.0 + sceneIntensity), 1.25) * specularColor.a;
  Out.Color = tex0 * In.Angle.x * invSceneIntensity;

#endif

  Out.Color.a = tex0.a;
  
#if BILLBOARD == 1
  Out.Depth = In.Depth;
#endif

  return (Out);
}
