#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye: register (c8);
float4 fogData : register (c9);
float4 texMod : register (c11); // tex0.u=x, tex0.v=y, tex1.u=z, tex1.v=w
float4 dirLight : register (c14);

struct VS_OUTPUT
{
   float4 Pos  : POSITION;
   float4 Diff : COLOR0;
#ifdef USEFOG
   float  Fog  : FOG;
#endif   
#if NUMTEX == 1
   float2 Tex  : TEXCOORD0;
#else   
   float4 Tex  : TEXCOORD0;
#endif
};

#if SCROLLTEX == 1

  #if NUMTEX == 2
    VS_OUTPUT SRPL_L25_noShadeModTexT0T1 (float4 inPos : POSITION, float4 inColor : COLOR0, float2 inTex : TEXCOORD0, float2 inTex1 : TEXCOORD1)
  #else  
    VS_OUTPUT SRPL_L25_noShadeModTexT0 (float4 inPos : POSITION, float4 inColor : COLOR0, float2 inTex : TEXCOORD0)
  #endif  

#else

  #if NUMTEX == 2
    VS_OUTPUT SRPL_L25_noShadeT0T1 (float4 inPos : POSITION, float4 inColor : COLOR0, float2 inTex : TEXCOORD0, float2 inTex1 : TEXCOORD1)
  #elif NUMTEX == 1
    VS_OUTPUT SRPL_L25_noShadeT0 (float4 inPos : POSITION, float4 inColor : COLOR0, float2 inTex : TEXCOORD0)
  #else  
    VS_OUTPUT SRPL_L25_noShade (float4 inPos : POSITION, float4 inColor : COLOR0, float2 inTex : TEXCOORD0)
  #endif  
  
#endif  
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

#ifdef USEFOG
  // compute fog
  float3 eyeVec = mul (worldMatrix, inPos) + worldEye;
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif
  
  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  
#if SCROLLTEX == 1

  Out.Tex.xy = inTex + texMod.xy;
 #if NUMTEX == 2
  Out.Tex.zw = inTex1 + texMod.zw;
 #endif   

#else
  
  Out.Tex.xy = inTex;
 #if NUMTEX == 2
  Out.Tex.zw = inTex1;
 #endif   

#endif
  return (Out);
}








