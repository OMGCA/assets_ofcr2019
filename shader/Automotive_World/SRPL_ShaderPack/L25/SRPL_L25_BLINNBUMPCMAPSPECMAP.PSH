float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
#if MULTALPHA == 1
   float4 Base   : TEXCOORD0; // base0=xy base1=zw
   float4 Bump   : TEXCOORD1; // spec=xy bump=zw
#else
   float4 Base   : TEXCOORD0; // base=xy, spec=zw
   float2 Bump   : TEXCOORD1; // bump map
#endif   
   float3 Light  : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
   
   float3 Basis1 : TEXCOORD4; // tangent space eye vec
   float3 Basis2 : TEXCOORD5; // tangent space eye vec
   float3 Basis3 : TEXCOORD6; // tangent space eye vec
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sBase : register (s0);

#if MULTALPHA == 1
 sampler2D sBase1 : register (s1);
 sampler2D sSpec : register (s2);
 sampler2D sBump : register (s3);
 samplerCUBE sCube : register (s4);
#else
 sampler2D sSpec : register (s1);
 sampler2D sBump : register (s2);
 samplerCUBE sCube : register (s3);
#endif

#if ADDALPHA == 1
 PS_OUTPUT SRPL_L25_blinnBumpCmapSpecMapAddAlphaReflectT0 (PS_INPUT Input)
#elif ADDSPECALPHA == 1
 PS_OUTPUT SRPL_L25_blinnBumpCmapSpecMapAddSpecularReflectT0 (PS_INPUT Input)
#elif MULTALPHA == 1
 PS_OUTPUT SRPL_L25_blinnBumpCmapSpecMapT0T1MatAlphaVertexAlpha (PS_INPUT Input)
#else 
 PS_OUTPUT SRPL_L25_blinnBumpCmapSpecMapT0 (PS_INPUT Input)
#endif 
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex = tex2D (sBase, Input.Base.xy);              // sample base tex

#if MULTALPHA == 1
  float4 tex1 = tex2D (sBase1, Input.Base.zw);              // sample base tex
  float4 spec = tex2D (sSpec, Input.Bump.xy);             // sample spec tex (gloss)
  float3 bump = (tex2D (sBump, Input.Bump.zw) - 0.5) * 2.0;  // sample and unbias normal

  tex.rgb = tex.rgb * lerp (tex1.rgb, 1.0f, ambientColor.a);

#else
  float4 spec = tex2D (sSpec, Input.Base.zw);             // sample spec tex (gloss)
  float3 bump = (tex2D (sBump, Input.Bump.xy) - 0.5) * 2.0;  // sample and unbias normal
#endif

  // put the normal into eye space using transpose texture basis
  float3 viewBump;
  viewBump.x = dot (Input.Basis1, bump);
  viewBump.y = dot (Input.Basis2, bump);
  viewBump.z = dot (Input.Basis3, bump);

  //float4 graySpec = mul (spec, (0.30, 0.59, 0.11, 0.0));
  float3 normLight = normalize (Input.Light);
  float3 normEyeVec = normalize (Input.EyeVec.xyz);
  
  // compute specular color 
  float3 halfway = normalize (normLight - normEyeVec); 
  
  half4 specular;
  specular.rgb = saturate( pow( dot( halfway, viewBump ), specularColor.a ) + Input.Omni) * specularColor * spec;// * tex.a;
  specular.a = 0;

  // compute diffuse color
  half4 diffuse = (saturate (dot (normLight, viewBump))) * diffuseColor * float4(1.5, 1.5, 1.5, 1.0) + ambientColor + Input.Omni;
 #if MULTALPHA == 1
  diffuse.a = lerp (tex1.a, 1.0f, ambientColor.a);
 #endif 

  // compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float4 cube = texCUBE (sCube, reflect (normEyeVec, viewBump));
  cube.a = 1.0;
  
#if MULTALPHA == 1
  cube.rgb = lerp (cube.rgb, 0.0, diffuse.a);
  //cube.a = 0.0;
#endif

#if ADDALPHA == 1  
  Out.Color = (((tex * diffuse) + specular) + (cube * diffuse * tex.a)) * Input.Color;
#elif ADDSPECALPHA == 1  
  Out.Color = (((tex * diffuse) + specular) + (cube * diffuse * spec.a)) * Input.Color;
#elif MULTALPHA == 1  
  Out.Color = (((tex * diffuse) + specular) + (cube * diffuse * blendPct.a)) * Input.Color;
#else
  Out.Color = lerp ((tex * diffuse) + specular, cube * diffuse, blendPct.a) * Input.Color;
#endif  
  return (Out);
}
