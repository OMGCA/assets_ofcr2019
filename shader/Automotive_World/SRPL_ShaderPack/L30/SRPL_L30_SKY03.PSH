float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 dirLight : register (c14);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

#define exposition(op1, op2) float3(1.05, 1.05, 1.05) - pow(float3(2.7, 2.7, 2.7), op1 * op2) * 1.05

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float3 Tex    : TEXCOORD0; // base
   float4 TexOff : TEXCOORD1; // texcoord offsets for two blended images
   float4 EyeVec : TEXCOORD3;
   float3 SunPos : TEXCOORD4;
   float2 NorthX : TEXCOORD5; // cubemap matrix 1
   float2 NorthY : TEXCOORD6; // cubemap matrix 2
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sTex1 : register (s1);
samplerCUBE sTex2 : register (s2);
sampler2D sTex3 : register (s3);


PS_OUTPUT SRPL_L30_SKY03 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

//  if (Input.EyeVec.y < -0.1) // this should work pretty nice 
//  {                          // but instead of increasing framerate it decreases it slightly
//    discard;
//  }
  float3 LightVec = Input.SunPos.xyz;
  float3 EyeVec = Input.EyeVec;
  float3 normEyeVec = normalize(EyeVec.xyz);

  float2 texc;
  texc.y = max(0.0, EyeVec.y) / 8.25 + 0.002;
  texc.x = distance(LightVec.xz, EyeVec.xz) / 16.5 + 0.003;

float3 cubeVec = normEyeVec;
cubeVec.xz = Input.NorthX * normEyeVec.x + Input.NorthY * normEyeVec.z;

  float4 cube = texCUBE (sTex2, cubeVec);


  //stars
  float4 tex = tex2D (sTex1, Input.Tex.xy) * (0.5 - 0.45 * diffuseColor.r);

  //atmosphere
  float4 tex1 = tex2D(sTex0, texc + Input.TexOff.xy);
  float4 tex2 = tex2D(sTex0, texc + Input.TexOff.zw);
  tex1.rgb *= tex1.a * 25.5;
  tex2.rgb *= tex2.a * 25.5;
//  float3 skyColor = pow(lerp(tex1.rgb, tex2.rgb, Input.Tex.z), 1.6) * float3(3.0, 2.8, 2.5);
  float3 skyColor = lerp(tex1.rgb, tex2.rgb, Input.Tex.z);

float3 cirrusColor = tex2D(sTex3, float2(distance(LightVec, normEyeVec) * 0.5, diffuseColor.g)).rgb;
skyColor = lerp(skyColor, cirrusColor * 5.0, cube.r * 0.5);

  skyColor += tex.rgb;
  skyColor = exposition(skyColor, Input.EyeVec.w);
  Out.Color.rgb = skyColor;

  Out.Color.a = 1.0;

  return (Out);
}
