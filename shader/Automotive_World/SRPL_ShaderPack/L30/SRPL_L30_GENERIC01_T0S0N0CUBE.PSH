float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

#define exposition(op1, op2) float3(1.2, 1.2, 1.2) - pow(float3(2.7, 2.7, 2.7), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color  : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;
   float4 TexCoord01    : TEXCOORD0;    // T0, Spec0
   float2 TexCoord23    : TEXCOORD1;    // Bump0
   float4 Normal        : TEXCOORD2;    // xyz - normal, w - omniSpec
   float4 EyeVec        : TEXCOORD4;    // xyz - eyeVec, w - fresnel power
   float3 Tangent       : TEXCOORD5;    // xyz - tangent, w - fresnel scale
   float3 Binormal      : TEXCOORD6;    // xyz - binormal, w - fresnel base
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0   : register (s0);
sampler2D sSpec0  : register (s1);
sampler2D sBump0  : register (s2);
samplerCUBE sCube : register (s3);
sampler2D sShading : register (s4);

PS_OUTPUT MAINFUNCTION (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Sample the textures
  float4 tex =  tex2D(sTex0, Input.TexCoord01.xy);
#ifdef ALPHATEST
  if (tex.a < ALPHATEST)
  {
    discard;
  }
#endif
  float4 spec = tex2D(sSpec0, Input.TexCoord01.zw);
  float3 bump = tex2D(sBump0, Input.TexCoord23.xy).rgb - 0.5;
  bump = normalize(bump);

  float3 normal = normalize(  bump.x * normalize(Input.Tangent.xyz)
                            + bump.y * normalize(Input.Binormal.xyz)
                            + bump.z * normalize(Input.Normal.xyz));

  float3 normEyeVec = normalize (Input.EyeVec.xyz);

  float3 reflectVec = reflect(normEyeVec, normal);

  float2 specular = tex2D(sShading, float2(saturate(dot (reflectVec, worldLight)), 0.0)).gb;
  specular.x = specular.x * 256.0 + specular.y;

  // Compute diffuse color
  float4 diffuse = saturate(dot (worldLight, normal)) * diffuseColor * 1.65 + ambientColor;
#ifdef OMNISCALE
  diffuse = diffuse + Input.Omni;
#endif

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float4 cube = texCUBE (sCube, reflectVec);
  float dayTime = saturate(ambientColor.b * 2.0) * 2.9 + 0.1;
//  cube.rgb *= 2.727272; //3.0 / 1.1;
  cube.rgb *= dayTime;


  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));

  // Use the specular alpha for reflection amount and add fresnel to that
  float2 fresnel = tex2D(sShading, float2(dotEyeVecNormal, 0.0)).ra;
  fresnel.x = fresnel.x / 256.0 + fresnel.y;

  //less reflection during night
//  fresnel *= diffuseColor.r;

  specular = specular * cube.a;

  cube = (tex * Input.Color * diffuse) +
                ((cube + specularColor * specular.x + float4(1.0, 0.9, 0.8, 0.0) * Input.Normal.w) * spec) *
                fresnel.x;

  cube.rgb = exposition(cube.rgb, Input.EyeVec.w);
  cube.a = tex.a + ALPHABIAS;
  Out.Color = cube;

  return (Out);
}
