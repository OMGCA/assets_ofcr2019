float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3);
float4 blendPct      : register (c4);

#define exposition(op1, op2) float4(1.2, 1.2, 1.2, 1.2) - pow(float4(2.7, 2.7, 2.7, 2.7), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float  Fog    : FOG;
   float4 Texc0  : TEXCOORD0; // base / specaulr and bump
   float4 Texc1  : TEXCOORD1; // aiw / skid
   float4 Normal : TEXCOORD2;
   float4 EyeVec : TEXCOORD3;
   float4 specularPower : TEXCOORD4;
   float3 Normal2 : TEXCOORD5;
   float4 fresnelScale  : TEXCOORD6;
   float4 fresnelPower  : TEXCOORD7;

   float4 Omni   : COLOR1;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0    : register (s0);
sampler2D sSpec    : register (s1);
sampler2D sBump    : register (s2);
sampler2D sSkid    : register (s3);
sampler2D sAiw     : register (s4);
sampler2D sShading : register (s5);
sampler2D sFresnel : register (s6);

PS_OUTPUT SRPL_L30_ROAD_T0SPECBUMPSKID03 (PS_INPUT Input) 
{
  PS_OUTPUT Out;


  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Texc0.xy);
  float4 spec = tex2D (sSpec, Input.Texc0.zw);
  float4 bump = tex2D (sBump, Input.Texc0.zw);
  float4 aiw = tex2D (sAiw, Input.Texc1.xy);


  float2 skidCoord = Input.Texc1.zw;
  float4 trackMap = tex2D (sSkid, skidCoord);


  float3 normal = Input.Normal.xyz;
  float3 normEyeVec = normalize(Input.EyeVec.xyz);
  float3 lightVec = worldLight;

  float3 bumpNormal = normalize(bump.xzy + Input.Normal2);


  float whitepaint = step(0.96, bump.z) * tex.a;


  float3 reflected = reflect(normEyeVec, bumpNormal);
  float4 dotReflect = float4(1.0, 1.0, 1.0, 1.0) * saturate(dot(reflected, lightVec));
  float4 dotEyeVecNormal = float4(2.0, 2.0, 2.0, 2.0) * saturate(0.5 + dot(normEyeVec, normal));

  float4 fresnelBase = tex2D(sFresnel, float2(0.5, 0.5));
  float4 specular = pow(dotReflect, Input.specularPower);
  float4 fresnel = pow(dotEyeVecNormal, Input.fresnelPower) * Input.fresnelScale + fresnelBase * 2.55;

  float4 mixer = tex2D(sShading, float2(trackMap.b, aiw.r));
  mixer = lerp(mixer, float4(0.0, 0.0, 0.0, 1.0), whitepaint);

  float finalSpecular = dot(mixer, specular);
  float finalFresnel = dot(mixer, fresnel);



  // track map - red component = darken
  tex.rgb = tex.rgb * (lerp(trackMap.r, 1.0, aiw.g) + whitepaint);


  // Compute diffuse color
  float4 diffuse = (saturate (dot (lightVec, bumpNormal))) * diffuseColor * 1.65 + ambientColor;
  diffuse += Input.Omni;

  
  // Final color
  tex = (tex * diffuse)
        + (specularColor * finalSpecular + Input.Color) * spec * finalFresnel;

  Out.Color = exposition(tex, Input.EyeVec.w);

  return (Out);
}
