#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog    : FOG;
   float3 Tex    : TEXCOORD0; // base
   float4 Normal : TEXCOORD2; // world space vertex normal + sign of reflection
   float3 EyeVec : TEXCOORD3; // world space eye vec
};

  VS_OUTPUT vs20_srpl_sky01 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL, float2 inTex0 : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inBump : TEXCOORD2)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  Out.Normal.xyz = worldNorm;
  Out.Normal.w = worldMatrix[1][1];
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec = eyeVec;
  
  // override fog
  Out.Fog = 1.0;

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex0;

  if (inPos.y > 1.0)
  {
    Out.Tex.z = min(1.0, (inPos.y - 1.0) / 20.0);
  }

  return (Out);
}

