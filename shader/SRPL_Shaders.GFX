///////////////////////////////////////////////////////////////////////////////
//
// gMotor2 Shader Description File
// SimracingPL FIA GT League
//
///////////////////////////////////////////////////////////////////////////////


//Used by most cars
ShaderName=SRPLDX9CARPAINT01
{
  ShaderDesc="Fresnel Cube Map Add Spec Map Add Alpha Reflect Tex1"
  ShaderLongDesc="Diffuse lighting, cube map with fresnel effect according to specular alpha, specular map, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_carpaint01
      {
        File=vs20_srpl_carpaint01.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_carpaint01
      {
        File=ps20_srpl_carpaint01.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        Define=(ADDALPHA, 1)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, CubeMap, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by Aston Martin DB9, Porsche 997, BMW (hood and bumpers)
ShaderName=SRPLDX9CARPAINT02
{
  ShaderDesc="Bump Cube Specular Map Add Specular Reflect T1"
  ShaderLongDesc="Diffuse lighting, normal map, cube map modulated by spec map alpha, specular map, tex1, DX9."
  ShaderDowngrade=L1BumpT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_carpaint02
      {
        File=vs20_srpl_carpaint02.vsh  
        Language=HLSL
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, BumpMap, OmniLight)
      }
      PixelShader=ps20_srpl_carpaint02
      {
        File=ps20_srpl_carpaint02.psh
        Language=HLSL
        Define=(ADDSPECALPHA, 1)
        ShaderConstants=(Lighting, CubeMapBlend, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, BumpMap, Shader)
        StageState=(3, CubeMap, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}



//Used by Corvette C6R
ShaderName=SRPLDX9CARPAINT03
{
  ShaderDesc="Bump Cube Specular Map Add Specular Reflect T1"
  ShaderLongDesc="Diffuse lighting, normal map, cube map modulated by spec map alpha, specular map, tex1, DX9."
  ShaderDowngrade=L1BumpT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_carpaint03
      {
        File=vs20_srpl_carpaint03.vsh  
        Language=HLSL
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, BumpMap, OmniLight)
      }
      PixelShader=ps20_srpl_carpaint03
      {
        File=ps20_srpl_carpaint03.psh
        Language=HLSL
        Define=(ADDSPECALPHA, 1)
        ShaderConstants=(Lighting, CubeMapBlend, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, BumpMap, Shader)
        StageState=(3, CubeMap, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}


//Used by C6R, MC12, S7R, RGT, F550
ShaderName=SRPLDX9COCKPIT01
{
  ShaderDesc="Specular Map T1 (Custom)"
  ShaderLongDesc="Diffuse + specular map, tex1 only, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1SpecularMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_cockpit01
      {
        File=vs20_srpl_cockpit01.vsh 
        Language=HLSL
        Define=(NUMTEX, 2)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_cockpit01
      {
        File=ps20_srpl_cockpit01.psh
        Language=HLSL
        Define=(NUMTEX, 2)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Add)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
      }
    }
  }
}

//not used
ShaderName=SRPLDX9COCKPIT02
{
  ShaderDesc="Specular Map T1 (Custom)"
  ShaderLongDesc="Diffuse + specular map, tex1 only, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1SpecularMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_cockpit02
      {
        File=vs20_srpl_cockpit02.vsh 
        Language=HLSL
        Define=(NUMTEX, 2)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_cockpit02
      {
        File=ps20_srpl_cockpit02.psh
        Language=HLSL
        Define=(NUMTEX, 2)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Add)
        StageState=(2, CubeMap, Shader)
        StageState=(3, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
      }
    }
  }
}


//Used by DB9R, P997
ShaderName=SRPLDX9COCKPIT03
{
  ShaderDesc="Specular Map T1 (Custom)"
  ShaderLongDesc="Diffuse + specular map, tex1 only, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1SpecularMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_cockpit03
      {
        File=vs20_srpl_cockpit03.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_cockpit03
      {
        File=ps20_srpl_cockpit03.psh
        Language=HLSL
        Define=(NUMTEX, 2)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Add)
        StageState=(2, BumpMap, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
      }
    }
  }
}



//Used by F430
ShaderName=SRPLDX9COCKPIT04
{
  ShaderDesc="Specular Map T1 (Custom)"
  ShaderLongDesc="Diffuse + specular map, tex1 only, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1SpecularMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_cockpit04
      {
        File=vs20_srpl_cockpit04.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_cockpit04
      {
        File=ps20_srpl_cockpit04.psh
        Language=HLSL
        Define=(NUMTEX, 2)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Add)
        StageState=(2, BumpMap, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
      }
    }
  }
}
//Used by most cars
ShaderName=SRPLDX9STWHEEL01
{
  ShaderDesc="Specular Map T1 (Custom)"
  ShaderLongDesc="Diffuse + specular map, tex1 only, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1SpecularMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_stwheel01
      {
        File=vs20_srpl_stwheel01.vsh 
        Language=HLSL
        Define=(NUMTEX, 2)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_stwheel01
      {
        File=ps20_srpl_stwheel01.psh
        Language=HLSL
        Define=(NUMTEX, 2)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Add)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
      }
    }
  }
}


//Used by most cars
ShaderName=SRPLDX9WINDOW01
{
  ShaderDesc="S7R Bump Cube Specular Map T1"
  ShaderLongDesc="Diffuse lighting, normal map, cube map, specular map tex1, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_window01
      {
        File=vs20_srpl_window01.vsh 
        Language=HLSL
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, BumpMap, OmniLight)
      }
      PixelShader=ps20_srpl_window01
      {
        File=ps20_srpl_window01.psh
        Language=HLSL
        ShaderConstants=(Lighting, CubeMapBlend, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, CubeMap, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by Lister Storm
ShaderName=SRPLDX9WINDOW02
{
  ShaderDesc="S7R Bump Cube Specular Map T1"
  ShaderLongDesc="Diffuse lighting, normal map, cube map, specular map tex1, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_window02
      {
        File=vs20_srpl_window02.vsh 
        Language=HLSL
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, BumpMap, OmniLight)
      }
      PixelShader=ps20_srpl_window02
      {
        File=ps20_srpl_window02.psh
        Language=HLSL
        ShaderConstants=(Lighting, CubeMapBlend, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, CubeMap, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by Aston Martin DB9
ShaderName=SRPLDX9WINDOW03
{
  ShaderDesc="Cube Map T1"
  ShaderLongDesc="Diffuse lighting, cube map, tex1 only, DX9."
  ShaderDowngrade=L1CMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_window03
      {
        File=vs20_srpl_window03.vsh
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_window03
      {
        File=ps20_srpl_window03.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        ShaderConstants=(Lighting, CubeMapBlend)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, CubeMap, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by safety car
ShaderName=SRPLDX9WINDOW04
{
  ShaderDesc="S7R Bump Cube Specular Map T1"
  ShaderLongDesc="Diffuse lighting, normal map, cube map, specular map tex1, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_window04
      {
        File=vs20_srpl_window04.vsh 
        Language=HLSL
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, BumpMap, OmniLight)
      }
      PixelShader=ps20_srpl_window04
      {
        File=ps20_srpl_window04.psh
        Language=HLSL
        ShaderConstants=(Lighting, CubeMapBlend, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, CubeMap, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by most cars
ShaderName=SRPLDX9HEADLIGHTS01
{
  ShaderDesc="S7R Bump Cube Specular Map T1"
  ShaderLongDesc="Diffuse lighting, normal map, cube map, specular map tex1, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_headlights01
      {
        File=vs20_srpl_headlights01.vsh 
        Language=HLSL
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, BumpMap, OmniLight)
      }
      PixelShader=ps20_srpl_headlights01
      {
        File=ps20_srpl_headlights01.psh
        Language=HLSL
        ShaderConstants=(Lighting, CubeMapBlend, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, CubeMap, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by S7R
ShaderName=SRPLDX9INTWINDOW01
{
  ShaderDesc="S7R int window"
  ShaderLongDesc="Interior window shader"
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_intwindow01
      {
        File=vs20_srpl_intwindow01.vsh 
        Language=HLSL
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, BumpMap, OmniLight)
      }
      PixelShader=ps20_srpl_intwindow01
      {
        File=ps20_srpl_intwindow01.psh
        Language=HLSL
        ShaderConstants=(Lighting, CubeMapBlend, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, BumpMap, Shader)
        StageState=(3, CubeMap, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by C6R, MC12, F550, RGT, DB9R, P997, F430 and for glass edges in BMW
ShaderName=SRPLDX9INTWINDOW02
{
  ShaderDesc="S7R int window"
  ShaderLongDesc="Interior window shader"
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_intwindow02
      {
        File=vs20_srpl_intwindow02.vsh 
        Language=HLSL
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_intwindow02
      {
        File=ps20_srpl_intwindow02.psh
        Language=HLSL
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}


//used by BMW
ShaderName=SRPLDX9INTWINDOW03
{
  ShaderDesc="S7R int window"
  ShaderLongDesc="Interior window shader"
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_intwindow03
      {
        File=vs20_srpl_intwindow03.vsh 
        Language=HLSL
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_intwindow03
      {
        File=ps20_srpl_intwindow03.psh
        Language=HLSL
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}




//Used by Corvette C6R
ShaderName=SRPLDX9RIM02
{
  ShaderDesc="Fresnel Cube Map Add Spec Map Add Alpha Reflect Tex1"
  ShaderLongDesc="Diffuse lighting, cube map with fresnel effect according to specular alpha, specular map, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_rim02
      {
        File=vs20_srpl_rim02.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_rim02
      {
        File=ps20_srpl_rim02.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        Define=(ADDALPHA, 1)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, CubeMap, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by Corvette C6R
ShaderName=SRPLDX9RIM03
{
  ShaderDesc="Fresnel Cube Map Add Spec Map Add Alpha Reflect Tex1"
  ShaderLongDesc="Diffuse lighting, cube map with fresnel effect according to specular alpha, specular map, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_rim03
      {
        File=vs20_srpl_rim03.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_rim03
      {
        File=ps20_srpl_rim03.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        Define=(ADDALPHA, 1)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, BumpMap, Shader)
        StageState=(3, CubeMap, Shader)
        StageState=(4, Math, Shader)
        StageState=(5, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
        SamplerState=(5, Wrap, Wrap, Wrap)
      }
    }
  }
}

//Used by most cars
ShaderName=SRPLDX9TYRE01
{
  ShaderDesc="Fresnel Cube Map Add Spec Map Add Alpha Reflect Tex1"
  ShaderLongDesc="Diffuse lighting, cube map with fresnel effect according to specular alpha, specular map, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_tyre01
      {
        File=vs20_srpl_tyre01.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default)
      }
      PixelShader=ps20_srpl_tyre01
      {
        File=ps20_srpl_tyre01.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        Define=(ADDALPHA, 1)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, Math, Shader)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
      }
    }
  }
}


ShaderName=SRPLDX9CAGE01
{
  ShaderDesc="Fresnel Cube Map Add Spec Map Add Alpha Reflect Tex1"
  ShaderLongDesc="Diffuse lighting, cube map with fresnel effect according to specular alpha, specular map, DX9."
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_srpl_cage01
      {
        File=vs20_srpl_cage01.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_srpl_cage01
      {
        File=ps20_srpl_cage01.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        Define=(ADDALPHA, 1)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, CubeMap, Shader)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
      }
    }
  }
}



ShaderName=SRPLDX9LIGHTGLOW01
{
  ShaderDesc="Light Flare - Headlights"
  ShaderLongDesc="Light Flare. Suitable for car headlights. DX9"
  ShaderLevel=(2) // DX9
  {
    Pass=(0)
    {
      VertexShader=vs20_srpl_lightglow01
      {
        File=vs20_srpl_lightglow01.vsh 
        Language=HLSL
        
	// always display glow 1m closer to camera
        Define=(ZFIX, 1.0)

        // Has flash effect
        Define=(HIGHFLASH, 1)
        Define=(FLASHSIZE, 1.012)
        //Define=(FLASHSIZE, 0.0)
        Define=(FLASHMULTIPLIER, 220)
	
        // Normal glow size and multiplier
        Define=(GLOWSIZE, 1.085)
        Define=(GLOWMULTIPLIER, 20)

        VertexDecl=PShadeDecl
        ShaderConstants=Default
      }
      PixelShader=ps20_srpl_lightglow01
      {
        File=ps20_srpl_lightglow01.psh
        Language=HLSL

        // Has flash effect
        Define=(HIGHFLASH, 1)

        ShaderConstants=(Lighting, SpecPower)

        StageState=(0, DiffuseMap, Modulate)
        SamplerState=(0, Wrap, Wrap, Wrap)

      }
    }
  }
}



ShaderName=SRPLDX9LIGHTGLOW01REV
{
  ShaderDesc="Light Flare - Headlights"
  ShaderLongDesc="Light Flare. Suitable for car headlights. DX9"
  ShaderLevel=(2) // DX9
  {
    Pass=(0)
    {
      VertexShader=vs20_srpl_lightglow01rev
      {
        File=vs20_srpl_lightglow01rev.vsh 
        Language=HLSL
        
	// always display glow 1m closer to camera
        Define=(ZFIX, 1.0)

        // Has flash effect
        Define=(HIGHFLASH, 1)
        Define=(FLASHSIZE, 1.012)
        //Define=(FLASHSIZE, 0.0)
        Define=(FLASHMULTIPLIER, 220)
	
        // Normal glow size and multiplier
        Define=(GLOWSIZE, 1.085)
        Define=(GLOWMULTIPLIER, 20)

        VertexDecl=PShadeDecl
        ShaderConstants=Default
      }
      PixelShader=ps20_srpl_lightglow01
      {
        File=ps20_srpl_lightglow01.psh
        Language=HLSL

        // Has flash effect
        Define=(HIGHFLASH, 1)

        ShaderConstants=(Lighting, SpecPower)

        StageState=(0, DiffuseMap, Modulate)
        SamplerState=(0, Wrap, Wrap, Wrap)

      }
    }
  }
}

            