float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;

   float2 Tex    : TEXCOORD0; // base
   float2 Spec   : TEXCOORD1; // spec
   float4 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex : register (s0);
sampler2D sSpec : register (s1);
samplerCUBE sCube : register (s2);


PS_OUTPUT ps20_srpl_window04 (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  float fresnelBias = 0.2;
  float fresnelScale = 1.0;   //original = 7.0
  float fresnelPower = 4.0;   //original = 4.0

  // sample the textures
  float4 tex = tex2D (sTex, Input.Tex.xy);              // sample base tex
  if (tex.a < 0.1)
  {
    discard;
  }

  float4 spec = tex2D (sSpec, Input.Spec);             // sample spec tex

//  tex.rgb = tex.rgb * lerp (tex1.rgb, 1.0f, ambientColor.a);


//  tex.rgb *= min(0.2, tex.a) * 5.0;

  //float4 graySpec = mul (spec, (0.30, 0.59, 0.11, 0.0));
//  float3 normLight = normalize (Input.Light);
  float3 normal = normalize (Input.Normal.xyz);
  float3 normEyeVec = normalize (Input.EyeVec);
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));
  
  // compute specular color 
  float3 halfway = normalize (worldLight.xyz - normEyeVec); 
  halfway.x = dot (halfway, normal);
  
  float specular;
  specular = saturate (pow (halfway.x, 5000.0));


  // compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float4 cube = texCUBE (sCube, reflect (normEyeVec, normal));
  
  // Calculate fresnel effect for better reflections
  float fresnel = fresnelBias  + fresnelScale * pow(1.0 - dotEyeVecNormal, fresnelPower);

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;

cube.rgb = cube.rgb * 1.6;
cube.a = fresnel;

float mixer = (min(max(tex.a, 0.66), 0.67) - 0.66) * 80.0 + 0.2;

tex.rgb = lerp(tex.rgb, cube.rgb, cube.a);
tex.a = 1.0;
cube.rgba = lerp(cube.rgba, tex.rgba, mixer);
cube.rgba = lerp(cube.rgba, float4(2.0, 1.8, 1.5, 1.5), specular);

cube.rgb *= day;
cube.rgb *= cube.a;

Out.Color = cube;

  return (Out);
}
