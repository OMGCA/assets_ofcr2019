float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 SideVec : COLOR0;
   float  Fog     : FOG;
   float2 Tex     : TEXCOORD0; // base
   float2 Spec    : TEXCOORD1; // spec
   float4 Normal  : TEXCOORD2;
   float3 EyeVec  : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);

PS_OUTPUT ps20_srpl_tyre01 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Fresnel parameters
  float fresnelBias = 0.1;
  float fresnelScale = 0.4;
  float fresnelPower = 12.0;

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);
  float3 normal = normalize (Input.Normal.xyz);
  float3 normEyeVec = normalize (Input.EyeVec);
  float3 sideVec = (Input.SideVec - float4(0.5f, 0.5f, 0.5f, 0.5f)) * 2.0;

  float3 halfway = normalize (worldLight - normEyeVec); 
  halfway.x = dot (halfway, normal);
  
  half4 specular;
  specular.rgb = float4(4.0, 3.8, 3.6, 0.0) * saturate (pow (halfway.x, 40.0));
  specular.a = 0;

  // Compute diffuse color
  half4 diffuse = saturate ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor * 0.5);
  float diffuseStrength = max(dot(normal.xyz, sideVec.xyz) * 0.75 + 0.25, 0.0);
  diffuse *= diffuseStrength;

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 cubeCoord;
  cubeCoord = reflect(normEyeVec, normal);
  float reflectStrength = pow(max(dot(cubeCoord.xyz, sideVec.xyz) * 0.9 + 0.1, 0.0), 0.4);
  cubeCoord.y = max(cubeCoord.y * 0.65 + 0.35, 0.0);
  float4 cube;
  cube.rgb = pow(cubeCoord.yyy, float3(1.0, 1.2, 1.5)) * float3(1.1, 1.2, 1.4);
  cube.a = 1.0;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = fresnelBias  + fresnelScale * pow(1 - dotEyeVecNormal, fresnelPower);
  fresnel *= reflectStrength;
  
  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day;

  // Final color
  Out.Color = (tex * diffuse) + ((cube + specular) * fresnel);
//Out.Color.rgb = float3(diffuseStrength, reflectStrength, 0.0);
  return (Out);
}
