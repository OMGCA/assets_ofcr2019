float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);

struct PS_INPUT
{
  float4 Color  : COLOR0;
  float2 Tex0   : TEXCOORD0; // xy=map0, zw=map1
  float3 Normal : TEXCOORD1;
};
sampler2D sTex0      : register (s0);

struct PS_OUTPUT
{
  float4 Color : COLOR0;
};

PS_OUTPUT ps20_luminosity (PS_INPUT Input)
{
  PS_OUTPUT Out;

  float4 tex = tex2D (sTex0, Input.Tex0.xy);
  float3 normal = normalize (Input.Normal);

  float intensity = saturate(0.2126 * diffuseColor.x + 0.7152 * diffuseColor.y + 0.0722 * diffuseColor.z);
  half4 diffuse = saturate ((saturate (dot (worldLight, normal)) * diffuseColor) + ambientColor);
  Out.Color = (diffuse * saturate (tex)) * Input.Color * tex.a + (1 - intensity) * tex;
  
  return (Out);
}
