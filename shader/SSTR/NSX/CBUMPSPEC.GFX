// Custom bumpmap and specular map shaders, fixes Omni light glow on specular.

ShaderName=L2BumpSpecMapT0Custom
{
  ShaderDesc="Bump Specular Map T1 (Custom)"
  ShaderLongDesc="Diffuse lighting, normal map, specular map tex1, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1BumpSpecMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_blinnBumpSpecMapT0
      {
        File=vs20_blinnBumpSpecMap.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_blinnBumpSpecMapT0Custom
      {
        File=ps20_blinnCBumpSpecMap.psh
        Language=HLSL
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Shader)
        StageState=(2, BumpMap, Shader)
        StageState=(3, Math, Shader)
        StageState=(4, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
        SamplerState=(4, Wrap, Wrap, Wrap)
      }
    }
  }
}

ShaderName=L2BumpSpecT0Custom
{
  ShaderDesc="Bump Map Specular T1 (Custom)"
  ShaderLongDesc="Diffuse lighting, normal map, specular tex1, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1BumpT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_blinnBumpT0
      {
        File=vs20_blinnBump.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeBumpDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_blinnBumpSpecT0Custom
      {
        File=ps20_blinnCBumpSpec.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap)
        StageState=(1, BumpMap, Shader)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
      }
    }
  }
}

ShaderName=L2SpecularMapT0Custom
{
  ShaderDesc="Specular Map T1 (Custom)"
  ShaderLongDesc="Diffuse + specular map, tex1 only, DX9. Fixed omni light glow for sepcular."
  ShaderDowngrade=L1SpecularMapT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0) // normal
    {
      VertexShader=vs20_blinnSpecMapT0
      {
        File=vs20_blinnSpecMap.vsh 
        Language=HLSL
        Define=(NUMTEX, 1)
        VertexDecl=PShadeDecl
        ShaderConstants=(Default, OmniLight)
      }
      PixelShader=ps20_blinnSpecMapT0Custom
      {
        File=ps20_blinnCSpecMap.psh
        Language=HLSL
        Define=(NUMTEX, 1)
        ShaderConstants=(Lighting, SpecPower)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, SpecularMap, Add)
        StageState=(2, Math, Shader)
        StageState=(3, Math, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
        SamplerState=(2, Wrap, Wrap, Wrap)
        SamplerState=(3, Wrap, Wrap, Wrap)
      }
    }
  }
}

