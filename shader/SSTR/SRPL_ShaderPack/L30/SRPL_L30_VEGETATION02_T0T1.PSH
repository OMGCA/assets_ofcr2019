float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

#define exposition(op1, op2) float3(1.2, 1.2, 1.2) - pow(float3(2.7, 2.7, 2.7), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color  : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;

   float2 Tex0    : TEXCOORD0; // base0
   float4 EyeVec  : TEXCOORD1; // world space eye vec
//   float3 Normal  : TEXCOORD2; // 
   float3 CenterVec1 : TEXCOORD3; //
   float3 CenterVec2 : TEXCOORD4; //
   float2 Radius : TEXCOORD5; //
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0: register (s0);
sampler2D sTex1: register (s1);
 
PS_OUTPUT SRPL_L30_VEGETATION02_T0T1 (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  float4 tex0 = tex2D (sTex0, Input.Tex0.xy);        // sample base tex
  float4 tex1 = tex2D (sTex1, Input.Tex0.xy);        // sample base tex

  if (tex0.a < 0.5)
  {
    discard;
  }


  float3 normEyeVec = normalize (Input.EyeVec.xyz);
  float3 centerVec = Input.CenterVec1 / Input.Radius.x + Input.CenterVec2 / Input.Radius.y;

  float centerVecLength = length(centerVec);
  centerVec /= max(centerVecLength, 1.0);


  
  float3 lightVec = normalize(worldLight); 
  float dotLightEye = dot(lightVec.xz, normEyeVec.xz);

  float lightThrough = pow(max(0.0, dotLightEye), 3.0);
  
  lightThrough *= tex1.a;

  float day = diffuseColor.r;
  lightThrough *= day;

  float l = saturate(dot(centerVec, lightVec) + 1.0 - dotLightEye * 0.5);

  // compute diffuse color
  half4 diffuse = diffuseColor * pow(l, 2.0) + ambientColor;
//#ifdef OMNISCALE
//  diffuse = diffuse + Input.Omni * OMNISCALE;
//#endif
//
//  Out.Color = (tex * diffuse) * Input.Color;


  tex0.rgb = lerp(tex0.rgb * diffuse.rgb, tex1.rgb, lightThrough);  //subsurface scattering

  tex0.rgb = exposition(tex0.rgb, Input.EyeVec.w);

  Out.Color.rgba = tex0;

  return (Out);
}
