float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 sunLight : register (c13);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;
   float4 TexCoord01    : TEXCOORD0;    // T0, Spec0
   float4 Normal        : TEXCOORD2;    // xyz - normal, w - omniSpec
   float4 EyeVec        : TEXCOORD4;    // xyz - eyeVec
};



VS_OUTPUT MAINFUNCTION (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0,
                        float2 inTex0 : TEXCOORD0, float2 inSpec : TEXCOORD1,
                        float2 inUVScale : TEXCOORD3)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  float3 worldNorm = normalize(mul(worldMatrix, inNorm.xyz));
  Out.EyeVec.xyz = eyeVec;

  Out.Normal.xyz = worldNorm;

  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

//exposition
float lightDist = distance(normalize(mul(viewMatrix, normalize(dirLight)).xyz), float3(0.0, 0.0, 1.0)); //0.0 - looking at sun, 2.0 - looking away from sun
lightDist = pow(lightDist / 2.0, 1.3);
Out.EyeVec.w = lerp(-1.3, lightDist * -1.3 - 0.5, saturate(sunLight.g * 2.0 - 0.3));

  float omniSpec = 0.0;
#ifdef OMNISCALE
  // Compute omnilights
  Out.Omni = float4(0,0,0,0);
  for (int i=0; i< numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);

    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
    {
      intens = dot (worldNorm, normalize (omniToVert)); // check backface
      if (intens > 0.0)
      {
        Out.Omni.rgb += (intens * (1.0F - (sqrt(mag) / sqrt(omniPosRad[i].w) )));
        intens = 1.0 - (mag / omniPosRad[i].w);
        float3 r = reflect(normalize(eyeVec), normalize(worldNorm));
        float s = saturate(dot(normalize(omniToVert), r));
        omniSpec += 10.0 * pow(s, 150.0) * intens;
      }
    }
  }
  Out.Omni.rgb *= OMNISCALE;
#endif
  Out.Normal.w = omniSpec * inUVScale.y;

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.TexCoord01.xy = inTex0;
  Out.TexCoord01.zw = inSpec;

  return (Out);
}

