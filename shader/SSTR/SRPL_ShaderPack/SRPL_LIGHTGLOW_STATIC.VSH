// Vertex shader for light flares 
// ------------------------------
// This shader makes the quad face the camera and change its size according to the camera vs quad normal.
// This shader should only be applied to quads (square consisting of 2 triangles) that face forward. 
//
// Created by Siim Annuk
// Date: June 28, 2009

#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);
float4 worldEye: register (c8);
float4 fogData : register (c9);
float4 texMod : register (c11); // tex0.u=x, tex0.v=y, tex1.u=z, tex1.v=w
float4 specularColor : register (c15); // power in c15.a

struct VS_OUTPUT
{
   float4 Pos  : POSITION;
#ifdef USEFOG
   float  Fog  : FOG;
#endif   
   float3 Tex  : TEXCOORD0;
};

VS_OUTPUT SRPL_LIGHTGLOW_STATIC (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0,
                                 float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL,
                                 float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2, float2 inTex3 : TEXCOORD3)
{
  VS_OUTPUT Out;

  float2 cornerOff = float2(-1.0f, -1.0f);
  if (inColor.a < 0.5f)
  {
    if (inColor.a > 0.25f)
    {
      cornerOff.x = 1.0f;
    }
  }
  else
  {
    cornerOff.y = 1.0f;
    if (inColor.a < 0.75f)
    {
      cornerOff.x = 1.0f;
    }
  }

  float4 centerPos;
  centerPos.xyz = inTangent.xyz;
  centerPos.w = 1.0f;
  
  float3 eyeVector = normalize(mul(worldMatrix, centerPos) + worldEye);

  float3 primaryNormal;
  primaryNormal.x = sin(inTex0.x) * cos(inTex0.y);
  primaryNormal.z = cos(inTex0.x) * cos(inTex0.y);
  primaryNormal.y = sin(inTex0.y);
  primaryNormal = mul (worldMatrix, primaryNormal.xyz);


  float primaryDot = dot(-eyeVector, primaryNormal);

  float glow1 = pow(max(primaryDot, 0.0f), inBiNorm.z);
  float radius = inBiNorm.x;
  radius += inBiNorm.y * glow1;

 
  float3 cornerOff2;
  float4x4 worldViewMatrix = mul(viewMatrix, worldMatrix);
  cornerOff2 = worldViewMatrix[0] * cornerOff.x + worldViewMatrix[1] * cornerOff.y;

  float3 eyeOffset;
  eyeOffset.x = dot(float3(worldMatrix[0].x, worldMatrix[1].x, worldMatrix[2].x), eyeVector);
  eyeOffset.y = dot(float3(worldMatrix[0].y, worldMatrix[1].y, worldMatrix[2].y), eyeVector);
  eyeOffset.z = dot(float3(worldMatrix[0].z, worldMatrix[1].z, worldMatrix[2].z), eyeVector);

  float3 normalOffset;
  normalOffset.x = dot(float3(worldMatrix[0].x, worldMatrix[1].x, worldMatrix[2].x), primaryNormal);
  normalOffset.y = dot(float3(worldMatrix[0].y, worldMatrix[1].y, worldMatrix[2].y), primaryNormal);
  normalOffset.z = dot(float3(worldMatrix[0].z, worldMatrix[1].z, worldMatrix[2].z), primaryNormal);

  centerPos.xyz += normalOffset * inTex2.y;
  centerPos.xyz -= normalize(eyeOffset) * inColor.r * 25.5;
  centerPos.xyz += cornerOff2 * radius;
  
  float alpha = 1.0f;
  if (inTex3.y > inTex3.x)
  {
    alpha = saturate(((primaryDot * 0.5f + 0.5f) - inTex3.x) / (inTex3.y - inTex3.x));
  }

  Out.Pos = mul (viewProjMatrix, centerPos);

#ifdef USEFOG
  Out.Fog.x = 100000.0f;
#endif
  
  Out.Tex.z = alpha * inColor.g * 2.0;
  Out.Tex.xy = cornerOff * float2(0.5f, 0.5f) + float2(0.5f, 0.5f);

  return (Out);
}








