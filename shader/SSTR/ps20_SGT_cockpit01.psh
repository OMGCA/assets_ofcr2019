float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;

   float4 Tex0    : TEXCOORD0; // base0 + specular map
   float3 Normal  : TEXCOORD1; // world space vertex normal
   float3 EyeVec  : TEXCOORD2; // world space eye vec
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0: register (s0);
sampler2D sSpec: register (s1);
 
PS_OUTPUT ps20_sgt_cockpit01 (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  // Fresnel parameters
  float fresnelBias = 0.3;
  float fresnelScale = 2.2;
  float fresnelPower = 4.0;

  float4 tex = tex2D (sTex0, Input.Tex0.xy);        // sample base tex

  float4 spec = tex2D (sSpec, Input.Tex0.zw);  // sample spec tex (gloss)

  float3 normal = normalize (Input.Normal);

  float3 normEyeVec = normalize (Input.EyeVec);
  
  // compute specular color
  float3 lightVec = normalize(worldLight); 
  float3 reflected = reflect(normEyeVec, normal);

  half4 specular;
  half fspecular = max(dot (lightVec, reflected), 0.0);
  specular.rgb = float3(1.4, 1.3, 1.2) * pow(fspecular, 20.0);
  specular.rgb += float3(0.3, 0.29, 0.28) * pow(fspecular, 2.0);
  specular.rgb += float3(0.8, 0.8, 0.8);
  specular.rgb -= float3(0.6, 0.5, 0.35) * pow(max(reflected.y, 0.0), 0.5);
  specular.rgb -= float3(1.0, 1.0, 1.0) * pow(max(-reflected.y, 0.0), 0.5);
  specular.rgb = max(specular.rgb, float3(0.1, 0.1, 0.1));

  specular.a = 0;

  // compute diffuse color
  half4 diffuse = saturate ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor + Input.Omni) * tex;
  
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));
  float fresnel = fresnelBias + fresnelScale * pow(1 - dotEyeVecNormal, fresnelPower);

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day;

  Out.Color = (diffuse * 0.3 + specular * spec * fresnel) * Input.Color;
//Out.Color.rgb = spec.rgb;
//Out.Color.a = 1.0;

  return (Out);
}
