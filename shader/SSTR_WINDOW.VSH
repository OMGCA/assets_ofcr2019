#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float4 Omni   : COLOR1;
#ifdef USEFOG
   float  Fog    : FOG;
#endif   

 
   float2 Tex0   : TEXCOORD0; // base texture and mask
   float2 Tex1   : TEXCOORD1; // dirt pattern texture
   float3 Normal : TEXCOORD2; // world space vertex normal
   float3 EyeVec : TEXCOORD3; // world space eye vec
   float2 Params : TEXCOORD4; // Specular power and scale
};

VS_OUTPUT SSTR_WINDOW (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  Out.Normal = worldNorm;
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec = eyeVec;
  
#ifdef USEFOG
  // compute fog
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif   

  // Compute omnilights
  Out.Omni = (0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }
  Out.Omni.rgb *= 0.5F;


  // Propagate color and texture coordinates:
  Out.Diff = inColor;

  Out.Tex0 = inTex0;
  Out.Tex1 = inTex1;
  Out.Params = inTex2;

  return (Out);
}

