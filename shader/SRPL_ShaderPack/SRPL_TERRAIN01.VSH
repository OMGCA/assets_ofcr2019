float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 sunLight : register (c13);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;
   float4 TexCoord01    : TEXCOORD0;    // Map, Layer 1
   float4 TexCoord23    : TEXCOORD1;    // Layer 2, Layer 3
   float3 Normal        : TEXCOORD2;    // xyz - normal
   float3 EyeVec        : TEXCOORD4;    // xyz - eyeVec
   float3 Tangent       : TEXCOORD5;    // xyz - tangent
   float3 Binormal      : TEXCOORD6;    // xyz - binormal
};



VS_OUTPUT SRPL_TERRAIN01 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0,
                              float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2, float2 inTex3 : TEXCOORD3)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = normalize(mul (worldMatrix, inNorm.xyz));
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec = eyeVec;

  float3 worldS = mul(worldMatrix, float3(1.0, 0.0, 0.0));
  float3 worldT = mul(worldMatrix, float3(0.0, 0.0, -1.0));

  float3 worldSxT = normalize(mul (worldMatrix, inNorm.xyz));
  worldS = normalize(worldS - worldSxT * dot(worldSxT, worldS));
  worldT = normalize(worldT - worldSxT * dot(worldSxT, worldT));
  Out.Tangent = worldS;
  Out.Binormal = worldT;
  Out.Normal = worldSxT;

  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

#ifdef OMNISCALE
  // Compute omnilights
  Out.Omni = float4(0,0,0,0);
  for (int i=0; i< numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);

    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
    {
      intens = dot (worldNorm, normalize (omniToVert)); // check backface
      if (intens > 0.0)
      {
        Out.Omni.rgb += (intens * (1.0F - (sqrt(mag) / sqrt(omniPosRad[i].w) )));
      }
    }
  }
  Out.Omni.rgb *= OMNISCALE;
#endif

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.TexCoord01.xy = inTex1;
  Out.TexCoord01.zw = inTex1 * inTex2.x;
  Out.TexCoord23.xy = inTex1 * inTex2.y;
  Out.TexCoord23.zw = inTex1 * inTex3.x;
  return (Out);
}

