float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 dirLight : register (c14);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float3 Tex    : TEXCOORD0; // base
   float4 TexOff : TEXCOORD1; // texcoord offsets for two blended images
   float3 EyeVec : TEXCOORD3;
   float3 SunPos : TEXCOORD4;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sTex1 : register (s1);
samplerCUBE sTex2 : register (s2);
sampler2D sTex3 : register (s3);


PS_OUTPUT SRPL_SKY02 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  float3 LightVec = Input.SunPos.xyz;
  float3 EyeVec = Input.EyeVec;
  float3 normEyeVec = normalize(EyeVec);

  float2 texc;
  texc.y = max(0.0, EyeVec.y) / 8.25 + 0.002;
  texc.x = distance(LightVec.xz, EyeVec.xz) / 16.5 + 0.003;

  float4 cube = texCUBE (sTex2, normEyeVec);


  //stars
  float4 tex = tex2D (sTex1, Input.Tex.xy) * (0.5 - 0.45 * diffuseColor.r);

  //atmosphere
  float4 tex1 = tex2D(sTex0, texc + Input.TexOff.xy);
  float4 tex2 = tex2D(sTex0, texc + Input.TexOff.zw);
  float3 skyColor = lerp(tex1.rgb, tex2.rgb, Input.Tex.z);

float3 cirrusColor = tex2D(sTex3, float2(distance(LightVec, normEyeVec) * 0.5, LightVec.y * 0.5 + 0.5)).rgb;
skyColor = lerp(skyColor, cirrusColor * 2.0, cube.r * 0.5);

  Out.Color.rgb = skyColor + tex.rgb;

  Out.Color.a = 1.0;

  return (Out);
}
