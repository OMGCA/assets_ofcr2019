float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
#if NUMTEX == 1
   float2 Tex    : TEXCOORD0; // base
#else   
   float4 Tex    : TEXCOORD0; // base
#endif   
#if NUMTEX > 3
   float4 Tex1   : TEXCOORD1; // base
#elif NUMTEX > 2
   float2 Tex1   : TEXCOORD1; // base
#endif   
   float3 Normal : TEXCOORD2;
   bool3  Blend  : TEXCOORD3; // not used yet
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};
 sampler2D sTex0 : register (s0);
#if NUMTEX > 1
 sampler2D sTex1 : register (s1);
#endif   
#if NUMTEX > 2
 sampler2D sTex2 : register (s2);
#endif   
#if NUMTEX > 3
 sampler2D sTex3 : register (s3);
#endif   

#if NUMTEX == 1

 PS_OUTPUT SRPL_L25_blinnDiffuseT0 (PS_INPUT Input)

#elif NUMTEX == 2

 #if   LERPALPHA == 1
   PS_OUTPUT SRPL_L25_blinnDiffuseT0lerpT1VertexAlpha (PS_INPUT Input)
 #elif ADDALPHA == 1
   PS_OUTPUT SRPL_L25_blinnDiffuseT0T1VertexAlpha (PS_INPUT Input)
 #elif MULALPHA == 1
   PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1VertexAlpha (PS_INPUT Input)
   
 #elif LERPTEX == 1
   PS_OUTPUT SRPL_L25_blinnDiffuseT0lerpT1 (PS_INPUT Input)
 #elif MULTEX == 0
  PS_OUTPUT SRPL_L25_blinnDiffuseT0T1 (PS_INPUT Input)
 #elif MULTEX == 1
   PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1 (PS_INPUT Input)
 #endif  

#elif NUMTEX == 3

 #if MULTEX == 0
  PS_OUTPUT SRPL_L25_blinnDiffuseT0T1T2 (PS_INPUT Input)
 #elif MULTEX == 1
  PS_OUTPUT SRPL_L25_blinnDiffuseT0T1xT2 (PS_INPUT Input)
 #elif MULTEX == 2
  PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1T2 (PS_INPUT Input)
 #elif MULTEX == 3
  PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1xT2 (PS_INPUT Input)
 #endif

#elif NUMTEX == 4
 
 #if MULTEX == 0
  PS_OUTPUT SRPL_L25_blinnDiffuseT0T1T2T3 (PS_INPUT Input)
 #elif MULTEX == 1
  PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1T2T3 (PS_INPUT Input)
 #elif MULTEX == 2
  #if LERPALPHA == 1
   PS_OUTPUT SRPL_L25_blinnDiffuseT0LerpT1xT2T3VertexAlpha (PS_INPUT Input)
  #else
   PS_OUTPUT SRPL_L25_blinnDiffuseT0T1xT2T3 (PS_INPUT Input)
  #endif 
 #elif MULTEX == 3
  PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1xT2T3 (PS_INPUT Input)
 #elif MULTEX == 4
  PS_OUTPUT SRPL_L25_blinnDiffuseT0T1T2xT3 (PS_INPUT Input)
 #elif MULTEX == 5
  PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1T2xT3 (PS_INPUT Input)
 #elif MULTEX == 6
  PS_OUTPUT SRPL_L25_blinnDiffuseT0T1xT2xT3 (PS_INPUT Input)
 #elif MULTEX == 7
  PS_OUTPUT SRPL_L25_blinnDiffuseT0xT1xT2xT3 (PS_INPUT Input)
 #endif
 
#endif
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex0 = tex2D (sTex0, Input.Tex.xy);  // sample tex0

#if NUMTEX == 2

  float4 tex1 = tex2D (sTex1, Input.Tex.zw);
  
 #if   LERPALPHA == 1
  tex0 = lerp (tex1, tex0, Input.Color.a);
 #elif ADDALPHA == 1
  tex0 = tex0 + (tex1 * Input.Color.a);
 #elif MULALPHA == 1
  tex0 = tex0 * (tex1 * Input.Color.a);

 #elif LERPTEX == 1
  Out.Color = lerp (tex0, tex1, blendPct.a) * Input.Color;
 #elif MULTEX == 0 
  tex0 = tex0 + tex1;
 #elif MULTEX == 1
  tex0 = tex0 * tex1;
 #endif 
 
#elif NUMTEX == 3

  float4 tex1 = tex2D (sTex1, Input.Tex.zw);
  float4 tex2 = tex2D (sTex2, Input.Tex1.xy);

 #if   MULTEX == 0
  tex0 = tex0 + tex1 + tex2;
 #elif MULTEX == 1
  tex0 = tex0 + tex1 * tex2;
 #elif MULTEX == 2
  tex0 = tex0 * tex1 + tex2;
 #elif MULTEX == 3
  tex0 = tex0 * tex1 * tex2;
 #endif 
 
#elif NUMTEX == 4

  float4 tex1 = tex2D (sTex1, Input.Tex.zw);
  float4 tex2 = tex2D (sTex2, Input.Tex1.xy);
  float4 tex3 = tex2D (sTex3, Input.Tex1.zw);
  
 #if   MULTEX == 0
  tex0 = tex0 + tex1 + tex2 + tex3;
 #elif MULTEX == 1
  tex0 = tex0 * tex1 + tex2 + tex3;
 #elif MULTEX == 2
  #if LERPALPHA == 1
   tex0 = lerp (tex1, tex0, Input.Color.a) * tex2 + tex3;
  #else
   tex0 = tex0 + tex1 * tex2 + tex3;
  #endif 
 #elif MULTEX == 3
  tex0 = tex0 + tex1 + tex2 * tex3;
 #elif MULTEX == 4
  tex0 = tex0 * tex1 * tex2 + tex3;
 #elif MULTEX == 5
  tex0 = tex0 * tex1 + tex2 * tex3;
 #elif MULTEX == 6
  tex0 = tex0 + tex1 * tex2 * tex3;
 #elif MULTEX == 7
  tex0 = tex0 * tex1 * tex2 * tex3;
 #endif
 
#endif   
  
 #ifndef LERPTEX
  // compute diffuse color
  float3 normal = normalize (Input.Normal.xyz);
  half4 diffuse = ((saturate (dot (worldLight, normal)) * diffuseColor * float4(1.5, 1.5, 1.5, 1.0)) + ambientColor + Input.Omni);
  Out.Color = (diffuse * saturate (tex0)) * Input.Color;
 #endif  
  return (Out);
}
