float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;

   float2 Tex0    : TEXCOORD0; // base0
   float3 EyeVec  : TEXCOORD1; // world space eye vec
//   float3 Normal  : TEXCOORD2; // 
   float3 CenterVec1 : TEXCOORD3; //
   float3 CenterVec2 : TEXCOORD4; //
   float2 Radius : TEXCOORD5; //
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0: register (s0);
 
PS_OUTPUT SRPL_L25_VEGETATION02_T0 (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  float4 tex = tex2D (sTex0, Input.Tex0.xy);        // sample base tex

  if (tex.a < 0.5)
  {
    discard;
  }

  float alpha1 = tex2D (sTex0, Input.Tex0.xy + float2(-0.025, -0.025)).a;
  float alpha2 = tex2D (sTex0, Input.Tex0.xy + float2( 0.025, -0.025)).a;
  float alpha3 = tex2D (sTex0, Input.Tex0.xy + float2(-0.025,  0.025)).a;
  float alpha4 = tex2D (sTex0, Input.Tex0.xy + float2( 0.025,  0.025)).a;

  float3 normEyeVec = normalize (Input.EyeVec.xyz);
//  float3 normalVec = Input.Normal;
  float3 centerVec = Input.CenterVec1 / Input.Radius.x + Input.CenterVec2 / Input.Radius.y;
//  float3 centerVec = Input.CenterVec1 * 0.2;

  float centerVecLength = length(centerVec);
  centerVec /= max(centerVecLength, 1.0);


  
  float3 lightVec = normalize(worldLight); 
  float dotLightEye = dot(lightVec.xz, normEyeVec.xz);

  float lightThrough = pow(max(0.0, dotLightEye), 3.0);
  
  lightThrough *= min(0.4, (5.0 - tex.a - alpha1 - alpha2 - alpha3 - alpha4) / 5.0);

  float day = diffuseColor.r;
  lightThrough *= day;

  float l = saturate(dot(centerVec, lightVec) + 1.0 - dotLightEye * 0.5);

  // compute diffuse color
//  half4 diffuse = diffuseColor * l * 1.25 + ambientColor * 0.6;
  half4 diffuse = diffuseColor * l * 1.4 + ambientColor * 0.4;
//#ifdef OMNISCALE
//  diffuse = diffuse + Input.Omni * OMNISCALE;
//#endif
//
//  Out.Color = (tex * diffuse) * Input.Color;

  tex.rgb = lerp(tex.rgb * diffuse.rgb, float3(2.0, 2.5, 1.0) * tex.rgb, lightThrough);  //subsurface scattering

//tex.rgb = diffuse.rgb;
//tex.rgb = normalize(centerVec) * 0.5 + float3(0.5, 0.5, 0.5);
  Out.Color.rgba = tex.rgba;

  return (Out);
}
