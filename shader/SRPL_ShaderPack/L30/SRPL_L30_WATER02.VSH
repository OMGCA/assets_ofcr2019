float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 sunLight : register (c13);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;

   float  Fog    : FOG;
   float4 Tex0    : TEXCOORD0;
   float4 Tex1    : TEXCOORD1;
   float3 Normal : TEXCOORD2;
   float4 EyeVec : TEXCOORD3;
   float3 WaterParams : TEXCOORD4;
};

VS_OUTPUT SRPL_L30_WATER02 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0,
                            float2 inParams1 : TEXCOORD1, float2 inParams2 : TEXCOORD2)
{
  VS_OUTPUT Out;
  
  Out.WaterParams.xy = inParams1.xy;
  Out.WaterParams.z = inParams2.x;

  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  Out.Normal.xyz = float3(1.0, 0.0, 0.0);
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec.xyz = eyeVec;
 
  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
 
  // Propagate color and texture coordinates:
  Out.Diff = inColor;

//exposition
float lightDist = distance(normalize(mul(viewMatrix, normalize(dirLight)).xyz), float3(0.0, 0.0, 1.0)); //0.0 - looking at sun, 2.0 - looking away from sun
lightDist = pow(lightDist / 2.0, 1.3);
Out.EyeVec.w = lerp(-1.0, lightDist * -0.5 - 0.9, saturate(sunLight.g * 2.0 - 0.3));


  float2 offset = mul(worldMatrix, inPos).xz; // + worldEye.xz;

////////////////////////////////////////
float2 vec1 = normalize(dirLight.rb);

float angle;

  if (vec1.x == 0)
  {
    if (vec1.y > 0)
    {
      angle = 0.0;
    }
    else
    {
      angle = 3.1415926535;
    }
  }

  if (vec1.y == 0)
  {
    if (vec1.x > 0)
    {
      angle = 0.5 * 3.1415926535;
    }
    else
    {
      angle = 1.5 * 3.1415926535;
    }
  }

  if (vec1.y > 0)
  {
    angle = atan(vec1.x / vec1.y);
  }
  else
  {
    angle = 3.1415926535 + atan(vec1.x / vec1.y);
  }
  float animation = angle / 6.2832;
////////////////////////////////////////
  animation *= 1440.0; //1 full cycle per 1 minute

  Out.Tex0.xy = offset * 0.030 + float2( 0.21,  0.27) * animation;
  Out.Tex0.zw = offset * 0.024 - float2( 0.26,  0.22) * animation;
  Out.Tex1.xy = offset * 0.008 + float2( 0.31,  0.38) * animation;
  Out.Tex1.zw = offset * 0.006 - float2( 0.35,  0.29) * animation;


  return (Out);
}

