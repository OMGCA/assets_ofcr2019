float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

#define exposition(op1, op2) float3(1.2, 1.2, 1.2) - pow(float3(2.7, 2.7, 2.7), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color  : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;

   float2 Tex0    : TEXCOORD0; // base0
   float4 EyeVec  : TEXCOORD1; // world space eye vec
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0: register (s0);
sampler2D sTex1: register (s1);
 
PS_OUTPUT SRPL_L30_VEGETATION01_T0T1 (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  float4 tex0 = tex2D (sTex0, Input.Tex0.xy);        // sample base tex
  float4 tex1 = tex2D (sTex1, Input.Tex0.xy);        // sample sss tex

  if (tex0.a < 0.5)
  {
    discard;
  }

  float3 normEyeVec = normalize (Input.EyeVec.xyz);
  
  float3 lightVec = normalize(worldLight); 

  float dotLightEye = dot(lightVec.xz, normEyeVec.xz);

  float lightThrough = pow(max(0.0, dotLightEye), 3.0);
  float lightInFront = 0.8 - dot(lightVec.xz, normEyeVec.xz) * 0.2;

//  lightThrough *= Input.Params.x;
  
  lightThrough *= tex1.a;

  float day = diffuseColor.r;
  lightThrough *= day;


  // compute diffuse color
  half4 diffuse = diffuseColor * 1.2 + ambientColor;
//#ifdef OMNISCALE
//  diffuse = diffuse + Input.Omni * OMNISCALE;
//#endif
//
//  Out.Color = (tex * diffuse) * Input.Color;


  tex0.rgb = lerp(tex0.rgb * diffuse.rgb * lightInFront, tex1.rgb, lightThrough);  //subsurface scattering

  tex0.rgb = exposition(tex0.rgb, Input.EyeVec.w);
  Out.Color.rgba = tex0;

  return (Out);
}
