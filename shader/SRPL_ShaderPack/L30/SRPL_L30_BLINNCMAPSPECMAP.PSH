float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

#define exposition(op1, op2) float4(1.2, 1.2, 1.2, (op1).a) - pow(float4(2.7, 2.7, 2.7, 0.0), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
#if NUMTEX == 1   
   float2 Tex    : TEXCOORD0; // base
#else
   float4 Tex    : TEXCOORD0; // base
#endif   
   float2 Spec   : TEXCOORD1; // spec
   float3 Normal : TEXCOORD2;
   float4 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);

#if NUMTEX > 1

 sampler2D sTex1: register (s1);
 sampler2D sSpec : register (s2);
 samplerCUBE sCube : register (s3);

 #if MULTEX == 0
  PS_OUTPUT SRPL_L30_blinnCmapSpecMapT0T1 (PS_INPUT Input)
 #elif MULTEX == 1 
  PS_OUTPUT SRPL_L30_blinnCmapSpecMapT0xT1 (PS_INPUT Input)
 #endif 

#else
 
 sampler2D sSpec : register (s1);
 samplerCUBE sCube : register (s2);
 
 #if ADDALPHA == 1
   PS_OUTPUT SRPL_L30_blinnCmapSpecMapAddGrayscaleReflectT0 (PS_INPUT Input)
 #elif SWAPALPHA == 1
   PS_OUTPUT SRPL_L30_blinnCmapSpecMapAlphaSwapSpecMapT0 (PS_INPUT Input)
 #else  
   PS_OUTPUT SRPL_L30_blinnCmapSpecMapT0 (PS_INPUT Input)
 #endif  

#endif
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);                // sample base tex
#if NUMTEX > 1
  float4 tex1 = tex2D (sTex1, Input.Tex.zw);  // sample base tex
 #if MULTEX == 0
   tex = saturate (tex + tex1);
 #elif MULTEX == 1 
   tex = (tex * tex1);
 #endif
#endif

  float4 spec = tex2D (sSpec, Input.Spec);                // sample spec tex
  float3 normal = normalize (Input.Normal);
  float3 normEyeVec = normalize (Input.EyeVec.xyz);
  
  float3 halfway = normalize (worldLight - normEyeVec); 
  half4 specular;
#if SWAPALPHA == 1
  specular.rgb = saturate (pow (dot (halfway, normal), specularColor.a) + Input.Omni) * specularColor * tex.a;
#else
  specular.rgb = saturate (pow (dot (halfway, normal), specularColor.a) + Input.Omni) * specularColor * spec;
#endif  
  specular.a = 0;
  // compute diffuse color
  half4 diffuse = ((saturate (dot (worldLight, normal))) * diffuseColor * float4(1.65, 1.65, 1.65, 1.0) + ambientColor + Input.Omni);

  // compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float4 cube = texCUBE (sCube, reflect (normEyeVec, normal));
  cube.a = 1.0;
  
#if ADDALPHA == 1
  half4 grayScale = {0.30, 0.59, 0.11, 0.0};
  Out.Color = exposition(((tex * diffuse) + specular + (cube * dot (spec, grayScale) * diffuse)) * Input.Color, Input.EyeVec.w);
#elif SWAPALPHA == 1
  Out.Color = exposition(((tex * diffuse) + specular + (cube * spec * diffuse)) * Input.Color, Input.EyeVec.w);
#else  
  Out.Color = exposition(lerp ((tex * diffuse) + specular, cube * diffuse, blendPct.a) * Input.Color, Input.EyeVec.w);
#endif
  
  return (Out);
}
