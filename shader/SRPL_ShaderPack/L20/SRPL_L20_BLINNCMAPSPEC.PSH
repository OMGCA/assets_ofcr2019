float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
#if NUMTEX == 1   
   float2 Tex    : TEXCOORD0; // base
#else
   float4 Tex    : TEXCOORD0; // base
#endif   
   float3 Normal : TEXCOORD1;
   float3 EyeVec : TEXCOORD2;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);

#if NUMTEX > 1

 sampler2D sTex1: register (s1);
 samplerCUBE sCube : register (s2);

 #if MULTEX == 0
  PS_OUTPUT SRPL_L20_blinnCmapSpecT0T1 (PS_INPUT Input)
 #elif MULTEX == 1 
  PS_OUTPUT SRPL_L20_blinnCmapSpecT0xT1 (PS_INPUT Input)
 #endif 

#else
 
 samplerCUBE sCube : register (s1);

 #if ADD == 1
  PS_OUTPUT SRPL_L20_blinnCmapSpecAddT0 (PS_INPUT Input)
 #elif ADDALPHA == 1
  PS_OUTPUT SRPL_L20_blinnCmapSpecAddAlphaReflectT0 (PS_INPUT Input)
 #else 
  PS_OUTPUT SRPL_L20_blinnCmapSpecT0 (PS_INPUT Input)
 #endif 

#endif
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);                // sample base tex
#if NUMTEX > 1
  float4 tex1= tex2D (sTex1, Input.Tex.zw);  // sample base tex
 #if MULTEX == 0
   tex = saturate (tex + tex1);
 #elif MULTEX == 1 
   tex = (tex * tex1);
 #endif
#endif

  float3 normal = normalize (Input.Normal);
  
  float3 halfway = normalize (worldLight - normalize(Input.EyeVec.xyz)); 
  half4 specular;
  specular.rgb = saturate (pow (dot (halfway, normal), specularColor.a) + Input.Omni) * specularColor.rgb;
  specular.a = 0;
  // compute diffuse color
  half4 diffuse = ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor + Input.Omni);
  // compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 eyeVecNorm = (Input.EyeVec.xyz);
  float4 cube = texCUBE (sCube, reflect (eyeVecNorm, normal));
  cube.a = 1.0;
  
 #if ADD == 1
  Out.Color = ((tex * diffuse) + (cube * diffuse)) + specular * Input.Color;
 #elif ADDALPHA == 1
  Out.Color = ((tex * diffuse) + (cube * diffuse * tex.a)) + specular * Input.Color;
 #else 
  // original way - cube alpha is lerped into the final pixel
  Out.Color = lerp ((tex * diffuse) + specular, (cube * diffuse), blendPct.a) * Input.Color;
  //Out.Color = (lerp ((tex * diffuse), (cube * diffuse), blendPct.a) + specular) * Input.Color;
  // new way - cube alpha is NOT included in the final pixel
  //Out.Color.rgb = lerp ((tex * diffuse) + specular, (cube * diffuse), blendPct.a) * Input.Color;
  //Out.Color.a = tex.a * diffuse.a * Input.Color.a;
 #endif

  return (Out);
}
