float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

#define exposition(op1, op2) float3(1.2, 1.2, 1.2) - pow(float3(2.7, 2.7, 2.7), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float  Fog    : FOG;

   float3 Tex    : TEXCOORD0;
   float4 Tex0   : TEXCOORD1;
   float4 Tex1   : TEXCOORD2;
  
   float3 Normal : TEXCOORD3;
   float3 Light  : TEXCOORD4;
   float4 EyeVec : TEXCOORD5;
   float4 WaterParams : TEXCOORD6;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sBump : register (s1);

PS_OUTPUT SRPL_L30_UNDERWATER01 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  float dayTime = saturate(ambientColor.b * 2.0);
  float3 waterColor = Input.Color.rgb;
  waterColor *= dayTime;
  
  // Sample the textures
  float3 n1 = tex2D (sBump, Input.Tex0.xy).xyz - float3(0.5, 0.5, 0.5);
  float3 n2 = tex2D (sBump, Input.Tex0.zw).xyz - float3(0.5, 0.5, 0.5);
  float3 n3 = tex2D (sBump, Input.Tex1.xy).xyz - float3(0.5, 0.5, 0.5);
  float3 n4 = tex2D (sBump, Input.Tex1.zw).xyz - float3(0.5, 0.5, 0.5);
  float3 bump = normalize(n1 * 0.3 + n2 * 0.4 + n3 * 1.0 + n4 * 1.2).xzy;

  float waterDepth = max(Input.Tex.z, 0.01);

  float caustics = 1.0 - min(abs(0.975 - bump.y) * 25.0, 1.0);
  caustics = pow(caustics, 2.0);
  caustics *= saturate(waterDepth);
  caustics *= 1.0 - saturate(waterDepth / Input.WaterParams.w);
  caustics *= Input.WaterParams.z;

  // sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);

  float3 normVec = normalize (Input.Normal);
  float3 normLight = normalize (Input.Light);

  // compute diffuse color
  half4 diffuse = saturate(dot (normLight, normVec)) * diffuseColor + ambientColor;
//  diffuse = saturate (diffuse);
  diffuse.rgb += waterColor * caustics;
  tex.rgb *= diffuse.rgb;

  waterDepth /= max(-Input.EyeVec.y, waterDepth);
  waterDepth *= length(Input.EyeVec.xyz);
  waterDepth = max(waterDepth, 0.0);
  waterDepth = saturate(1.0 - pow(2.7, -waterDepth * Input.WaterParams.y));

  tex.rgb = lerp(tex.rgb, waterColor, waterDepth);

  tex.rgb = exposition(tex.rgb, Input.EyeVec.w);

  // Final color
  Out.Color = tex;

  return (Out);
}
