#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog    : FOG;
   float3 Tex    : TEXCOORD0; // xy - base texcoord, z - blended textures mix rate
   float4 TexOff : TEXCOORD1; // texcoord offsets for two blended images
   float3 EyeVec : TEXCOORD3; // world space eye vec
   float3 SunPos : TEXCOORD4; // world space sun position
};

VS_OUTPUT SRPL_SKY02 (float4 inPos : POSITION, float4 inColor : COLOR0,
                      float2 inTex0 : TEXCOORD0, float2 inParam1 : TEXCOORD1,
                      float2 inParam2 : TEXCOORD2, float2 inParam3 : TEXCOORD3)
{
  VS_OUTPUT Out;

  // override fog
  Out.Fog = 1.0;

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex0;






float LAT = inParam2.x;
float NORTH = inParam2.y;

  float3x3 sunMatrix;
  sunMatrix[0] = float3(1.0, 0.0, 0.0);
  sunMatrix[1] = float3(0.0, sin(-LAT), cos(-LAT));
  sunMatrix[2] = float3(0.0, -cos(-LAT), sin(-LAT));

  float3x3 northMatrix;
  northMatrix[0] = float3(cos(NORTH), 0.0, -sin(NORTH));
  northMatrix[1] = float3(0.0, 1.0, 0.0);
  northMatrix[2] = float3(sin(NORTH), 0.0, cos(NORTH));

  sunMatrix = mul(sunMatrix, northMatrix);


float4 plane1;
plane1.xyz = sunMatrix[2];
plane1.w = inParam3.y;
float4 sunDir;
sunDir.xyz = normalize(dirLight.xyz);
sunDir.w = 1.0;


float d1 = dot(sunDir, plane1 * float4(1.0, 1.0, 1.0, 1.0));

sunDir.y -= d1 / sunMatrix[2].y;
sunDir.xyz = normalize(sunDir.xyz);


////////////////////////////////////////

float2 vec1;
vec1.x = dot(sunMatrix[0], sunDir.xyz);
vec1.y = dot(sunMatrix[1], sunDir.xyz);
vec1 = normalize(vec1);

float angle;

  if (vec1.x == 0)
  {
    if (vec1.y > 0)
    {
      angle = 0.0;
    }
    else
    {
      angle = 3.1415926535;
    }
  }

  if (vec1.y == 0)
  {
    if (vec1.x > 0)
    {
      angle = 0.5 * 3.1415926535;
    }
    else
    {
      angle = 1.5 * 3.1415926535;
    }
  }

  if (vec1.y > 0)
  {
    angle = atan(vec1.x / vec1.y);
  }
  else
  {
    angle = 3.1415926535 + atan(vec1.x / vec1.y);
  }
////////////////////////////////////////


Out.SunPos = sunDir.xyz;


float3x3 sunMatrix2;
sunMatrix2[0] = float3( sin(angle), 0.0, cos(angle));
sunMatrix2[1] = float3(0.0,         1.0,        0.0);
sunMatrix2[2] = float3(-cos(angle), 0.0, sin(angle));

sunMatrix = mul(sunMatrix, sunMatrix2);

float4 pos;
pos.xyz = mul(sunMatrix, inPos.xyz);
pos.w = 1.0;

Out.Pos = mul(viewProjMatrix, pos);
Out.EyeVec = normalize(pos.xyz);


  float fImageIndex = atan2(sunDir.y, length(sunDir.xz));
  fImageIndex = fImageIndex * 31.0 + 32.0;
  fImageIndex = max(0.0, fImageIndex);
  fImageIndex = min(62.0, fImageIndex);
  int imageIndex = int(fImageIndex);

  Out.Tex.z = fImageIndex - float(imageIndex);
  
  int2 imageCoord;
  imageCoord.y = imageIndex / 8;
  imageCoord.x = 7 - imageIndex - imageCoord.y * 8;
  Out.TexOff.xy = float2(imageCoord) / 8.0;

  imageIndex = imageIndex + 1;
  imageCoord.y = imageIndex / 8;
  imageCoord.x = 7 - imageIndex - imageCoord.y * 8;
  Out.TexOff.zw = float2(imageCoord) / 8.0;

  return (Out);
}

