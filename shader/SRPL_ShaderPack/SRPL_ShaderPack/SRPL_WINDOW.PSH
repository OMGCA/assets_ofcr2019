float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;

   float2 Tex0   : TEXCOORD0;
   float2 Tex1   : TEXCOORD1;
   float3 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
   float2 Params : TEXCOORD4;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sTex1 : register (s1);
sampler2D sTex2 : register (s2);

PS_OUTPUT SRPL_WINDOW (PS_INPUT Input)
{
  PS_OUTPUT Out;

  // sample the textures
  float4 overlay = tex2D (sTex0, Input.Tex0);
  float mask     = tex2D (sTex1, Input.Tex0).r;
  float details  = tex2D (sTex2, Input.Tex1).r;

  float day = saturate(diffuseColor.r + diffuseColor.g + diffuseColor.b - 1.0);
  overlay.rgb *= day;

  float3 normal = normalize (-1.0 * Input.Normal);
  float3 normEyeVec = normalize (Input.EyeVec);
  

  // compute glass color 
  float highlight;
  highlight = max(dot (worldLight.xyz, normEyeVec), 0.05);
  highlight = pow(highlight, Input.Params.x - details * Input.Params.x);


  float4 color = diffuseColor + ambientColor;
  color.a = details * highlight * mask * Input.Params.y;

  Out.Color = lerp(color, overlay, overlay.a);

  return (Out);
}
