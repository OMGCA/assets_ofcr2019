float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
   float4 Tex    : TEXCOORD0; // base
   float3 Normal : TEXCOORD1;
   float3 EyeVec : TEXCOORD2;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

 sampler2D sTex0 : register (s0);
 samplerCUBE sCube : register (s1);

 PS_OUTPUT ps20_blinnFresnelCmapSpecAddT0 (PS_INPUT Input)
{
  PS_OUTPUT Out;

  // Fresnel parameters  
  float fresnelBias = 0.15;
  float fresnelScale = 1.9;
  float fresnelPower = 5.0;

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);
  float3 normal = normalize (Input.Normal);
  float3 normEyeVec = normalize (Input.EyeVec);
  
  float3 halfway = normalize (worldLight - normEyeVec); 
  half4 specular;
  specular.rgb = saturate (pow (dot (halfway, normal), specularColor.a)) * specularColor.rgb;
  specular.a = 0;

  // Compute diffuse color
  half4 diffuse = saturate ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor + Input.Omni);

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 eyeVecNorm = (Input.EyeVec);
  float4 cube = texCUBE (sCube, reflect (eyeVecNorm, normal));
  cube.a = 1.0;
  
  // Calculate fresnel
  float fresnel = min(fresnelBias + fresnelScale * pow(1 - abs(dot(normEyeVec, normal)), fresnelPower), 1.0);

  // Cube diffusion
  float cubeDiffuse = diffuseColor + (Input.Omni.r * 0.333) + (Input.Omni.g * 0.333) + (Input.Omni.b * 0.333);

  // Final color
  Out.Color = ((tex * diffuse) + (cube * cubeDiffuse * fresnel)) + specular * Input.Color;
  Out.Color.a = tex.a;
  return (Out);
}
