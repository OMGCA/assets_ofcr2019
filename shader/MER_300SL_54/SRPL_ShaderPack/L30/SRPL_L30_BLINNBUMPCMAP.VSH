#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye: register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 sunLight : register (c13);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);

int4 numOmniLight : register (i0); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos   : POSITION;
   float4 Diff  : COLOR0;
   float4 Omni  : COLOR1;
#ifdef USEFOG
   float  Fog   : FOG;
#endif   
#if NUMTEX == 1   
   float2 Tex    : TEXCOORD0; // base
#else
   float4 Tex    : TEXCOORD0; // bump map
#endif   
   float2 Bump   : TEXCOORD1; // bump map
   float3 Light  : TEXCOORD2; // tangent space light
   float4 EyeVec : TEXCOORD3; // tangent space eye vec

   float3 Basis1 : TEXCOORD4; // tangent space eye vec
   float3 Basis2 : TEXCOORD5; // tangent space eye vec
   float3 Basis3 : TEXCOORD6; // tangent space eye vec
};

#if NUMTEX == 1
 VS_OUTPUT SRPL_L30_blinnBumpCmapT0 (float4 inPos : POSITION, float3 inNorm : NORMAL, float4 inColor : COLOR0, float3 inTangent : TANGENT, float3 inBiNorm : BINORMAL, float2 inTex0 : TEXCOORD0, float2 inBump : TEXCOORD1)
#else
 VS_OUTPUT SRPL_L30_blinnBumpCmapT0T1 (float4 inPos : POSITION, float3 inNorm : NORMAL, float4 inColor : COLOR0, float3 inTangent : TANGENT, float3 inBiNorm : BINORMAL, float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inBump : TEXCOORD2)
#endif 
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);
  float3 worldPos = mul (worldMatrix, inPos);

  float3 worldS;
  float3 worldT;
  float3 worldSxT;
  worldS = mul (worldMatrix, inTangent.xyz);
  worldT = mul (worldMatrix, inBiNorm.xyz);
  worldSxT = mul (worldMatrix, inNorm.xyz);
  
  Out.Basis1.x = worldS.x;
  Out.Basis1.y = worldT.x;
  Out.Basis1.z = worldSxT.x;
  Out.Basis2.x = worldS.y;
  Out.Basis2.y = worldT.y;
  Out.Basis2.z = worldSxT.y;
  Out.Basis3.x = worldS.z;
  Out.Basis3.y = worldT.z;
  Out.Basis3.z = worldSxT.z;

  Out.Light = dirLight;
  Out.EyeVec.xyz = worldPos - invViewMatrix[3];

//exposition
float lightDist = distance(normalize(mul(viewProjMatrix, normalize(dirLight)).xyz), float3(0.0, 0.0, 1.0)); //0.0 - looking at sun, 2.0 - looking away from sun
lightDist = pow(lightDist / 2.0, 1.3);
Out.EyeVec.w = lerp(-1.3, lightDist * -1.3 - 0.5, saturate(sunLight.g * 2.0 - 0.3));
  
  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  float3 eyeVec = (worldEye + worldPos);

#ifdef USEFOG
  // compute fog
  Out.Fog.x = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));
#endif   
  
  // Compute omnilights
  Out.Omni = (0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }
  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Bump = inBump;
  Out.Tex.xy = inTex0;
#if NUMTEX > 1
  Out.Tex.zw = inTex1;
#endif  
  return (Out);
}

