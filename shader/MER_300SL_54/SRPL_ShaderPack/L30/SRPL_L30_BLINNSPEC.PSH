float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

#define exposition(op1, op2) max(float4(1.2, 1.2, 1.2, 0.0), op1) - pow(float4(2.7, 2.7, 2.7, 0.0), op1 * op2) * 1.2

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;
#if NUMTEX == 1
   float2 Tex0   : TEXCOORD0; // base
#else   
   float4 Tex0   : TEXCOORD0; // base
#endif   
#if NUMTEX > 1
   float2 Tex1   : TEXCOORD1; // base
   float3 Normal : TEXCOORD2;
   float4 EyeVec : TEXCOORD3;
#else
   float3 Normal : TEXCOORD1;
   float4 EyeVec : TEXCOORD2;
#endif   
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0: register (s0);

#if NUMTEX > 2

 sampler2D sTex1: register (s1);
 sampler2D sTex2: register (s2);

 #if MULTEX == 0
  PS_OUTPUT SRPL_L30_blinnSpecT0T1T2 (PS_INPUT Input)
 #elif MULTEX == 1 
  PS_OUTPUT SRPL_L30_blinnSpecT0xT1T2 (PS_INPUT Input)
 #elif MULTEX == 2 
  PS_OUTPUT SRPL_L30_blinnSpecT0T1xT2 (PS_INPUT Input)
 #elif MULTEX == 3 
  PS_OUTPUT SRPL_L30_blinnSpecT0xT1xT2 (PS_INPUT Input)
 #endif 

#elif NUMTEX > 1

 sampler2D sTex1: register (s1);

 #if MULTEX == 0
  PS_OUTPUT SRPL_L30_blinnSpecT0T1 (PS_INPUT Input)
 #elif MULTEX == 1 
  PS_OUTPUT SRPL_L30_blinnSpecT0xT1 (PS_INPUT Input)
 #endif 

#else
 
 PS_OUTPUT SRPL_L30_blinnSpecT0 (PS_INPUT Input)

#endif
{
  PS_OUTPUT Out;
  
  float4 tex = tex2D (sTex0, Input.Tex0.xy);        // sample base tex

#if NUMTEX > 2

  float4 tex1 = tex2D (sTex1, Input.Tex0.zw);  // sample base tex
  float4 tex2 = tex2D (sTex2, Input.Tex1.xy);  // sample base tex

 #if MULTEX == 0
  tex = saturate (tex + tex1 + tex2);
 #elif MULTEX == 1 
  tex = saturate (tex * tex1 + tex2);
 #elif MULTEX == 2 
  tex = saturate (tex + tex1 * tex2);
 #elif MULTEX == 3 
  tex = saturate (tex * tex1 * tex2);
 #endif

#elif NUMTEX > 1

  float4 tex1 = tex2D (sTex1, Input.Tex0.zw);  // sample base tex

 #if MULTEX == 0
  tex = saturate (tex + tex1);
 #elif MULTEX == 1 
  tex = (tex * tex1);
 #endif

#endif
 
  float3 normal = normalize (Input.Normal);
  // compute specular color
  float3 halfway = normalize (worldLight - normalize(Input.EyeVec.xyz)); 
  half4 specular;
  specular.rgb = saturate (pow (dot (halfway, normal), specularColor.a) + Input.Omni) * specularColor;
  specular.a = 0;
  // compute diffuse color
  half4 diffuse = ((saturate (dot (worldLight, normal))) * diffuseColor * float4(1.65, 1.65, 1.65, 1.0) + ambientColor + Input.Omni) * tex;
  
  Out.Color = exposition((diffuse + specular) * Input.Color, Input.EyeVec.w);
  return (Out);
}
