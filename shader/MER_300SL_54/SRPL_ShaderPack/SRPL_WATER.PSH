float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float  Fog    : FOG;

   float4 Tex0    : TEXCOORD0;
   float4 Tex1    : TEXCOORD1;
   float3 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sBump : register (s0);
samplerCUBE sCube : register (s1);

PS_OUTPUT SRPL_WATER (PS_INPUT Input) 
{
  PS_OUTPUT Out;


  // Sample the textures
  float3 n1 = tex2D (sBump, Input.Tex0.xy).xyz - float3(0.5, 0.5, 0.5);
  float3 n2 = tex2D (sBump, Input.Tex0.zw).xyz - float3(0.5, 0.5, 0.5);
  float3 n3 = tex2D (sBump, Input.Tex1.xy).xyz - float3(0.5, 0.5, 0.5);
  float3 n4 = tex2D (sBump, Input.Tex1.zw).xyz - float3(0.5, 0.5, 0.5);

  float3 bump = normalize(n1 * 0.3 + n2 * 0.4 + n3 * 1.0 + n4 * 1.2 + float3(0.0, 0.0, 1.0)).xzy;

  float3 normEyeVec = normalize (Input.EyeVec);


  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 cubeCoord;
  cubeCoord = reflect(normEyeVec, bump);

  float specularDot = saturate(dot(worldLight, cubeCoord));
  float3 specular;
  specular  = float3( 0.3,  0.25, 0.2) * pow(specularDot, 3.0);
  specular += float3( 0.6,  0.5,  0.4) * pow(specularDot, 50.0);
  specular += float3(11.0, 10.0,  9.5) * pow(specularDot, 1500.0);

  cubeCoord.y += REFLECTIONOFFSET;
  float4 cube = texCUBE (sCube, cubeCoord);
  cube.rgb += specular;

  cube.rgb *= diffuseColor;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, bump));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = min(1.0, FRESNELBASE  + FRESNELSCALE * pow(1.0 - dotEyeVecNormal, FRESNELPOWER));

  cube = lerp( float4(0.0, 0.2, 0.15, 1.0), cube, fresnel);

  Out.Color.rgba = cube.rgba;


  return (Out);
}
