float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Omni   : COLOR1;
   float  Fog    : FOG;

   float4 Tex0    : TEXCOORD0; // base0 + specular map
   float3 Normal  : TEXCOORD1; // world space vertex normal
   float3 EyeVec  : TEXCOORD2; // world space eye vec
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0: register (s0);
sampler2D sSpec: register (s1);
 samplerCUBE sCube : register (s2);

PS_OUTPUT ps20_srpl_cockpit02 (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  // Fresnel parameters
  float fresnelBias = 0.2;
  float fresnelScale = 1.6;
  float fresnelPower = 3.0;

  float4 tex = tex2D (sTex0, Input.Tex0.xy);        // sample base tex

  float4 spec = tex2D (sSpec, Input.Tex0.zw);  // sample spec tex (gloss)

  float3 normal = normalize (Input.Normal);
  
  float3 normEyeVec = normalize (Input.EyeVec);

  // compute specular color
  float3 lightVec = normalize(worldLight); 
  float3 reflected = reflect(normEyeVec, normal);

  half4 specular;
  half fspecular = max(dot (lightVec, reflected), 0.0);
  //specular.rgb = saturate ((pow (dot (halfway, normal), specularColor.a) * specularColor)) * spec;
  //specular.rgb = saturate ((pow (dot (halfway, normal), specularColor.a) * specularColor) + Input.Omni) * spec;

  // compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float4 cube = texCUBE (sCube, normalize(reflected + float3(tex.r - 0.5, spec.r - 0.2, tex.b - 0.5) * 0.4));

cube = float4(1.2, 1.6, 2.0, 1.0);

  specular = cube * 0.4 + float4(0.3, 0.3, 0.3, 1.0) * (1.0 + dot(reflected, normEyeVec) * 0.5);
  specular.rgb += float3(0.9, 0.85, 0.8) * pow(fspecular, 2.0);
  specular.a = 0;

  // compute diffuse color
  half4 diffuse = saturate ((saturate (dot (worldLight, normal))) * diffuseColor + ambientColor + Input.Omni) * tex;
  
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));
  float fresnel = fresnelBias + fresnelScale * pow(1 - dotEyeVecNormal, fresnelPower);

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day;

  Out.Color = (diffuse * 0.2 + specular * spec * fresnel) * Input.Color;

  return (Out);
}
             