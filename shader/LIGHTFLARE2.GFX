// Light flare effect shader description file
// ------------------------------------------
// 
// Created by Siim Annuk
// Date: June 28, 2009; Update: October 13, 2013

ShaderName=L2LightFlare2
{
  ShaderDesc="Light Flare (Advanced)"
  ShaderLongDesc="Advanced Light Flare. DX9"
  ShaderDowngrade=L2DiffuseT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0)
    {
      VertexShader=vs20_blinnLightFlare2
      {
        File=vs20_LightFlare2.vsh 
        Language=HLSL
        Define=(LIGHTFLARE_VARIANT, 1)
        Define=(BILLBOARD, 1)
        // Normal glow size and multiplier
        Define=(GLOWSIZE, 1.5)
        Define=(GLOWMULTIPLIER, 3)
        VertexDecl=PShadeDecl
        ShaderConstants=Default
      }
      PixelShader=ps20_blinnLightFlare2
      {
        File=ps20_LightFlare2.psh
        Language=HLSL
        Define=(LIGHTFLARE_VARIANT, 1)
        Define=(BILLBOARD, 1)
        StageState=(0, DiffuseMap, Modulate)
        SamplerState=(0, Wrap, Wrap, Wrap)
      }
    }
  }
}

ShaderName=L2LightFlareHeadlights2
{
  ShaderDesc="Light Flare - Headlights (Advanced)"
  ShaderLongDesc="Advanced Light Flare. Suitable for car headlights. DX9"
  ShaderDowngrade=L2DiffuseT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0)
    {
      VertexShader=vs20_blinnLightFlareHeadlights2
      {
        File=vs20_LightFlare2.vsh 
        Language=HLSL
        Define=(LIGHTFLARE_VARIANT, 2)
        Define=(BILLBOARD, 1)
        // Has flash effect
        Define=(HIGHFLASH, 1)
        Define=(FLASHSIZE, 1)
        Define=(FLASHMULTIPLIER, 250)
        // Normal glow size and multiplier
        Define=(GLOWSIZE, 1.5)
        Define=(GLOWMULTIPLIER, 2)
        VertexDecl=PShadeDecl
        ShaderConstants=Default
      }
      PixelShader=ps20_blinnLightFlareHeadlights2
      {
        File=ps20_LightFlare2.psh
        Language=HLSL
        Define=(LIGHTFLARE_VARIANT, 2)
        Define=(BILLBOARD, 1)
        // Has flash effect
        Define=(HIGHFLASH, 1)
        StageState=(0, DiffuseMap, Modulate)
        StageState=(1, DiffuseMap, Shader)
        SamplerState=(0, Wrap, Wrap, Wrap)
        SamplerState=(1, Wrap, Wrap, Wrap)
      }
    }
  }
}

ShaderName=L2LightsWithGlareEffect2
{
  ShaderDesc="Lights with glare effect"
  ShaderLongDesc="Lights with glare effect. Light intensity is calculated based on viewing angle and further intensifier by alpha channel. DX9"
  ShaderDowngrade=L2DiffuseT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0)
    {
      VertexShader=vs20_blinnLightGlareEffect
      {
        File=vs20_LightFlare2.vsh 
        Language=HLSL
        Define=(LIGHTFLARE_VARIANT, 3)
        Define=(BILLBOARD, 0)
        // Has flash effect
        Define=(HIGHFLASH, 1)
        Define=(FLASHSIZE, 1.01)
        Define=(FLASHMULTIPLIER, 250)
        // Normal glow size and multiplier
        Define=(GLOWSIZE, 2)
        Define=(GLOWMULTIPLIER, 2)
        VertexDecl=PShadeDecl
        ShaderConstants=Default
      }
      PixelShader=ps20_blinnLightGlareEffect
      {
        File=ps20_LightFlare2.psh
        Language=HLSL
        Define=(LIGHTFLARE_VARIANT, 3)
        Define=(BILLBOARD, 0)
        // Has flash effect
        Define=(HIGHFLASH, 1)
        StageState=(0, DiffuseMap, Modulate)
        SamplerState=(0, Wrap, Wrap, Wrap)
      }
    }
  }
}

