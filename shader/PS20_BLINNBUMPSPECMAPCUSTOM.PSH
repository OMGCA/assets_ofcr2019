#if CUSTOM == 3

   ps.1.1
  
   // ambient color in c0
   // directional color in c1
   // world light vector in c2
   // specular color in c3
  
   // vertex normal in v1
   // vertex color in v0
  
   tex t0  // get base 0 pixel
   tex t1  // get base 1 pixel
   tex t2  // get base 2 pixel
   tex t3  // get base 2 pixel
  
   dp3_sat  r1, c2, v1_bx2   // dir intensity
   mad_sat  r1, r1, c1, c0   // dir intens * dir color + ambient
  #if OMNI == 1
   add_sat  r1.rgb, r1, v0   // omni color
  #endif  
   mov      r0.w, t3.w
   lrp_sat  r0, r0.w, t1, t2 // final color
   mul      r0, r0, t0      // tex2 + tex
   mul      r0, r1, r0      // tex * color
  #if OMNI == 0
   mul      r0, r0, v0  // add vertex color
  #endif  

#else

  float4 ambientColor  : register (c0);
  float4 diffuseColor  : register (c1);
  float4 worldLight    : register (c2);
  float4 specularColor : register (c3); //power in c3.w
  float4 blendPct      : register (c4); //bump map blend in c4.z, cube map blend in c4.w
  
  struct PS_INPUT
  {
    float4 Color   : COLOR0;
    float4 Omni    : COLOR1;
    float  Fog     : FOG;
  #if CUSTOM == 1
    float4 Tex0    : TEXCOORD0; // base0=xy, spec=zw - textures swapped
    float4 Tex1    : TEXCOORD1; // bump=xy, base1=zw - textures swapped
    float3 Light   : TEXCOORD2;
    float3 EyeVec  : TEXCOORD3;
  #elif CUSTOM == 2
    float4 Tex0    : TEXCOORD0; // base0=xy, base1=zw
    float4 Tex1    : TEXCOORD1; // base2=xy, base3=zw
    float3 Normal  : TEXCOORD2;
  #endif
  };
  
  struct PS_OUTPUT
  {
    float4 Color : COLOR0;
  };
  
  #if CUSTOM == 1
  
  sampler2D sBase:  register (s0);
  sampler2D sSpec:  register (s1);
  sampler2D sBump:  register (s2);
  sampler2D sBase1: register (s3);
  
  PS_OUTPUT ps20_blinnBumpSpecMapT0LerpT1VertexAlphaCustom (PS_INPUT Input)
  {
    PS_OUTPUT Out;
    
    float4 basePixel  = tex2D (sBase, Input.Tex0.xy);  // sample base tex
    float4 specPixel  = tex2D (sSpec, Input.Tex0.zw);  // sample spec tex (gloss)
    float3 bumpPixel  = (tex2D (sBump, Input.Tex1.xy) - 0.5) * 2.0;  // sample and unbias normal
    float4 base1Pixel = tex2D (sBase1, Input.Tex1.zw);  // sample base tex
    
    basePixel = lerp (base1Pixel, basePixel, Input.Color.a);
    
    float3 normLight = (Input.Light);
      
    // compute specular color
    float3 halfway =  normalize (normLight -  normalize (Input.EyeVec)); 
      
    half4 specular;
    specular.rgb = saturate ((pow (saturate (dot (halfway, bumpPixel)), specularColor.a) * specularColor) + Input.Omni) * specPixel;
    specular.a = 0;
    
    // compute diffuse color
    half4 diffuse;
    diffuse = saturate ((saturate (dot (normLight, bumpPixel)) * diffuseColor) + ambientColor + Input.Omni) * basePixel;
      
    Out.Color = ((diffuse + specular) - (diffuse * specular * Input.Omni.a)) * Input.Color;
    return (Out);
  }
  
  #elif CUSTOM == 2
  
  sampler2D sBase0: register (s0);
  sampler2D sBase1: register (s1);
  sampler2D sBase2: register (s2);
  sampler2D sBase3: register (s3);
  
  PS_OUTPUT ps20_blinnT0MulT1LerpT2T3AlphaCustom (PS_INPUT Input)
  {
    PS_OUTPUT Out;
    
    float4 base0Pixel = tex2D (sBase0, Input.Tex0.xy);  // sample base tex
    float4 base1Pixel = tex2D (sBase1, Input.Tex0.zw);  // sample base tex
    float4 base2Pixel = tex2D (sBase2, Input.Tex1.xy);  // sample base tex
    float4 base3Pixel = tex2D (sBase3, Input.Tex1.zw);  // sample base tex
    
    float4 tex;
    tex.xyz = base0Pixel.xyz * lerp (base2Pixel.xyz, base1Pixel.xyz, base3Pixel.a);
    tex.a = base0Pixel.a;
  
    // compute diffuse color
    float3 normal = normalize (Input.Normal);
    half4 diffuse = saturate ((saturate (dot (worldLight, normal)) * diffuseColor) + ambientColor + Input.Omni);
    Out.Color = (diffuse * saturate (tex)) * Input.Color;
    return (Out);
  }
  
  #endif

#endif
