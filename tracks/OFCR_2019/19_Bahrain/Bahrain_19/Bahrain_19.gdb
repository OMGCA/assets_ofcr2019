Bahrain_19
{
  Filter Properties = *
  Attrition = 18
  TrackName = Bahrain International Circuit 
  GrandPrixName = 2019 OFCR BAHRAIN GRAND PRIX
  EventName =  19.Bahrain
  VenueName = OFCR Season 2019
  Location = Sakhir, Bahrain
  Length = 5.412 km
  Track Record = Lewis Hamilton (2016), 1:29.493
  TrackType = Pernament Road Course
  HeadlightsRequired = false
  TerrainDataFile= Bahrain_19.tdf 

  NumStartingLights=6
  GarageDepth = 1.5

  
  FormationSpeedKPH = 240
  SafetyCarSpeedKPH = 240
  RacePitKPH   = 80
  QualPitKPH   = 80
  NormalPitKPH = 60

  TestDayDay        = Friday
  TestDayStart      = 10:00
  Practice1Day      = Friday   // Friday Practice
  Practice1Start    = 14:00
  Practice1Duration = 120
  Practice2Day      = Saturday // Saturday Practice
  Practice2Start    = 19:00
  Practice2Duration = 90
  Practice3Day      = Saturday // Qualifying 1
  Practice3Start    = 19:00
  Practice3Duration = 18
  Practice4Day      = Saturday // Qualifying 2
  Practice4Start    = 17:30
  Practice4Duration = 15
  QualifyDay        = Saturday // Qualifying 3
  QualifyStart      = 19:00
  QualifyDuration   = 60
  QualifyLaps       = 12
  WarmupDay         = Sunday   // Warmup
  WarmupStart       = 18:45
  WarmupDuration    = 30
  RaceDay           = Sunday   // Grand Prix
  RaceStart         = 19:00
  RaceLaps          = 57
  RaceTime          = 120
  

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = 26
  NorthDirection = 0
  RaceDate = 3th April 2016

  SunriseAmbientRGB = (76,51,51)      
  SunriseDirectionalRGB = (229,204,178)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (90,90,90)
  DayDirectionalRGB = (255,255,240)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (160,155,140)
  SunsetDirectionalRGB = 200,190,150)
  SunsetFogRGB = (130,130,130)

  NightAmbientRGB = (25,25,25)
  NightDirectionalRGB = (51,51,51)
  NightFogRGB = (0,0,0)

  SettingsFolder = OFCR_2019

PitStopStrategies
  {

    Lewis Hamilton = 2 - 19,41

    Nico Rosberg = 2 - 21,43

    Fernando Alonso = 3 - 12,28,41

    Kimi Raikkonen = 3 - 15,34,45

    Romain Grosjean = 3 - 13,28,37

    Pastor Maldonado = 2 - 19,39

    Nico Hulkenberg = 2 - 15,35

    Sergio Perez = 2 - 16,34

    Jenson Button = 2 - 19,37

    Kevin Magnussen = 3 - 17,31,42

    Kamui Kobayashi = 2 - 15,35

    Marcus Ericcson = 3 - 11,28,42

    Max Chilton = 3 - 11,28,40

    Jules Bianchi = 3 - 13,31,43

    Esteban Gutierrez = 2 - 16,37

    Adrian Sutil = 3 - 13,26,39

    Felipe Massa = 3 - 13,28,38

    Valterri Bottas = 3 - 10,25,40

    Danill Kvyat = 3 - 10,24,39

    Jean-Eric Vergne = 2 - 11,32

    Daniel Ricciardo = 2 - 18,35

    Sebastian Vettel = 2 - 16,34

  }

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 4773
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = 2636
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 5253
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 590
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 2707
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 3359
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------