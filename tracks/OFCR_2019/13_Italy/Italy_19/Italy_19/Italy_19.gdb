Italy_19
{
  Filter Properties = *
  Attrition = 30
  TrackName = Autodromo di Monza RFE
  GrandPrixName = OFCR GRAN PREMIO D'ITALIA 2019
  EventName = 13.Italy
  VenueName = OFCR Season 2019
  Location = Vedano al Lambro (MB), Italy
  Length = 5.793 meters / 3,6 miles
  TrackType = permanent road course
  Track Record = Rubens Barrichello, F2004, 1'21"046
  HeadlightsRequired = true  // whether headlights are required at night
  TerrainDataFile = ..\Italy_12RFE.tdf        // terrain file override

  GarageDepth = 4.0
  FormationSpeedKPH = 185
  RacePitKPH = 100
  NormalPitKPH = 100
  TestDaystart = 18:30
  Practice1Day = Friday
  Practice1Start = 17:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 17:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 17:30
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 18:30
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 16:30
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 16:50
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 17:00
  RaceLaps = 53
  RaceTime = 120

  NumStartingLights=6

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = 47.65     // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 340 // the direction of North in degrees (range: 0 to 359)
  RaceDate =  2012 September 9      // default date for the race

  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)

   DayAmbientRGB = (110,115,125)
   DayDirectionalRGB = (255,240,230)
   DayFogRGB = (128,128,128) 

   SunsetAmbientRGB = (130,130,120)
   SunsetDirectionalRGB = (255,248,198)
   SunsetFogRGB = (94,94,94)

  NightAmbientRGB = (5,10,23)
  NightDirectionalRGB = (30,30,30)
  NightFogRGB = (0,0,0)

///////////////////////////SCORETOWER DATA////////////////////////////////////////////

//ScoreboardFont=Brianza_SCOREFONT.tga // default is scoreboardfont.bmp
//ScoreboardBackground=SCORETOWERBKG.tga // default is scoreboardbkg.bmp

//ScoreboardMaxEntries=6 // how many car numbers can be displayed on tower (default is 32)
//ScoreboardStartX=0 // x-position in texture to write first car number (default is 0)
//ScoreboardStartY=1 // y-position in texture to write first car number (default is 10)
//ScoreboardIncX=0 // increment in x-position for each new car number (default is 0)
//ScoreboardIncY=43 // increment in y-position for each new car number (default is 16)
//ScoreboardScaleX=2.3 // scale multiplier for x (default is 1.0)
//ScoreboardScaleY=1.8 // scale multiplier for y (default is 1.0)

//////////////////////////////////////////////////////////////////////////////////////

  SettingsFolder = OFCR_2019
  SettingsCopy = Grip.svm
  SettingsCopy = Italy_12RFE.svm
  SettingsAI = Italy_12RFE.svm
  Qualify Laptime = 82.00
  Race Laptime = 84.50
}
