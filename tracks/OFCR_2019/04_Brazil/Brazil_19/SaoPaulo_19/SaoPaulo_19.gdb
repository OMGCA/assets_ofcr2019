SaoPaulo_19
{
  Filter Properties = *
  Attrition = 30
  VenueName = OFCR Season 2019
  TrackName = Autodromo Jose Carlos Pace
  EventName = 04.Brazil
  GrandPrixName = OFCR GRANDE PREMIO DO BRAZIL 2019
  Location = Sao Paulo, Brazil
  Length = 4.31 km / 2.68 Miles \ 
  TrackType = Road Course
  Track Record = 1.11.473 by Juan Pablo Montoya, BMW Williams 2004
  TerrainDataFile = SaoPaulo_19.TDF   // RFExL3C

  FormationSpeedKPH = 200
  SafetyCarSpeedKPH=200 // la vitesse du safety
  RacePitKPH = 100
  NormalPitKPH = 100
  TestDaystart = 14:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 11:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 13:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 14:00
  RaceLaps = 71
  RaceTime = 120


  GarageDepth = 4.0

  NumStartingLights=6

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = -23
  NorthDirection = 0
  RaceDate = 9th November 2014

  SunriseAmbientRGB = (165,185,195)
  SunriseDirectionalRGB = (120,110,105) 
  SunriseFogRGB = (30,35,60)

  DayAmbientRGB = (90,90,90)
  DayDirectionalRGB = (255,255,240)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (160,155,140)
  SunsetDirectionalRGB = 200,190,150)
  SunsetFogRGB = (130,130,130)


  NightAmbientRGB = (25,27,30)
  NightDirectionalRGB = (19,20,24)
  NightFogRGB = (4,7,10)

  
  SettingsFolder = OFCR_2019

PitStopStrategies
  {

    Lewis Hamilton = 3 - 8,28,51

    Nico Rosberg = 3 - 7,26,50

    Fernando Alonso = 3 - 7,28,52

    Kimi Raikkonen = 2 - 8,35

    Romain Grosjean = 3 - 24,40,59

    Pastor Maldonado = 3 - 4,27,45

    Nico Hulkenberg = 3 - 16,36,60

    Sergio Perez = 3 - 5,25,47

    Jenson Button = 3 - 6,27,50

    Kevin Magnussen = 3 - 7,26,47

    Kamui Kobayashi = 3 - 10,32,54

    Marcus Ericcson = 3 - 8,29,55

    Max Chilton = 3 - 14,39,56

    Jules Bianchi = 3 - 15,40,58

    Esteban Gutierrez = 3 - 8,27,48

    Adrian Sutil = 3 - 18,39,49

    Felipe Massa = 3 - 5,25,50

    Valterri Bottas = 3 - 6,26,42

    Danill Kvyat = 3 - 18,38,59

    Jean-Eric Vergne = 3 - 9,27,46

    Daniel Ricciardo = 3 - 7,27,50

    Sebastian Vettel = 3 - 6,24,48
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 411
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = 3407
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 758
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 1335
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 3888
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 257
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------
 
