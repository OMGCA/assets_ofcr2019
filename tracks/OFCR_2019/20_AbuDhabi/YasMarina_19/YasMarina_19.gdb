YasMarina_19
{
  Filter Properties = *
  Attrition = 30
  TrackName = Yas Marina Circuit 
  GrandPrixName = 2019 OFCR ABU DHABI GRAND PRIX 
  EventName = 20.Abu Dhabi
  VenueName = OFCR Season 2019
  Location = Abu Dhabi, UAE
  Length = 5.554 km / 3.451 miles
  TerrainDataFile = YasMarina_19.TDF   // RFExL3C

  GarageDepth = 2.5
  FormationSpeedKPH = 200
  SafetyCarSpeedKPH=200 // la vitesse du safety
  RacePitKPH = 100
  NormalPitKPH = 100
  TestDaystart = 14:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 14:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 17:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 10:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 17:00
  RaceLaps = 55
  RaceTime = 120


  HeadlightsRequired = False
  NumStartingLights=6
  
  SkyBlendSunAngles=(-5.5, 5.0, 11.5, 25.5)
  ShadowMinSunAngle=65.0

  Latitude = 25.000          // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 0   // the direction of North in degrees (range: 0 to 359)
  RaceDate = November 23   // default date for the race

  SunriseAmbientRGB = (105,123,142)      
  SunriseDirectionalRGB = (254,240,208)
  SunriseFogRGB = (170,170,170)

  DayAmbientRGB = (110,115,125) 
  DayDirectionalRGB = (255,240,230) 
  DayFogRGB = (128,128,128) 

  SunsetAmbientRGB = (130,130,120) 
  SunsetDirectionalRGB = (255,248,198) 
  SunsetFogRGB = (94,94,94) 

  NightAmbientRGB = (20,20,20)
  NightDirectionalRGB = (40,40,40)
  NightFogRGB = (0,0,0)


  
  SettingsFolder = OFCR_2019
SettingsCopy=YasMarina_14_RFE.svm
SettingsCopy=YasMarina_14_RFE.svm
SettingsAIYasMarina_14_RFE.svm

}
// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 1344
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = 2757
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 1846
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 2619
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 2986
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 3648
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------
