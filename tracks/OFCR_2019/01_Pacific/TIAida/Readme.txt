rFactor TIAida V1.01 2009/4

V1.01 Changes

Fixed Invalid Garage Position.
Fixed Invalid Start Posision(Grid 32~).
Max 42 Grid&Garage Support.

Fixed Invisible Bump  Outside HairPin Curve(05TIAida).
Fixed Cut Track Warning(TIAida)

modify tdf file(Grip Level)
modify virtual startlight position.

----------------------------------------------
Convert From GPL Version.
Original Readme Include. Check ReadmeGPL.txt

Original Creater Polar Blue 21.
Thanks Polar san allowing convert.

LoadingScreen By Taku.

Special Thanks
  Rodgers
  IRC #rFactor Members.

Include 2 Layouts
TIAida    (Original Ver)
TIAida 05 (New MOSS-S Ver)

Extract TIAida folder Into rFactor\GameData\Locations

Have fun.
--
Piro
Contact to Piro77@gmail.com
