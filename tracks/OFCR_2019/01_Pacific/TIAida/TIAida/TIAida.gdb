TIAida
{
  Filter Properties = RoadCourse 2005 SRGrandPrix OWChallenge 
  Attrition = 15
  TrackName = TIAida
  EventName = 01.Pacific
  GrandPrixName = OFCR 2019 Pacific Grand Prix		//this must be the same as event name in order to sort circuit info correctly.
  VenueName = OFCR Season 2019
  Location = Okayama,Japan
  Length = 3.703 km / 2.3 miles
  TrackType = Permanent Road Course
  Track Record = Ayrton Senna(1994) , 70.218
  TerrainDataFile=TIAida.tdf

  GarageDepth = 1.0
  RacePitKPH = 80
  NormalPitKPH = 80
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 60
  Practice3Day = Saturday
  Practice3Start = 9:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 12:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 9:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 12:00
  RaceLaps = 60
  RaceTime = 120
  
  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = 34          // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 0  // the direction of North in degrees (range: 0 to 359)
  RaceDate = March 21   // default date for the race

  SunriseAmbientRGB = (160,160,150)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (180,189,226)
  DayDirectionalRGB = (255,255,255)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (130,130,120)
  SunsetDirectionalRGB = (255,248,198)
  SunsetFogRGB = (204,196,122)

  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)
  
  SettingsFolder = OFCR_2019
  Qualify Laptime = 75.669
  Race Laptime = 80.869
}
