Malaysia_19
{
  Filter Properties = *
  Attrition = 30
  TrackName = Sepang International Circuit
  GrandPrixName = OFCR 2019 Malaysia Grand Prix
  EventName = 02.Malaysia
  VenueName = OFCR Season 2019
  TrackType = Pernament Road Course
  Location = Sepang, Malaysia
  Length = 5.543 km
  TerrainDataFile= Malaysia_19.tdf 

  NumStartingLights=5

  GarageDepth = 2.9

  FormationSpeedKPH = 240
  SafetyCarSpeedKPH = 240
  RacePitKPH   = 80
  QualPitKPH   = 80
  NormalPitKPH = 60

  TestDayDay        = Friday
  TestDayStart      = 10:00
  Practice1Day      = Friday   // Friday Practice
  Practice1Start    = 10:00
  Practice1Duration = 120
  Practice2Day      = Saturday // Saturday Practice
  Practice2Start    = 13:00
  Practice2Duration = 90
  Practice3Day      = Saturday // Qualifying 1
  Practice3Start    = 16:00
  Practice3Duration = 18
  Practice4Day      = Saturday // Qualifying 2
  Practice4Start    = 16:30
  Practice4Duration = 15
  QualifyDay        = Saturday // Qualifying 3
  QualifyStart      = 15:50
  QualifyDuration   = 60
  QualifyLaps       = 12
  WarmupDay         = Sunday   // Warmup
  WarmupStart       = 14:00
  WarmupDuration    = 30
  RaceDay           = Sunday   // Grand Prix
  RaceStart         = 16:00
  RaceLaps          = 56
  RaceTime          = 120


  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=10.0

  Latitude = 3
  NorthDirection = 0
  RaceDate = 30th March 2014


  SunriseAmbientRGB = (80,120,160)      
  SunriseDirectionalRGB = (255,150,20)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (100,115,130)
  DayDirectionalRGB = (255,255,245)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (80,120,160)
  SunsetDirectionalRGB = (255,150,20)
  SunsetFogRGB = (94,94,94)

  NightAmbientRGB = (1,1,2)
  NightDirectionalRGB = (5,5,5)
  NightFogRGB = (0,0,0)

  Average Day Temp = 32

  SettingsFolder = OFCR_2019

PitStopStrategies
  {


    Lewis Hamilton = 3 - 15,33,51

    Nico Rosberg = 3 - 14,32,50

    Fernando Alonso = 3 - 11,27,42

    Kimi Raikkonen = 3 - 12,23,39

    Romain Grosjean = 3 - 13,26,41

    Pastor Maldonado = 3 - 15,30,45

    Nico Hulkenberg = 2 - 16,34

    Sergio Perez = 2 - 13,38

    Jenson Button = 3 - 13,25,39

    Kevin Magnussen = 3 - 10,25,40

    Kamui Kobayashi = 2 - 16,32

    Marcus Ericcson = 3 - 13,28,40

    Max Chilton = 3 - 12,29,43

    Jules Bianchi = 3 - 13,31,46

    Esteban Gutierrez = 3 - 10,23,37

    Adrian Sutil = 3 - 11,36,50

    Felipe Massa = 3 - 12,27,42

    Valterri Bottas = 3 - 14,29,44

    Danill Kvyat = 2 - 11,23,35

    Jean-Eric Vergne = 2 - 14,30,42

    Daniel Ricciardo = 2 - 12,38,50

    Sebastian Vettel = 2 - 13,31,51


  }

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 3920
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = 5070
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 4402
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 5060
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 5252
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 550
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------