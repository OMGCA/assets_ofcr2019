Singapore_19
{
  Filter Properties = * SuperSpeedWay 2005 SRGrandPrix OWChallenge 
  Attrition = 15
  VenueName = OFCR Season 2019
  TrackName = Marina Bay Street Circuit 
  EventName = 18.Singapore
  GrandPrixName = 2019 OFCR SINGAPORE GRAND PRIX 
  Location = Singapore, Singapore
  Length = 5.065 km
  TrackType = Temporary Street Circuit
  Track Record = 1:48.574
  TerrainDataFile = Singapore_19.TDF   
  HeadlightsRequired = False

  GarageDepth = 2.5
  RacePitKPH = 60
  NormalPitKPH = 60
  Practice1Day = Friday
  Practice1Start = 20:00
  Practice1Duration = 90
  Practice2Day = Friday
  Practice2Start = 22:00
  Practice2Duration = 90
  Practice3Day = Saturday
  Practice3Start = 19:00
  Practice3Duration = 60
  QualifyDay = Saturday
  QualifyStart = 20:00
  QualifyDuration = 30
  QualifyLaps = 30
  WarmupDay = Sunday
  WarmupStart = 21:30
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 22:00
  RaceLaps = 61
  RaceTime = 120

  NumStartingLights=6

  // Time-of-day lighting
  SkyBlendSunAngles=(-5.5, 5.0, 11.5, 25.5)

  ShadowMinSunAngle=10.0


  Latitude = 1.35          // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 103   // the direction of North in degrees (range: 0 to 359)
  RaceDate = October 19  // default date for the race

  SunriseAmbientRGB = (105,123,142)      
  SunriseDirectionalRGB = (254,240,208)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (85,90,100)
  DayDirectionalRGB = (255,255,255)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (101,133,161)
  SunsetDirectionalRGB = (213,166,112)
  SunsetFogRGB = (204,196,122)

  NightAmbientRGB = (15,17,20)                                    //(10,10,10)
  NightDirectionalRGB = (19,14,24)                                    //(15,15,15)
  NightFogRGB = (0,0,0)

  SettingsFolder = OFCR_2019
  SettingsCopy = Grip.svm
  SettingsCopy = Singapore_14_RFE.svm
  SettingsAI = Singapore_14_RFE.svm
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 2 = 4515
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 1 = 573
// DRS Zone 2 start (must be greater than Detection point)
DRS ZONE 2 START = 4790
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 254
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 1 START = 903
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 1612
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------