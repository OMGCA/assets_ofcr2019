Algarve_2019
{
  Filter Properties = *
  RaceLaps = 20
  Attrition = 10
  TrackName = Algarve
  EventName = 03.Portugal
  GrandPrixName = OFCR 2019 Grande Premio de Portugal
  VenueName =  OFCR Season 2019
  Location = PORTIMAO, PORTUGAL
  Length = 4.692 km
  TrackType = Permanent Circuit
  Track Record = 
  TerrainDataFile= Algarve_2010.tdf 
  
  GarageDepth = 3.5
  RacePitKPH = 80
  NormalPitKPH = 80
  TestDaystart = 22:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 60
  Practice3Day = Saturday
  Practice3Start = 9:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 12:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 9:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 13:00
  RaceLaps = 60
  RaceTime = 120

  SkyBlendSunAngles=(-5.5, 5.0, 11.5, 25.5)
  ShadowMinSunAngle=10.0

  Latitude = 45 // in degrees, -90 ... +90 (def. 0)
  NorthDirection = 270 // in degrees (def. 245)
  RaceDate = August 1 // (def. July 1)

  SunriseAmbientRGB = (150,132,130)             //(120,120,110)      
  SunriseDirectionalRGB =(175,178,210)          //(255,248,198)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (135,130,128)                 //(85,90,100)
  DayDirectionalRGB = (255,250,248)             //(255,255,255)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (108,124,144)              //(130,130,120)
  SunsetDirectionalRGB = (215,172,72)           //(255,248,198)
  SunsetFogRGB = (204,196,122)

  NightAmbientRGB = (25,27,40)                  //(10,10,10)
  NightDirectionalRGB = (29,34,54)              //(15,15,15)
  NightFogRGB = (0,0,0)

  SettingsFolder = OFCR_2019
  SettingsCopy = Grip.svm
  SettingsCopy = PORTIMAO.svm
  SettingsAI = PORTIMAO.svm
  Qualify Laptime = 80.000
  Race Laptime = 84.000
}
