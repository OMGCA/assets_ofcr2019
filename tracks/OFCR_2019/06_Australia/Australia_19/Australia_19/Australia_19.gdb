Australia_19
{
  Filter Properties = *
  Attrition = 15
  TrackName = Melbourne Park Circuit
  GrandPrixName = 2019 OFCR AUSTRALIAN GRAND PRIX
  EventName = 06.Australia
  VenueName = OFCR Season 2019
  Location = Melbourne, AU
  TrackType = Temporary Street Circuit
  Track Record = Michael Schumacher (2004), 1:24.125
  Length = 5.302 km
  TerrainDataFile = Australia_19.tdf 
  
HeadlightsRequired = false
  GarageDepth = 3.2
  NumStartingLights = 6

  FormationSpeedKPH = 240
  SafetyCarSpeedKPH = 240
  RacePitKPH   = 80
  QualPitKPH   = 80
  NormalPitKPH = 60

  TestDayDay        = Friday
  TestDayStart      = 10:00
  Practice1Day      = Friday   // Friday Practice
  Practice1Start    = 10:00
  Practice1Duration = 120
  Practice2Day      = Saturday // Saturday Practice
  Practice2Start    = 13:00
  Practice2Duration = 90
  Practice3Day      = Saturday // Qualifying 1
  Practice3Start    = 16:00
  Practice3Duration = 18
  Practice4Day      = Saturday // Qualifying 2
  Practice4Start    = 16:30
  Practice4Duration = 15
  QualifyDay        = Saturday // Qualifying 3
  QualifyStart      = 16:00
  QualifyDuration   = 60
  QualifyLaps       = 12
  WarmupDay         = Sunday   // Warmup
  WarmupStart       = 14:00
  WarmupDuration    = 30
  RaceDay           = Sunday   // Grand Prix
  RaceStart         = 15:00
  RaceLaps          = 58
  RaceTime          = 120

  
  SkyBlendSunAngles=(-10.0, -1.0, 1.0, 10.0)

  ShadowMinSunAngle=15.0

  Latitude = -37     // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 0 // the direction of North in degrees (range: 0 to 359)
  RaceDate = 16th March 2014    // default date for the race

  SunriseAmbientRGB = (80,120,160)      
  SunriseDirectionalRGB = (255,150,20)
  SunriseFogRGB = (255,239,223)

  DayAmbientRGB = (110,115,125) // RFExL3C
  DayDirectionalRGB = (255,240,230) // RFExL3C
  DayFogRGB = (128,128,128) // RFExL3C

  SunsetAmbientRGB = (130,130,120) // RFExL3C
  SunsetDirectionalRGB = (255,248,198) // RFExL3C
  SunsetFogRGB = (94,94,94) // RFExL3C

  NightAmbientRGB = (1,1,2)
  NightDirectionalRGB = (5,5,5)
  NightFogRGB = (0,0,0)
  
  SettingsFolder = OFCR_2019
  SettingsCopy = Melbourne14.svm
  SettingsAI = Melbourne14.svm

PitStopStrategies
  {

    Lewis Hamilton = 2 - 14,39

    Nico Rosberg = 2 - 12,38

    Fernando Alonso = 2 - 12,35

    Kimi Raikkonen = 2 - 13,37

    Romain Grosjean = 2 - 28,43

    Pastor Maldonado = 2 - 29,42

    Nico Hulkenberg = 2 - 12,33

    Sergio Perez = 2 - 11,32

    Jenson Button = 2 - 11,32

    Kevin Magnussen = 2 - 12,37

    Kamui Kobayashi = 2 - 21,45

    Marcus Ericcson = 2 - 23,42

    Max Chilton = 2 - 24,41

    Jules Bianchi = 2 - 20,35

    Esteban Gutierrez = 1 - 30

    Adrian Sutil = 1 - 35

    Felipe Massa = 2 - 15,41

    Valterri Bottas = 2 - 10,36

    Danill Kvyat = 2 - 12,36

    Jean-Eric Vergne = 2 - 12,33

    Daniel Ricciardo = 2 - 12,36

    Sebastian Vettel = 2 - 13,34
}
 
[RF_HIGHVOLTAGE1]
DRS DETECTION POINT 1 = 4362
DRS DETECTION POINT 2 = -1
DRS ZONE 1 START = 4885
DRS ZONE 1 STOP = 258
DRS ZONE 2 START = 584
DRS ZONE 2 STOP = 982
DRS ZONE 3 START = -1
DRS ZONE 3 STOP = -1
DRS ZONE 4 START = -1
DRS ZONE 4 STOP = -1
DRS BLOCK ZONE 1 START = -1
DRS BLOCK ZONE 1 STOP = -1
DRS BLOCK ZONE 2 START = -1
DRS BLOCK ZONE 2 STOP = -1