Germany_19
{
  Filter Properties = *
  Attrition = 30
  TrackName = Hockenheimring RFE
  GrandPrixName = OFCR GROSSER PREIS VON DEUTSCHLAND 2019
  EventName =  12.Germany
  VenueName = OFCR Season 2019
  Location = Hockenheim, Germany
  Length = 4.2 Miles / 6.8 Km
  TrackType = Permanent Road Course
  Track Record =
  TerrainDataFile=Germany_19.tdf

  GarageDepth = 1.0
  FormationSpeedKPH = 200
  SafetyCarSpeedKPH=200 // la vitesse du safety
  RacePitKPH = 100
  NormalPitKPH = 100
  TestDaystart = 14:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 14:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 10:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 14:00
  RaceLaps = 67
  RaceTime = 120
  
  NumStartingLights=5

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = 41.34     // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 340 // the direction of North in degrees (range: 0 to 359)
  RaceDate = July 22   // default date for the race

  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (90,90,90)
  DayDirectionalRGB = (255,255,240)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (160,155,140)
  SunsetDirectionalRGB = 200,190,150)
  SunsetFogRGB = (130,130,130)

  NightAmbientRGB = (10,19,46)
  NightDirectionalRGB = (30,30,30)
  NightFogRGB = (0,0,0)
  
  SettingsFolder = OFCR_2019
  SettingsAI = Hockenheim_12.svm
  Qualify Laptime = 85.000
  Race Laptime = 87.000
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 1053
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = -1
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 1315
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 1950
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = -1
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = -1
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------