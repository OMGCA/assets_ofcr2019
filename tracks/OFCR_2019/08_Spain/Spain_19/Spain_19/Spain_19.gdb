Spain_19
{
  Filter Properties = * SuperSpeedWay 2005 SRGrandPrix OWChallenge 
  Attrition = 15
  TrackName = Circuit de Catalunya 
  GrandPrixName = OFCR GRAN PREMIO DE ESPANA 2019	
  EventName = 08.Spain
  VenueName = OFCR Season 2019
  Location = Barcelona, Spain
  TrackType = Pernament Road Course
  Length = 4.637 km
  Track Record = Kimi Raikkonen, 2008 - 1:21,670
  TerrainDataFile= Spain_19.tdf

  FormationSpeedKPH = 200
  SafetyCarSpeedKPH=200 // la vitesse du safety
  RacePitKPH = 80
  NormalPitKPH = 80
  TestDaystart = 8:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 14:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 10:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 14:00
  RaceLaps = 66
  RaceTime = 120

  GarageDepth = 3.0

  NumStartingLights=6
  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)


  ShadowMinSunAngle=15.0


  Latitude = 50          // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 6  // the direction of North in degrees (range: 0 to 359)
  RaceDate = MAY 15   // default date for the race


  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)


  DayAmbientRGB = (100,100,126)
  DayDirectionalRGB = (255,255,255)
  DayFogRGB = (203,214,236)


  SunsetAmbientRGB = (130,130,120)
  SunsetDirectionalRGB = (255,248,198)
  SunsetFogRGB = (204,196,122)


  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)

///////////////////////////SCORETOWER DATA////////////////////////////////////////////

ScoreboardFont=COTA_scorefont.tga // default is scoreboardfont.bmp
ScoreboardBackground=scoretowerbkg.tga // default is scoreboardbkg.bmp

ScoreboardMaxEntries=10 // how many car numbers can be displayed on tower (default is 32)
ScoreboardStartX=0 // x-position in texture to write first car number (default is 0)
ScoreboardStartY=2 // y-position in texture to write first car number (default is 10)
ScoreboardIncX=0 // increment in x-position for each new car number (default is 0)
ScoreboardIncY=52 // increment in y-position for each new car number (default is 16)
ScoreboardScaleX=2.2 // scale multiplier for x (default is 1.0)
ScoreboardScaleY=1.8 // scale multiplier for y (default is 1.0)

//////////////////////////////////////////////////////////////////////////////////////

  SettingsFolder = OFCR_2019

PitStopStrategies
  {

    Lewis Hamilton = 2 - 18,43

    Nico Rosberg = 2 - 21,45

    Fernando Alonso = 3 - 16,35,53

    Kimi Raikkonen = 2 - 18,44

    Romain Grosjean = 2 - 16,35

    Pastor Maldonado = 2 - 16,38

    Nico Hulkenberg = 2 - 18,37

    Sergio Perez = 2 - 19,38

    Jenson Button = 2 - 18,37

    Kevin Magnussen = 2 - 20,42

    Kamui Kobayashi = 2 - 23,40

    Marcus Ericcson = 2 - 20,42

    Max Chilton = 3 - 19,33,44

    Jules Bianchi = 2 - 23,43

    Esteban Gutierrez = 3 - 19,37,53

    Adrian Sutil = 2 - 18,36

    Felipe Massa = 3 - 16,29,47

    Valterri Bottas = 2 - 20,45

    Danill Kvyat = 3 - 19,37,54

    Jean-Eric Vergne = 3 - 16,33,52

    Daniel Ricciardo = 2 - 14,45

    Sebastian Vettel = 2 - 12,33,52
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 4108
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = 2612
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 4500
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 520
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 2859
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 3306
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------
// ----------------------------------------------------------------