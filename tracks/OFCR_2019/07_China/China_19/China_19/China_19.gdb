China_19
{
  Filter Properties = *
  Attrition = 45
  TrackName = Shanghai International Circuit
  GrandPrixName = 2019 OFCR CHINESE GRAND PRIX
  EventName = 07.China
  VenueName = OFCR Season 2019
  Location = Shanghai, China
  Length = 5.451 km
  TerrainDataFile = China_19.tdf 
  NumStartingLights=6

  GarageDepth = 1.5
  FormationSpeedKPH = 185
  SafetyCarSpeedKPH=200 // la vitesse du safety
  RacePitKPH = 80
  NormalPitKPH = 80
  TestDaystart = 14:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 14:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 10:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 14:00
  RaceLaps = 56
  RaceTime = 120

///////////////////////////SCORETOWER DATA////////////////////////////////////////////

ScoreboardFont=COTA_scorefont.tga // default is scoreboardfont.bmp
ScoreboardBackground=scoretowerbkg.tga // default is scoreboardbkg.bmp

ScoreboardMaxEntries=10 // how many car numbers can be displayed on tower (default is 32)
ScoreboardStartX=0 // x-position in texture to write first car number (default is 0)
ScoreboardStartY=2 // y-position in texture to write first car number (default is 10)
ScoreboardIncX=0 // increment in x-position for each new car number (default is 0)
ScoreboardIncY=52 // increment in y-position for each new car number (default is 16)
ScoreboardScaleX=2.2 // scale multiplier for x (default is 1.0)
ScoreboardScaleY=1.8 // scale multiplier for y (default is 1.0)

//////////////////////////////////////////////////////////////////////////////////////
 
  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = 2.76       // in degrees, -90 ... +90 (def. 0)  //original was 40.97
  NorthDirection = 0.00 // in degrees (def. 245)
  RaceDate = March 25   // default date for the race


  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (110,115,125)
  DayDirectionalRGB = (255,240,230)
  DayFogRGB = (128,128,128)

  SunsetAmbientRGB = (130,130,120)
  SunsetDirectionalRGB = (255,248,198)
  SunsetFogRGB = (94,94,94)

  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)
  

  SettingsFolder = OFCR_2019
  SettingsCopy = Grip.svm
  SettingsCopy = Shanghai_12.svm
  SettingsAI = Shanghai_12.svm
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 3243
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = -1
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 3962
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 4650
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = -1
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = -1
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------