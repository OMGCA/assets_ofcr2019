Montreal_19
{
  Filter Properties = *
  Attrition = 30
  TrackName = Circuit Gilles Villeneuve
  EventName = 09.Canada
  GrandPrixName = OFCR GRAND PRIX DU CANADA 2019
  VenueName = OFCR Season 2019
  Location = Canada, Montreal
  Length = 4.361 km
  TrackType = Temporary Street Circuit
  Track Record = Ralf Schumacher (2004), 1:12.275
  HeadlightsRequired = false
  TerrainDataFile = Montreal_19.TDF   // RFExL3C


  FormationSpeedKPH = 240
  SafetyCarSpeedKPH = 240
  RacePitKPH   = 80
  QualPitKPH   = 80
  NormalPitKPH = 60

  TestDayDay        = Friday
  TestDayStart      = 10:00
  Practice1Day      = Friday   // Friday Practice
  Practice1Start    = 10:00
  Practice1Duration = 120
  Practice2Day      = Saturday // Saturday Practice
  Practice2Start    = 13:00
  Practice2Duration = 90
  Practice3Day      = Saturday // Qualifying 1
  Practice3Start    = 16:00
  Practice3Duration = 18
  Practice4Day      = Saturday // Qualifying 2
  Practice4Start    = 16:30
  Practice4Duration = 15
  QualifyDay        = Saturday // Qualifying 3
  QualifyStart      = 13:50
  QualifyDuration   = 12
  QualifyLaps       = 255
  WarmupDay         = Sunday   // Warmup
  WarmupStart       = 14:00
  WarmupDuration    = 30
  RaceDay           = Sunday   // Grand Prix
  RaceStart         = 14:00
  RaceLaps          = 70
  RaceTime          = 120
  
  NumStartingLights=6

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=10.0

  Latitude = 45.31     // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 90   // the direction of North in degrees (range: 0 to 359)
  RaceDate = June 7   // default date for the race

  SunriseAmbientRGB = (117,116,89)      
  SunriseDirectionalRGB = (255,255,245)
  SunriseFogRGB = (170,170,170)

  DayAmbientRGB = (110,115,125) // RFExL3C
  DayDirectionalRGB = (255,240,230) // RFExL3C
  DayFogRGB = (128,128,128) // RFExL3C

  SunsetAmbientRGB = (130,130,120) // RFExL3C
  SunsetDirectionalRGB = (255,248,198) // RFExL3C
  SunsetFogRGB = (94,94,94) // RFExL3C

  NightAmbientRGB = (15,15,50)
  NightDirectionalRGB = (20,20,20)
  NightFogRGB = (30,30,30)

  Average Rain = 0.1
  Average Rain Intensity = 0.1
  Average Day Temp = 28

  SettingsFolder = OFCR_2019
  SettingsCopy = Grip.svm
  SettingsCopy=Montreal_2011_RFE.svm
  SettingsAIMontreal_2011_RFE.svm
  Qualify Laptime = 70.200
  Race Laptime = 70.500

PitStopStrategies
  {

    Lewis Hamilton = 2 - 19,46

    Nico Rosberg = 2 - 18,44

    Fernando Alonso = 2 - 16,44

    Kimi Raikkonen = 2 - 17,39

    Romain Grosjean = 2 - 11,46

    Pastor Maldonado = 2 - 21,49

    Nico Hulkenberg = 1 - 41

    Sergio Perez = 1 - 34

    Jenson Button = 2 - 14,38

    Kevin Magnussen = 2 - 15,45

    Kamui Kobayashi = 2 - 18,41

    Marcus Ericcson = 2 - 21,44

    Max Chilton = 2 - 35,53

    Jules Bianchi = 2 - 32,51

    Esteban Gutierrez = 2 - 16,39

    Adrian Sutil = 2 - 16,41

    Felipe Massa = 2 - 15,33

    Valterri Bottas = 2 - 14,35

    Danill Kvyat = 2 - 29,46

    Jean-Eric Vergne = 2 - 15,39

    Daniel Ricciardo = 2 - 13,37

    Sebastian Vettel = 2 - 15,36
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 2233
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = -1
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 3155
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 3845
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 4002
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 210
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------

