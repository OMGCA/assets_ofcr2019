Indianapolis_19
{
  Filter Properties = RoadCourse SRGrandPrix OWChallenge
  Attrition = 21
  TrackName = Indianapolis Road Course
  EventName = 10.USA
  GrandPrixName = OFCR 2019 United States Grand Prix  //this must be the same as event name in order to sort circuit info correctly
  VenueName = OFCR Season 2019
  Location = Speedway, Indiana
  TrackType = Speedway/Infield Road Course
  Length = 4.192 km / 2.606 miles
  Track Record = 1:10.399, Rubens Barrichello 
  FormationAndStart= 0  // standing start
  HeadlightsRequired = True  // whether headlights are required at night
  TerrainDataFile= ..\Indianapolis.tdf  // terrain file override


  // DefaultScoring overrides

  GarageDepth = 2.00
  RacePitKPH = 120.70
  QualPitKPH = 96.56
  NormalPitKPH = 96.56
  FormationSpeedKPH = 160.93
  TestDaystart = 11:00
  Practice1Start = 10:00
  Practice2Start = 14:00
  Practice3Start = 10:00
  QualifyStart = 13:00
  RaceStart = 13:00
  RaceLaps = 73
  NumStartingLights=5

  CONE = ( 2.5, 0.25, 0.20, 0.25, 1425.6, 72.0, 0.80 )
  SIGN = ( 5.0, 4.0, 2.0, 6.0, 1024.0, 44.8, 0.70 )



  // Time-of-day lighting
  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=11.0

  Latitude = 39         // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 0  // the direction of North in degrees (range: 0 to 359)
  RaceDate = June 30   // default date for the race

  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (195,160,175)

  DayAmbientRGB = (80,89,126)
  DayDirectionalRGB = (255,255,255)
  DayFogRGB = (200,205,215)

  SunsetAmbientRGB = (130,130,120)
  SunsetDirectionalRGB = (255,248,198)
  SunsetFogRGB = (204,196,122)

  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)


///////////////////////////SCORETOWER DATA////////////////////////////////////////////

  ScoreboardFont=IMS_SCOREFONT.tga 
  ScoreboardBackground=scoretowerbkg.tga 

  ScoreboardMaxEntries=33
  ScoreboardStartX=0 
  ScoreboardStartY=0 
  ScoreboardIncX=0 
  ScoreboardIncY=29.25
  ScoreboardScaleX=2.2 
  ScoreboardScaleY=1.2

//////////////////////////////////////////////////////////////////////////////////////

  SettingsFolder = Indianapolis Road Course
  SettingsCopy = Indianapolis_RC.svm
  SettingsCopy = Indianapolis_RC.svm
  SettingsAI = Indianapolis_RC.svm
  Qualify Laptime = 71.413
  Race Laptime = 75.294
}
