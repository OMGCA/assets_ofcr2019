------ Kyalami VERSION 0.9 ------
=====================================
INSTALL
=====================================

Extract to your "rFactor\GameData\Locations" folder

=====================================
FEATURES
=====================================

* Kyalami Conversion for rFactor which includes 2 versions
* 1 to represent the Grand Prix Masters race held in Nov, 2005
* 1 version that has Nightlights (fantasy) roughly based on the era of 91-93

=====================================
KNOWN BUGS
=====================================

* Track has been known to briefly pause ocassionally.
* Lag spikes known to arise for a period of 3-5secs 
  when Nightlights are activated. 

=====================================
TRACK HISTORY
=====================================

* Simbin bought it over from SBK to F1C for GTR2002 Mod
* TheHeathen converted it to GTR
* Ferrari27 and DeadEyeski got it into rFactor
 
=====================================
CREDITS
=====================================

* Stu Griffiths for advice
* Ferrari27 and DeadEyeski for conversion to rFactor
* afdelta, ISI and others for Models
* motorfx and Mr DutchDevil for compiling great AIW files.
* (Euro)Danny for great Skies
* andronin and GoReFeST for server and local info
* Portstevo for brutal honesty 
* And all the beta testers.

=====================================
MORE INFO
=====================================
  
  http://team-redback-racing.com/toads/Kyalami_site/index.htm

=====================================
DEDICATION
=====================================

* I'd like to dedicate my efforts of this conversion 
  to my late Father that i lost during this project.
 
  R.I.P. Dad, forever with you

=====================================

Thanks Toads :)
