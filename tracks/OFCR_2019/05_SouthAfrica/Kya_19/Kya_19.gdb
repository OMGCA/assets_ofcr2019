Kya_19
{
  Filter Properties = *
  Attrition = 20
  TrackName = Kyalami
  EventName = 05.South Africa
  GrandPrixName = OFCR 2019 South African Grand Prix
  VenueName = OFCR Season 2019
  Location = South Africa
  Length = 2.6 miles / 4.2 km
  TrackType = Permanent Road Course
  Track Record = 1:17:578
  TerrainDataFile=Kya_19.tdf

  GarageDepth = 1.0
  RacePitKPH = 80
  NormalPitKPH = 80
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 60
  Practice3Day = Saturday
  Practice3Start = 9:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 12:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 9:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 14:30
  RaceLaps = 60
  RaceTime = 120
  
  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = 0          // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 245  // the direction of North in degrees (range: 0 to 359)
  RaceDate = July 12   // default date for the race

  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (85,90,100)
  DayDirectionalRGB = (255,255,255)
  DayFogRGB = (203,214,236)

  SunsetAmbientRGB = (130,130,120)
  SunsetDirectionalRGB = (255,248,198)
  SunsetFogRGB = (204,196,122)

  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)
  
  SettingsFolder = OFCR_2019
  SettingsCopy = Grip.svm
  SettingsCopy = Kya_GPM_05.svm
  SettingsAI = Kya_GPM_05.svm

  Qualify Laptime = 
  Race Laptime = 
}
