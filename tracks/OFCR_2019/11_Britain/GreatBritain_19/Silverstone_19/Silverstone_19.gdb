Silverstone_19
{
//MAIN TRACK SETTINGS

  Filter Properties = * SuperSpeedWay 2005 SRGrandPrix OWChallenge
  TrackName = Silverstone Circuit
  EventName = 11.United Kingdom
  GrandPrixName = 2019 OFCR BRITISH GRAND PRIX
  VenueName = OFCR Season 2019
  Location = Silverstone, Great Britain
  Length = 5.890 km
  TrackType = Permanent Road Course
  TerrainDataFile = Silverstone_19.TDF   // RFExL3C
Track Record = 1:30.874 � Fernando Alonso

SIGN = ( 7.5, 6.0, 3.0, 9.0, 1536.0, 67.2, 1.40 )

  GarageDepth = 4.5
  FormationSpeedKPH = 200
  RacePitKPH = 80
  NormalPitKPH = 80
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 10:00
  Practice2Duration = 60
  Practice3Day = Saturday
  Practice3Start = 14:00
  Practice3Duration = 60
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 60
  QualifyStart = 13:50
  WarmupStart = 12:55
  RaceStart = 13:00
  RaceLaps = 52
  
  NumStartingLights=6

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)


  ShadowMinSunAngle=15.0


  Latitude = 52.09          // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 6  // the direction of North in degrees (range: 0 to 359)
  RaceDate = July 4   // default date for the race


  SunriseAmbientRGB = (117,116,89)      
  SunriseDirectionalRGB = (255,255,245)
  SunriseFogRGB = (170,170,170)

  DayAmbientRGB = (110,115,125) // RFExL3C
  DayDirectionalRGB = (255,240,230) // RFExL3C
  DayFogRGB = (128,128,128) // RFExL3C

  SunsetAmbientRGB = (130,130,120) // RFExL3C
  SunsetDirectionalRGB = (255,248,198) // RFExL3C
  SunsetFogRGB = (94,94,94) // RFExL3C

  NightAmbientRGB = (15,15,50)
  NightDirectionalRGB = (20,20,20)
  NightFogRGB = (30,30,30)

  SettingsFolder = OFCR_2019
  SettingsAISilverStoneBGP_RFE.svm
  Qualify Laptime = 89.615
  Race Laptime = 90.874


}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 853
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = 3725
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 1319
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 1903
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 4304
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 4900
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
