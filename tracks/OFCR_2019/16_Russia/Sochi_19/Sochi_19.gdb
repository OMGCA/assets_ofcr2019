Sochi_19
{
  Filter Properties = * 
  Attrition = 30
  VenueName = OFCR Season 2019
  TrackName = Sochi Autodrom 
  EventName = 16.Russia
  GrandPrixName = 2019 OFCR RUSSIAN GRAND PRIX 
  Location = Sochi, Russia
  Length = 5.848 km
  TrackType = Permanent Road Course
  Track Record = 1:40.896 (Valttieri Bottas) 
  TerrainDataFile = Sochi_19.TDF   // RFExL3C

GarageDepth = 1.0
  FormationSpeedKPH = 200
  SafetyCarSpeedKPH=200 // la vitesse du safety
  RacePitKPH = 100
  NormalPitKPH = 100
  TestDaystart = 14:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 12:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 15:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 14:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 15:00
  RaceLaps = 67
  RaceTime = 120
  
  NumStartingLights=6

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=15.0

  Latitude = 25.34     // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 340 // the direction of North in degrees (range: 0 to 359)
  RaceDate = October 12   // default date for the race

  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (110,115,125) // RFExL3C
  DayDirectionalRGB = (255,240,230) // RFExL3C
  DayFogRGB = (128,128,128) // RFExL3C

  SunsetAmbientRGB = (130,130,120) // RFExL3C
  SunsetDirectionalRGB = (255,248,198) // RFExL3C
  SunsetFogRGB = (94,94,94) // RFExL3C

  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)
  
  SettingsFolder = OFCR_2019
  SettingsAI = 2014_Sochi.svm
  Qualify Laptime = 85.000
  Race Laptime = 87.000
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 2 = 3542
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 1 = 333
// DRS Zone 2 start (must be greater than Detection point)
DRS ZONE 2 START = 3904
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 4698
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 1 START = 547
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 1198
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------