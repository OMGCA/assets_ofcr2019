Spa_19
{
  Filter Properties = *
  Attrition = 25
  VenueName = OFCR Season 2019
  TrackName = Circuit de Spa-Francorchamps
  EventName = 15.Belgium
  GrandPrixName = 2019 OFCR BELGIAN GRAND PRIX
  Location = Belgium, Spa
  Length = 7.004 km
  TrackType = Pernament Road Course
  Track Record =
  TerrainDataFile = Spa_19.TDF   // RFExL3C

  GarageDepth = 1.9
  
  NumStartingLights=6

  TestDaystart = 14:00
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 60
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 10
  Practice3Day = Saturday
  Practice3Start = 11:00
  Practice3Duration = 45
  Practice4Day = Saturday
  Practice4Start = 10:00
  Practice4Duration = 45
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 60
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 13:00
  WarmupDuration = 30
  RaceDay = Sunday
  RaceStart = 14:00
  RaceLaps = 52
  RaceTime = 120
  
  NumStartingLights=6

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)


  ShadowMinSunAngle=15.0


  Latitude = 50
  NorthDirection = 0
  RaceDate = 24th August 2014


  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)


  DayAmbientRGB = (100,100,126)
  DayDirectionalRGB = (255,255,255)
  DayFogRGB = (203,214,236)


  SunsetAmbientRGB = (130,130,120)
  SunsetDirectionalRGB = (255,248,198)
  SunsetFogRGB = (204,196,122)


  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)

  
  SettingsFolder = OFCR_2019

PitStopStrategies
  {

    Lewis Hamilton = 2 - 17,31

    Nico Rosberg = 3 - 8,19,34

    Fernando Alonso = 2 - 12,25

    Kimi Raikkonen = 2 - 8,21

    Romain Grosjean = 2 - 13,27

    Pastor Maldonado = 2 - 11,29

    Nico Hulkenberg = 2 - 10,30

    Sergio Perez = 2 - 9,22

    Jenson Button = 2 - 13,29

    Kevin Magnussen = 2 - 11,23

    Kamui Kobayashi = 2 - 14,28

    Marcus Ericcson = 2 - 11,24

    Max Chilton = 2 - 11,30

    Jules Bianchi = 1 - 23

    Esteban Gutierrez = 2 - 14,30

    Adrian Sutil = 3 - 11,21,34

    Felipe Massa = 3 - 9,21,35

    Valterri Bottas = 2 - 12,28

    Danill Kvyat = 2 - 10,24

    Jean-Eric Vergne = 2 - 15,29

    Daniel Ricciardo = 2 - 11,27

    Sebastian Vettel = 3 - 10,22,34
  
    Andre Lotterer = 2 - 17,31
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 6584
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = 606
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 6750
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 211
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = 1423
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = 2235
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------
 
