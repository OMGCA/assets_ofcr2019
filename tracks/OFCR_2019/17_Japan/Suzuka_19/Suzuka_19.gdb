Suzuka_19
{
Filter Properties =  *
  Attrition = 10
  VenueName = OFCR Season 2019
  TrackName = Suzuka International Racing Course
  EventName = 17.Japan
  GrandPrixName = 2019 OFCR JAPANESE GRAND PRIX
  Location = Mie, Japan
  Length = 5.807 km
  TrackType = Pernament Road Course
  Track Record = 1:31.540
  TerrainDataFile = Suzuka_19.TDF
  HeadlightsRequired = true

  GarageDepth = 2.9
  RacePitKPH = 80
  NormalPitKPH = 80
  Practice1Day = Friday
  Practice1Start = 10:00
  Practice1Duration = 165
  Practice2Day = Friday
  Practice2Start = 14:00
  Practice2Duration = 165
  Practice3Day = Saturday
  Practice3Start = 9:00
  Practice3Duration = 15
  Practice4Day = Saturday
  Practice4Start = 12:00
  Practice4Duration = 12
  QualifyDay = Saturday
  QualifyStart = 14:00
  QualifyDuration = 8
  QualifyLaps = 12
  WarmupDay = Sunday
  WarmupStart = 9:00
  WarmupDuration = 165
  RaceDay = Sunday
  RaceStart = 12:00
  RaceLaps = 90
  RaceTime = 120

  NumStartingLights=6

  SkyBlendSunAngles=(-20.5, -1.0, 11.5, 25.5)

  ShadowMinSunAngle=5.0

  Latitude = 34.73          // degs from Equator (range: -90 to 90, positive is Northern Hemisphere)
  NorthDirection = 19  // the direction of North in degrees (range: 0 to 359)
  RaceDate = October 08   // default date for the race

  SunriseAmbientRGB = (120,120,110)      
  SunriseDirectionalRGB = (255,248,198)
  SunriseFogRGB = (204,174,240)

  DayAmbientRGB = (110,115,125) // RFExL3C
  DayDirectionalRGB = (255,240,230) // RFExL3C
  DayFogRGB = (128,128,128) // RFExL3C

  SunsetAmbientRGB = (130,130,120) // RFExL3C
  SunsetDirectionalRGB = (255,248,198) // RFExL3C
  SunsetFogRGB = (94,94,94) // RFExL3C

  NightAmbientRGB = (10,10,10)
  NightDirectionalRGB = (15,15,15)
  NightFogRGB = (0,0,0)

  SettingsFolder = OFCR_2019
  SettingsCopy = Grip.svm
  SettingsCopy = Suzuka.svm
  SettingsAI = Suzuka.svm
  Qualify Laptime = 91.147
  Race Laptime = 93.449
}

// ----------------------------------------------------------------
[RF_HIGHVOLTAGE1]
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline)
DRS DETECTION POINT 1 = 5298
// Where will the distance to the car in front be detected (Distance in meters from Start/Finishline / -1 to disable)
DRS DETECTION POINT 2 = -1
// DRS Zone 1 start (must be greater than Detection point)
DRS ZONE 1 START = 5721
// DRS Zone 1 stop, DRS is allowed in this zone only during race
DRS ZONE 1 STOP = 634
// DRS Zone 2 start (must be greater than Zone 1 stop, or -1 to deactivate)
DRS ZONE 2 START = -1
// DRS Zone 2 stop, DRS is allowed in this zone only during race
DRS ZONE 2 STOP = -1
// DRS Zone 3 start (must be greater than Zone 2 stop, or -1 to deactivate)
DRS ZONE 3 START = -1
// DRS Zone 3 stop, DRS is allowed in this zone only during race
DRS ZONE 3 STOP = -1
// DRS Zone 4 start (must be greater than Zone 3 stop, or -1 to deactivate)
DRS ZONE 4 START = -1
// DRS Zone 4 stop, DRS is allowed in this zone only during race
DRS ZONE 4 STOP = -1

// Block zones are zones on the track where DRS isn't allowed to be used in any session
// DRS Block Zone 1 start (-1 to deactivate)
DRS BLOCK ZONE 1 START = -1
// DRS Block Zone 1 stop (after Block Zone 1 start, or -1 to deactivate)
DRS BLOCK ZONE 1 STOP = -1
// DRS Block Zone 2 start (-1 to deactivate)
DRS BLOCK ZONE 2 START = -1
// DRS Block Zone 2 stop (after Block Zone 2 start, or -1 to deactivate)
DRS BLOCK ZONE 2 STOP = -1
// ----------------------------------------------------------------